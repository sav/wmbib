include(conferences.m4)

@Article{	  abadi1991:refinement,
  title		= {The existence of refinement mappings},
  author	= {Martín Abadi and Leslie Lamport},
  journal	= {Theoretical Computer Science},
  year		= 1991,
  volume	= 82,
  number	= 2,
  pages		= {253-284},
  doi		= {10.1016/0304-3975(91)90224-P}
}

@Article{	  abadi1995:dynamic-typing,
  title		= {Dynamic typing in polymorphic languages},
  author	= {Martín Abadi and Luca Cardelli and Benjamin Pierce and
		  Didier Rémy},
  journal	= {JNL_JFP},
  year		= 1995,
  volume	= 5,
  number	= 1
}

@InProceedings{	  abdulla2013:specification,
  title		= {An integrated specification and verification technique for
		  highly concurrent data structures},
  author	= {Abdulla, Parosh Aziz and Haziza, Frédéric and Holík,
		  Lukáš and Jonsson, Bengt and Rezine, Ahmed},
  booktitle	= {CONF_TACAS 2013},
  year		= {2013},
  pages		= {324-338},
  editor	= {Nir Piterman and Scott A. Smolka},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {7795}
}

@InProceedings{	  abdulla2014:optimal-dpor,
  title		= {Optimal dynamic partial order reduction},
  author	= {Parosh Aziz Abdulla and Stavros Aronis and Bengt Jonsson
		  and Konstantinos Sagonas},
  booktitle	= {CONF_POPL 2014},
  year		= {2014},
  pages		= {373--384},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {http://doi.acm.org/10.1145/2535838.2535845},
  doi		= {10.1145/2535838.2535845},
  ids		= {optimal-dpor}
}

@InProceedings{	  abdulla2015:fence-insertion,
  title		= {The best of both worlds: {Trading} efficiency and
		  optimality in fence insertion for {TSO}},
  author	= {Parosh Aziz Abdulla and Mohamed Faouzi Atig and Ngo Tuan
		  Phong},
  booktitle	= {CONF_ESOP 2015},
  year		= {2015},
  editor	= {Jan Vitek},
  series	= {LNCS},
  volume	= {9032},
  pages		= {308--332},
  publisher	= {Springer},
  doi		= {10.1007/978-3-662-46669-8_13}
}

@InProceedings{	  abdulla2015:stateless-tso,
  title		= {Stateless model checking for {TSO} and {PSO}},
  author	= {Parosh Aziz Abdulla and Stavros Aronis and Mohamed Faouzi
		  Atig and Bengt Jonsson and Carl Leonardsson and
		  Konstantinos Sagonas},
  booktitle	= {CONF_TACAS 2015},
  year		= {2015},
  series	= {LNCS},
  volume	= {9035},
  pages		= {353--367},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  url		= {http://dx.doi.org/10.1007/978-3-662-46681-0_28},
  doi		= {10.1007/978-3-662-46681-0_28},
  ids		= {nidhugg-tso,stateless-tso}
}

@InProceedings{	  abdulla2016:power,
  title		= {Stateless model checking for {POWER}},
  author	= {Parosh Aziz Abdulla and Mohamed Faouzi Atig and Bengt
		  Jonsson and Carl Leonardsson},
  booktitle	= {CONF_CAV 2016},
  year		= {2016},
  series	= {LNCS},
  volume	= {9780},
  pages		= {134--156},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  url		= {https://doi.org/10.1007/978-3-319-41540-6_8},
  doi		= {10.1007/978-3-319-41540-6_8},
  ids		= {nidhugg-power,stateless-power}
}

@InProceedings{	  abdulla2017:context-bounded-power,
  title		= {Context-Bounded Analysis for POWER},
  author	= {Abdulla, Parosh Aziz and Atig, Mohamed Faouzi and
		  Bouajjani, Ahmed and Ngo, Tuan Phong},
  booktitle	= {CONF_TACAS 2017},
  year		= {2017},
  editor	= {Legay, Axel and Margaria, Tiziana},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {56--74},
  doi		= {10.1007/978-3-662-54580-5_4}
}

@Article{	  abdulla2017:optimal-dpor-journal,
  title		= {Source sets: {A} foundation for optimal dynamic partial
		  order reduction},
  author	= {Abdulla, Parosh Aziz and Aronis, Stavros and Jonsson,
		  Bengt and Sagonas, Konstantinos},
  journal	= {JNL_JACM},
  year		= {2017},
  month		= sep,
  issue_date	= {September 2017},
  volume	= {64},
  number	= {4},
  pages		= {25:1--25:49},
  articleno	= {25},
  numpages	= {49},
  doi		= {10.1145/3073408},
  acmid		= {3073408},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {optimal-dpor-journal, optimal-dpor-jacm}
}

@Article{	  abdulla2018:tracer,
  title		= {Optimal stateless model checking under the release-acquire
		  semantics},
  author	= {Abdulla, Parosh Aziz and Atig, Mohamed Faouzi and Jonsson,
		  Bengt and Ngo, Tuan Phong},
  journal	= {JNL_PACMPL},
  year		= {2018},
  month		= oct,
  issue_date	= {November 2018},
  volume	= {2},
  number	= {OOPSLA},
  pages		= {135:1--135:29},
  articleno	= {135},
  numpages	= {29},
  url		= {http://doi.acm.org/10.1145/3276505},
  doi		= {10.1145/3276505},
  acmid		= {3276505},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {C/C++11, Release-Acquire, software model checking, weak
		  memory models},
  ids		= {tracer}
}

@Article{	  abdulla2019:smc-rf,
  title		= {Optimal stateless model checking for reads-from
		  equivalence under sequential consistency},
  author	= {Abdulla, Parosh Aziz and Atig, Mohamed Faouzi and Jonsson,
		  Bengt and Lång, Magnus and Ngo, Tuan Phong and Sagonas,
		  Konstantinos},
  journal	= {JNL_PACMPL},
  volume	= {3},
  url		= {https://doi.org/10.1145/3360576},
  doi		= {10.1145/3360576},
  pages		= {150:1--150:29},
  issue		= {{OOPSLA}},
  urldate	= {2021-01-18},
  date		= {2019-10-10},
  keywords	= {stateless model checking, sequential consistency, program
		  verification, concurrent programs, dynamic partial order
		  reduction},
  ids		= {nidhugg-rf}
}

@InProceedings{	  abdulla2021:ps-decidability,
  title		= {The Decidability of Verification under {PS 2.0}},
  author	= {Abdulla, Parosh Aziz and Atig, Mohamed Faouzi and Godbole,
		  Adwait and Krishna, S. and Vafeiadis, Viktor},
  booktitle	= {CONF_ESOP 2021},
  year		= {2021},
  editor	= {Yoshida, Nobuko},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {1--29}
}

@InProceedings{	  abdulla2024:parsimonious-dpor,
  title		= {Parsimonious Optimal Dynamic Partial Order Reduction},
  author	= {Abdulla, Parosh Aziz and Atig, Mohamed Faouzi and Das,
		  Sarbojit and Jonsson, Bengt and Sagonas, Konstantinos},
  booktitle	= {CONF_CAV 2024},
  year		= {2024},
  opteditor	= {Gurfinkel, Arie and Ganesh, Vijay},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {19--43},
  isbn		= {978-3-031-65630-9},
  doi		= {10.1007/978-3-031-65630-9_2},
  ids		= {parsimonious-dpor}
}

@InCollection{	  aceto2011:tso-robustness,
  title		= {Deciding robustness against total store ordering},
  author	= {Bouajjani, Ahmed and Meyer, Roland and Möhlmann, Eike},
  booktitle	= {CONF_ICALP 2011},
  year		= {2011},
  volume	= {6756},
  series	= {LNCS},
  editor	= {Aceto, Luca and Henzinger, Monika and Sgall, Jiří},
  doi		= {10.1007/978-3-642-22012-8_34},
  publisher	= {Springer},
  pages		= {428-440},
  language	= {English}
}

@Article{	  adve1996:wmm-tutorial,
  title		= {Shared memory consistency models: {A} tutorial},
  author	= {Adve, Sarita V. and Gharachorloo, Kourosh},
  journal	= {JNL_COMPUTER},
  year		= {1996},
  month		= dec,
  issue_date	= {December 1996},
  volume	= {29},
  number	= {12},
  pages		= {66--76},
  publisher	= {IEEE Computer Society Press}
}

@InProceedings{	  agarwal2021:vcdpor-rf,
  title		= {Stateless Model Checking Under a Reads-Value-From
		  Equivalence},
  author	= {Agarwal, Pratyush and Chatterjee, Krishnendu and Pathak,
		  Shreya and Pavlogiannis, Andreas and Toman, Viktor},
  booktitle	= {CONF_CAV 2021},
  year		= {2021},
  month		= jul,
  editor	= {Silva, Alexandra and Leino, K. Rustan M.},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {341--366},
  doi		= {10.1007/978-3-030-81685-8_16},
  ids		= {vcdpor-rf}
}

@Article{	  aghayev2017:evolving-ext4,
  title		= {Evolving {ext4} for shingled disks},
  author	= {Aghayev, Abutalib and Ts’o, Theodore and Gibson, Garth
		  and Desnoyers, Peter},
  booktitle	= {CONF_FAST 2017},
  year		= 2017,
  pages		= {17},
  langid	= {english}
}

@Article{	  ahamad1995:causal,
  title		= {Causal memory: {Definitions}, implementation, and
		  programming},
  author	= {Ahamad, Mustaque and Neiger, Gil and Burns, JamesE. and
		  Kohli, Prince and Hutto, PhillipW.},
  journal	= {JNL_DC},
  year		= {1995},
  volume	= {9},
  number	= {1},
  doi		= {10.1007/BF01784241},
  publisher	= {Springer},
  pages		= {37--49},
  language	= {English}
}

@InProceedings{	  ahmed2006:relations-types,
  title		= {Step-indexed syntactic logical relations for recursive and
		  quantified types},
  author	= {Amal Ahmed},
  booktitle	= {CONF_ESOP 2006},
  year		= 2006
}

@InProceedings{	  albert2017:csdpor,
  title		= {Context-sensitive dynamic partial order reduction},
  author	= {Albert, Elvira and Arenas, Puri and de la Banda, María
		  García and Gómez-Zamalloa, Miguel and Stuckey, Peter J.},
  booktitle	= {CONF_CAV 2017},
  year		= {2017},
  editor	= {Majumdar, Rupak and Kunčak, Viktor},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {526--543},
  doi		= {10.1007/978-3-319-63387-9_26},
  ids		= {csdpor}
}

@InProceedings{	  albert2018:cdpor,
  title		= {Constrained dynamic partial order reduction},
  author	= {Albert, Elvira and Gómez-Zamalloa, Miguel and Isabel,
		  Miguel and Rubio, Albert},
  booktitle	= {CONF_CAV 2018},
  year		= {2018},
  editor	= {Chockler, Hana and Weissenbacher, Georg},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {392--410},
  doi		= {10.1007/978-3-319-96142-2_24},
  ids		= {cdpor}
}

@InProceedings{	  alglave2013:bmc,
  title		= {Partial orders for efficient bounded model checking of
		  concurrent software},
  author	= {Jade Alglave and Daniel Kroening and Michael Tautschnig},
  booktitle	= {CONF_CAV 2013},
  year		= {2013},
  pages		= {141--157},
  editor	= {Natasha Sharygina and Helmut Veith},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  series	= {LNCS},
  volume	= {8044},
  doi		= {10.1007/978-3-642-39799-8_9}
}

@InProceedings{	  alglave2013:transformations,
  title		= {Software verification for weak memory via program
		  transformation},
  author	= {Alglave, Jade and Kroening, Daniel and Nimal, Vincent and
		  Tautschnig, Michael},
  booktitle	= {CONF_ESOP 2013},
  year		= {2013},
  location	= {Rome, Italy},
  pages		= {512--532},
  numpages	= {21},
  url		= {http://dx.doi.org/10.1007/978-3-642-37036-6_28},
  doi		= {10.1007/978-3-642-37036-6_28},
  acmid		= {2450306},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg}
}

@InProceedings{	  alglave2013:weakness,
  title		= {Weakness is a virtue (position paper)},
  author	= {Alglave, Jade},
  booktitle	= {CONF_EC2 2013},
  year		= {2013},
  url		= {http://www0.cs.ucl.ac.uk/staff/j.alglave/papers/ec213.pdf}
}

@Article{	  alglave2014:herding-cats,
  title		= {Herding cats: {Modelling}, simulation, testing, and data
		  mining for weak memory},
  author	= {Alglave, Jade and Maranget, Luc and Tautschnig, Michael},
  journal	= {JNL_TOPLAS},
  year		= {2014},
  month		= jul,
  issue_date	= {July 2014},
  volume	= {36},
  number	= {2},
  pages		= {7:1--7:74},
  articleno	= {7},
  numpages	= {74},
  url		= {http://doi.acm.org/10.1145/2627752},
  doi		= {10.1145/2627752},
  acmid		= {2627752},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {Concurrency, software verification, weak memory models},
  ids		= {herding-cats, herd}
}

@InProceedings{	  alglave2017:ogre,
  title		= {{Ogre} and {Pythia}: an invariance proof method for weak
		  consistency models},
  author	= {Jade Alglave and Patrick Cousot},
  booktitle	= {CONF_POPL 2017},
  year		= {2017},
  editor	= {Giuseppe Castagna and Andrew D. Gordon},
  pages		= {3--18},
  publisher	= {{ACM}},
  url		= {http://dl.acm.org/citation.cfm?id=3009883}
}

@InProceedings{	  alglave2018:lkmm,
  title		= {Frightening small children and disconcerting grown-ups:
		  {Concurrency} in the Linux kernel},
  author	= {Alglave, Jade and Maranget, Luc and McKenney, Paul E. and
		  Parri, Andrea and Stern, Alan},
  booktitle	= {CONF_ASPLOS 2018},
  year		= {2018},
  location	= {Williamsburg, VA, USA},
  pages		= {405--418},
  numpages	= {14},
  url		= {http://doi.acm.org/10.1145/3173162.3177156},
  doi		= {10.1145/3173162.3177156},
  acmid		= {3177156},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {formal models, linux, memory model, shared memory
		  parallelism},
  ids		= {lkmm}
}

@Article{	  alglave2021:armed-cats,
  title		= {Armed Cats: Formal Concurrency Modelling at Arm},
  author	= {Alglave, Jade and Deacon, Will and Grisenthwaite, Richard
		  and Hacquard, Antoine and Maranget, Luc},
  journal	= {ACM Trans. Program. Lang. Syst.},
  year		= {2021},
  month		= {jul},
  issue_date	= {June 2021},
  publisher	= {Association for Computing Machinery},
  address	= {New York, NY, USA},
  volume	= {43},
  number	= {2},
  issn		= {0164-0925},
  url		= {https://doi.org/10.1145/3458926},
  doi		= {10.1145/3458926},
  articleno	= {8},
  numpages	= {54},
  ids		= {armed-cats},
  keywords	= {Concurrency Weak memory models, arm architecture, linux,
		  mixed-size accesses}
}

@InProceedings{	  amit2007:linearizability,
  title		= {Comparison under abstraction for verifying
		  linearizability},
  author	= {Daphna Amit and Noam Rinetzky and Tom Reps and Mooly Sagiv
		  and Eran Yahav},
  booktitle	= {CONF_CAV 2007},
  year		= {2007},
  pages		= {477-490},
  editor	= {Werner Damm and Holger Hermanns},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {4590},
  doi		= {10.1007/978-3-540-73368-3_49}
}

@InProceedings{	  anand2007:symbolic-jpf,
  title		= {{JPF--SE}: {A} Symbolic Execution Extension to Java
		  PathFinder},
  author	= {Anand, Saswat and Păsăreanu, Corina S. and Visser,
		  Willem},
  booktitle	= {CONF_TACAS 2007},
  year		= {2007},
  opteditor	= {Grumberg, Orna and Huth, Michael},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {134--138},
  isbn		= {978-3-540-71209-1},
  doi		= {10.1007/978-3-540-71209-1_12},
  ids		= {symbolic-jpf}
}

@InProceedings{	  anderson2014:netkat,
  title		= {{NetkAT}: semantic foundations for networks},
  author	= {Carolyn Jane Anderson and Nate Foster and Arjun Guha and
		  Jean{-}Baptiste Jeannin and Dexter Kozen and Cole
		  Schlesinger and David Walker},
  booktitle	= {CONF_POPL 2014, 2014},
  year		= {2014},
  editor	= {Suresh Jagannathan and Peter Sewell},
  pages		= {113--126},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2535838.2535862},
  doi		= {10.1145/2535838.2535862}
}

@Misc{		  anonymized,
  title		= {ANONYMIZED_TITLE},
  author	= {ANONYMIZED_AUTHOR},
  year		= {CURRENT_YEAR}
}

@Article{	  appel2001:proof-carrying,
  title		= {An indexed model of recursive types for foundational
		  proof-carrying code},
  author	= {Andrew W. Appel and David McAllester},
  journal	= {JNL_TOPLAS},
  year		= 2001,
  volume	= 23,
  number	= 5,
  pages		= {657--683}
}

@InProceedings{	  appel2007:vmm,
  title		= {A very modal model of a modern, major, general type
		  system},
  author	= {Andrew W. Appel and Paul-André Melliès and Christopher
		  D. Richards and Jérôme Vouillon},
  booktitle	= {CONF_POPL 2007},
  year		= 2007
}

@Misc{		  appendix,
  title		= {APPENDIX_TITLE},
  author	= {APPENDIX_AUTHOR},
  year		= {CURRENT_YEAR}
}

@InProceedings{	  arcangeli2003:rcu-systemv,
  title		= {Using read-copy-update techniques for {System V IPC} in
		  the {Linux} 2.5 kernel},
  author	= {Andrea Arcangeli and Mingming Cao and Paul E. McKenney and
		  Dipankar Sarma},
  booktitle	= {CONF_FREENIX 2003},
  year		= {2003},
  pages		= {297-309},
  publisher	= {USENIX}
}

@InProceedings{	  aronis2018:observers,
  title		= {Optimal dynamic partial order reduction with observers},
  author	= {Stavros Aronis and Bengt Jonsson and Magnus Lång and
		  Konstantinos Sagonas},
  booktitle	= {CONF_TACAS 2018},
  year		= {2018},
  series	= {LNCS},
  volume	= {10806},
  pages		= {229--248},
  publisher	= {Springer},
  doi		= {10.1007/978-3-319-89963-3_14},
  ids		= {dpor-observers, observers, nidhugg-observers,
		  Nidhugg-observers}
}

@InProceedings{	  atig2010:reachability,
  title		= {On the verification problem for weak memory models},
  author	= {Mohamed Faouzi Atig and Ahmed Bouajjani and Sebastian
		  Burckhardt and Madanlal Musuvathi},
  booktitle	= {CONF_POPL 2010},
  year		= {2010},
  pages		= {7-18},
  publisher	= {ACM}
}

@InProceedings{	  atig2014:context-bounded-tso,
  title		= {Context-Bounded Analysis of TSO Systems},
  author	= {Atig, Mohamed Faouzi and Bouajjani, Ahmed and Parlato,
		  Gennaro},
  booktitle	= {CONF_FPS 2014},
  year		= {2014},
  editor	= {Bensalem, Saddek and Lakhneck, Yassine and Legay, Axel},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {21--38},
  doi		= {10.1007/978-3-642-54848-2_2}
}

@InProceedings{	  aydemir2005:poplmark,
  title		= {Mechanized metatheory for the masses: {The} {POPLmark}
		  challenge},
  author	= {Brian E. Aydemir and Aaron Bohannon and Matthew Fairbairn
		  and J. Nathan Foster and Benjamin C. Pierce and Peter
		  Sewell and Dimitrios Vytiniotis and Geoffrey Washburn and
		  Stephanie Weirich and Steve Zdancewic},
  booktitle	= {CONF_TPHOLS 2005},
  year		= {2005},
  ids		= {poplmark}
}

@InProceedings{	  aydemir2008:metatheory,
  title		= {Engineering formal metatheory},
  author	= {Brian Aydemir and Arthur Charguéraud and Benjamin C.
		  Pierce and Randy Pollack and Stephanie Weirich},
  booktitle	= {CONF_POPL 2008},
  year		= {2008}
}

@InProceedings{	  ball2004:slam,
  title		= {{SLAM} and {Static Driver Verifier}: Technology Transfer
		  of Formal Methods inside {Microsoft}},
  author	= {Ball, Thomas and Cook, Byron and Levin, Vladimir and
		  Rajamani, Sriram K.},
  booktitle	= {CONF_IFM 2004},
  year		= {2004},
  editor	= {Boiten, Eerke A. and Derrick, John and Smith, Graeme},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {1--20},
  doi		= {10.1007/978-3-540-24756-2_1},
  ids		= {slam}
}

@InProceedings{	  baranova2017:divine,
  title		= {Model Checking of {C} and {C}++ with {DIVINE} 4},
  author	= {Zuzana Baranová and Jiří Barnat and Katarína Kejstová
		  and Tadeáš Kučera and Henrich Lauko and Jan Mrázek and
		  Petr Ročkai and Vladimír Štill},
  booktitle	= {CONF_ATVA 2017},
  year		= 2017,
  pages		= {201-207},
  volume	= 10482,
  series	= {LNCS},
  publisher	= {Springer},
  doi		= {10.1007/978-3-319-68167-2_14},
  ids		= {divine}
}

@InProceedings{	  barnat2014:underapproximated-tso,
  title		= {{LTL} model checking of parallel programs with
		  under-approximated {TSO} memory model},
  author	= {Barnat, J. and Brim, L. and Havel, V.},
  booktitle	= {CONF_ACSD 2013},
  year		= {2013},
  month		= jul,
  pages		= {51-59},
  doi		= {10.1109/ACSD.2013.8}
}

@InBook{	  basin2018:mc-protocols,
  title		= {Model Checking Security Protocols},
  author	= {Basin, David and Cremers, Cas and Meadows, Catherine},
  booktitle	= {Handbook of Model Checking},
  year		= {2018},
  editor	= {Clarke, Edmund M. and Henzinger, Thomas A. and Veith,
		  Helmut and Bloem, Roderick},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {727--762},
  doi		= {10.1007/978-3-319-10575-8_22},
  url		= {https://doi.org/10.1007/978-3-319-10575-8_22}
}

@InProceedings{	  batty2011:mathematizing,
  title		= {Mathematizing {C++} concurrency},
  author	= {Batty, Mark and Owens, Scott and Sarkar, Susmit and
		  Sewell, Peter and Weber, Tjark},
  booktitle	= {CONF_POPL 2011},
  year		= {2011},
  location	= {Austin, Texas, USA},
  pages		= {55--66},
  url		= {http://doi.acm.org/10.1145/1926385.1926394},
  doi		= {10.1145/1926385.1926394},
  acmid		= {1926394},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {relaxed memory models, semantics},
  ids		= {c11-batty, batty-c11}
}

@InProceedings{	  batty2012:c11-power,
  title		= {Clarifying and compiling {C/C++} concurrency: {From}
		  {C++11} to {POWER}},
  author	= {Batty, Mark and Memarian, Kayvan and Owens, Scott and
		  Sarkar, Susmit and Sewell, Peter},
  booktitle	= {CONF_POPL 2012},
  year		= {2012},
  location	= {Philadelphia, PA, USA},
  pages		= {509--520},
  numpages	= {12},
  url		= {http://doi.acm.org/10.1145/2103656.2103717},
  doi		= {10.1145/2103656.2103717},
  acmid		= {2103717},
  publisher	= {ACM},
  optaddress	= {New York, NY, USA},
  keywords	= {relaxed memory models, semantics}
}

@InProceedings{	  batty2013:abstraction,
  title		= {Library abstraction for {C/C++} concurrency},
  author	= {Mark Batty and Mike Dodds and Alexey Gotsman},
  booktitle	= {CONF_POPL 2013},
  year		= {2013},
  pages		= {235--248},
  publisher	= {ACM},
  doi		= {10.1145/2429069.2429099}
}

@InProceedings{	  batty2015:semantics,
  title		= {The problem of programming language concurrency
		  semantics},
  author	= {Mark Batty and Kayvan Memarian and Kyndylan Nienhuis and
		  Jean Pichon-Pharabod and Peter Sewell},
  booktitle	= {CONF_ESOP 2015},
  year		= {2015},
  series	= {LNCS},
  volume	= {9032},
  pages		= {283--307},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  url		= {http://dx.doi.org/10.1007/978-3-662-46669-8_12}
}

@InProceedings{	  batty2016:sc-atomics,
  title		= {Overhauling {SC} atomics in {C11} and {OpenCL}},
  author	= {Mark Batty and Alastair F. Donaldson and John Wickerson},
  booktitle	= {CONF_POPL 2016},
  year		= {2016},
  pages		= {634--648},
  publisher	= {{ACM}},
  ids		= {sc-atomics}
}

@InProceedings{	  berdine2005:smallfoot,
  title		= {Smallfoot: {Modular} automatic assertion checking with
		  separation logic},
  author	= {Josh Berdine and Cristiano Calcagno and Peter W. O'Hearn},
  booktitle	= {CONF_FMCO 2015},
  year		= {2005},
  pages		= {115--137},
  editor	= {Frank S. de Boer and Marcello M. Bonsangue and Susanne
		  Graf and Willem P. de Roever},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {4111}
}

@Article{	  birkedal2010:ultrametric,
  title		= {The category-theoretic solution of recursive metric-space
		  equations},
  author	= {Lars Birkedal and Kristian Støvring and Jacob Thamsborg},
  journal	= {JNL_TCS},
  year		= {2010},
  volume	= {411},
  number	= {47},
  pages		= {4102-4122},
  publisher	= {Elsevier}
}

@Article{	  biswas2019:transactions,
  title		= {On the Complexity of Checking Transactional Consistency},
  author	= {Biswas, Ranadeep and Enea, Constantin},
  journal	= {JNL_PACMPL},
  year		= {2019},
  month		= oct,
  issue_date	= {October 2019},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {3},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3360591},
  doi		= {10.1145/3360591},
  articleno	= {165},
  numpages	= {28},
  keywords	= {consistency, axiomatic specifications, transactional
		  databases, testing}
}

@InProceedings{	  blanchet2012:protocol-verification,
  title		= {Security Protocol Verification: Symbolic and Computational
		  Models},
  author	= {Blanchet, Bruno},
  booktitle	= {CONF_POST 2012},
  year		= {2012},
  editor	= {Degano, Pierpaolo and Guttman, Joshua D.},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {3--29},
  doi		= {10.1007/978-3-642-28641-4_2},
  url		= {https://doi.org/10.1007/978-3-642-28641-4_2}
}

@InProceedings{	  boehm2008:model,
  title		= {Foundations of the {C++} concurrency memory model},
  author	= {Hans-Juergen Boehm and Sarita V. Adve},
  booktitle	= {CONF_PLDI 2008},
  year		= {2008},
  pages		= {68--78},
  publisher	= {ACM},
  doi		= {10.1145/1375581.1375591}
}

@InProceedings{	  boehm2012:seqlock,
  title		= {Can seqlocks get along with programming language memory
		  models?},
  author	= {Hans-Juergen Boehm},
  booktitle	= {CONF_MSPC 2012},
  year		= {2012},
  pages		= {12--20},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2247684.2247688},
  doi		= {10.1145/2247684.2247688},
  ids		= {seqlock, seqlocks}
}

@Misc{		  boehm2013:oota,
  title		= {{N3710}: {Specifying} the absence of ``out of thin air''
		  results},
  author	= {Hans-Juergen Boehm},
  year		= {2013}
}

@InProceedings{	  boehm2014:ghosts,
  title		= {Outlawing ghosts: {Avoiding} out-of-thin-air results},
  author	= {Boehm, Hans-Juergen and Demsky, Brian},
  booktitle	= {CONF_MSPC 2014},
  year		= {2014},
  location	= {Edinburgh, United Kingdom},
  pages		= {7:1--7:6},
  articleno	= {7},
  numpages	= {6},
  url		= {http://doi.acm.org/10.1145/2618128.2618134},
  doi		= {10.1145/2618128.2618134},
  acmid		= {2618134},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {C++, Java, atomic operations, relaxed memory models}
}

@Misc{		  boehm2020:relaxed-guide,
  title		= {{P2055R0}: {A} Relaxed Guide to {memory\_order\_relaxed}},
  author	= {Hans Boehm and Paul E. McKenney},
  year		= {2020},
  url		= {http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p2055r0.pdf},
  urldate	= {2021-04-12}
}

@InProceedings{	  bonchi2013:congruence,
  title		= {Checking {NFA} equivalence with bisimulations up to
		  congruence},
  author	= {Filippo Bonchi and Damien Pous},
  booktitle	= {CONF_POPL 2013},
  year		= {2013},
  editor	= {Roberto Giacobazzi and Radhia Cousot},
  pages		= {457--468},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2429069.2429124},
  doi		= {10.1145/2429069.2429124}
}

@Misc{		  bonwick2005:zfs,
  title		= {{ZFS}: The last word in filesystems},
  author	= {Bonwick, Jeff},
  year		= {2005},
  month		= oct,
  url		= {https://blogs.oracle.com/bonwick/zfs%3A-the-last-word-in-filesystems},
  shorttitle	= {{ZFS}},
  day		= {31},
  urldate	= {2020-06-17},
  note		= {Library Catalog: blogs.oracle.com},
  ids		= {zfs}
}

@Misc{		  book:circular-buffers,
  title		= {Circular buffers},
  author	= {David Howells and Paul E. McKenney},
  url		= {https://www.kernel.org/doc/Documentation/circular-buffers.txt}
}

@Book{		  book:herlihy-shavit,
  title		= {The art of multiprocessor programming},
  author	= {Herlihy, Maurice and Shavit, Nir},
  year		= {2008},
  optpublisher	= {Morgan Kaufmann Publishers Inc.}
}

@Book{		  book:tbb,
  title		= {Intel Threading Building Blocks},
  author	= {James Reinders},
  year		= {2007},
  optpublisher	= {O'Reilly Media, Inc.},
  ids		= {tbb}
}

@Book{		  book:tla,
  title		= {Specifying Systems, The {TLA+} Language and Tools for
		  Hardware and Software Engineers},
  author	= {Leslie Lamport},
  year		= {2002},
  publisher	= {Addison-Wesley},
  url		= {http://research.microsoft.com/users/lamport/tla/book.html},
  isbn		= {0-3211-4306-X}
}

@InCollection{	  borger2008:flash-alloy,
  title		= {Formal modeling and analysis of a flash filesystem in
		  {Alloy}},
  author	= {Kang, Eunsuk and Jackson, Daniel},
  booktitle	= {CONF_ABZ 2008},
  location	= {Berlin, Heidelberg},
  volume	= {5238},
  url		= {http://link.springer.com/10.1007/978-3-540-87603-8_23},
  pages		= {294--308},
  publisher	= {Springer},
  editor	= {Börger, Egon and Butler, Michael and Bowen, Jonathan P.
		  and Boca, Paul},
  urldate	= {2020-06-17},
  date		= {2008},
  langid	= {english},
  doi		= {10.1007/978-3-540-87603-8_23}
}

@Article{	  bornat2006:variables-sep,
  title		= {Variables as resource in separation logic},
  author	= {Bornat, Richard and Calcagno, Cristiano and Yang,
		  Hongseok},
  journal	= {JNL_ENTCS},
  year		= {2006},
  volume	= {155},
  pages		= {247--276},
  publisher	= {Elsevier}
}

@Article{	  bornholt2016:specifying,
  title		= {Specifying and checking file system crash-consistency
		  models},
  author	= {Bornholt, James and Kaufmann, Antoine and Li, Jialin and
		  Krishnamurthy, Arvind and Torlak, Emina and Wang, Xi},
  year		= {2016},
  journaltitle	= {CONF_ASPLOS 2016},
  volume	= {44},
  url		= {https://doi.org/10.1145/2980024.2872406},
  doi		= {10.1145/2980024.2872406},
  pages		= {83--98},
  number	= {2},
  keywords	= {verification, crash consistency, file systems},
  ids		= {ferrite}
}

@InProceedings{	  bornholt2017:memsynth,
  title		= {Synthesizing memory models from framework sketches and
		  Litmus tests},
  author	= {James Bornholt and Emina Torlak},
  booktitle	= {CONF_PLDI 2017},
  year		= {2017},
  editor	= {Albert Cohen and Martin T. Vechev},
  pages		= {467--481},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/3062341.3062353},
  doi		= {10.1145/3062341.3062353}
}

@InProceedings{	  bornholt2017:synthesizing,
  title		= {Synthesizing memory models from framework sketches and
		  litmus tests},
  author	= {Bornholt, James and Torlak, Emina},
  booktitle	= {CONF_PLDI 2017},
  year		= {2017},
  location	= {Barcelona, Spain},
  pages		= {467--481},
  numpages	= {15},
  url		= {http://doi.acm.org/10.1145/3062341.3062353},
  doi		= {10.1145/3062341.3062353},
  acmid		= {3062353},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {program synthesis, weak memory models},
  ids		= {memsynth}
}

@InProceedings{	  bouajjani2011:robustness,
  title		= {Deciding robustness against total store ordering},
  author	= {Ahmed Bouajjani and Roland Meyer and Eike Möhlmann},
  booktitle	= {CONF_ICALP 2011},
  year		= {2011},
  pages		= {428-440},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {6756}
}

@InProceedings{	  bouajjani2013:robustness,
  title		= {Checking and enforcing robustness against {TSO}},
  author	= {Ahmed Bouajjani and Egor Derevenetc and Roland Meyer},
  booktitle	= {CONF_ESOP 2013},
  year		= {2013},
  pages		= {533-553},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {7792}
}

@InProceedings{	  bouajjani2017:causal,
  title		= {On verifying causal consistency},
  author	= {Bouajjani, Ahmed and Enea, Constantin and Guerraoui,
		  Rachid and Hamza, Jad},
  booktitle	= {CONF_POPL 2017},
  year		= {2017},
  location	= {Paris, France},
  pages		= {626--638},
  numpages	= {13},
  url		= {http://doi.acm.org/10.1145/3009837.3009888},
  doi		= {10.1145/3009837.3009888},
  acmid		= {3009888},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {causal consistency, distributed systems, model checking,
		  static program analysis}
}

@Article{	  bouajjani2023:transactions,
  title		= {Dynamic Partial Order Reduction for Checking Correctness
		  against Transaction Isolation Levels},
  author	= {Bouajjani, Ahmed and Enea, Constantin and Román-Calvo,
		  Enrique},
  journal	= {JNL_PACMPL},
  year		= {2023},
  month		= {jun},
  issue_date	= {June 2023},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {7},
  number	= {PLDI},
  url		= {https://doi.org/10.1145/3591243},
  doi		= {10.1145/3591243},
  articleno	= {129},
  numpages	= {26}
}

@InProceedings{	  boyland2003:interference,
  title		= {Checking interference with fractional permissions},
  author	= {Boyland, John},
  booktitle	= {CONF_SAS 2003},
  year		= {2003},
  series	= {LNCS},
  volume	= {2694},
  pages		= {55--72},
  publisher	= {Springer}
}

@InProceedings{	  bracha1990:inheritance,
  title		= {Mixin-based Inheritance},
  author	= {Gilad Bracha and William Cook},
  booktitle	= {CONF_OOPSLA 1990},
  year		= 1990
}

@InProceedings{	  bracha1992:modularity,
  title		= {Modularity Meets Inheritance},
  author	= {Gilad Bracha and Gary Lindstrom},
  booktitle	= {CONF_ICCL 1992},
  year		= 1992
}

@InProceedings{	  brookes2004:csep,
  title		= {A semantics for concurrent separation logic},
  author	= {Stephen D. Brookes},
  booktitle	= {CONF_CONCUR 2004},
  year		= {2004},
  pages		= {16-34},
  editor	= {Philippa Gardner and Nobuko Yoshida},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {3170}
}

@InProceedings{	  brunet2014:kac,
  title		= {Kleene Algebra with Converse},
  author	= {Paul Brunet and Damien Pous},
  booktitle	= {CONF_RAMICS 2014},
  year		= {2014},
  editor	= {Peter Höfner and Peter Jipsen and Wolfram Kahl and Martin
		  Eric Müller},
  series	= {LNCS},
  volume	= {8428},
  pages		= {101--118},
  publisher	= {Springer},
  doi		= {10.1007/978-3-319-06251-8_7}
}

@InProceedings{	  brunet2015:ka-allegories,
  title		= {Petri Automata for Kleene Allegories},
  author	= {Paul Brunet and Damien Pous},
  booktitle	= {CONF_LICS 2015},
  year		= {2015},
  pages		= {68--79},
  publisher	= {{IEEE} Computer Society},
  url		= {https://doi.org/10.1109/LICS.2015.17},
  doi		= {10.1109/LICS.2015.17}
}

@InProceedings{	  bucur2011:cloud9,
  title		= {Parallel symbolic execution for automated real-world
		  software testing},
  author	= {Bucur, Stefan and Ureche, Vlad and Zamfir, Cristian and
		  Candea, George},
  booktitle	= {CONF_EUROSYS 2011},
  year		= {2011},
  isbn		= {9781450306348},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/1966445.1966463},
  doi		= {10.1145/1966445.1966463},
  pages		= {183–198},
  numpages	= {16},
  location	= {Salzburg, Austria},
  optseries	= {EuroSys '11}
}

@InProceedings{	  bugliesi2013:resource,
  title		= {Logical Foundations of Secure Resource Management},
  author	= {Michele Bugliesi and Stefano Calzavara and Fabienne Eigner
		  and Matteo Maffei},
  booktitle	= {CONF_POST 2013},
  year		= {2013}
}

@Article{	  bui2021:rf-tso,
  title		= {The Reads-from Equivalence for the TSO and PSO Memory
		  Models},
  author	= {Bui, Truc Lam and Chatterjee, Krishnendu and Gautam,
		  Tushar and Pavlogiannis, Andreas and Toman, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2021},
  month		= oct,
  issue_date	= {October 2021},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {5},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3485541},
  doi		= {10.1145/3485541},
  articleno	= {164},
  numpages	= {30},
  keywords	= {relaxed memory models, stateless model checking,
		  concurrency, execution-consistency verification},
  ids		= {dpor-rf-tso}
}

@InProceedings{	  burch1990:circuit-verification,
  title		= {Sequential Circuit Verification Using Symbolic Model
		  Checking},
  author	= {Burch, J. R. and Clarke, E. M. and McMillan, K. L. and
		  Dill, David L.},
  booktitle	= {CONF_DAC 1990},
  year		= {1990},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/123186.123223},
  doi		= {10.1145/123186.123223},
  pages		= {46–51},
  numpages	= {6},
  location	= {Orlando, Florida, USA}
}

@InProceedings{	  burckhardt2006:bmc,
  title		= {Bounded Model Checking of Concurrent Data Types on Relaxed
		  Memory Models: {A} Case Study},
  author	= {Sebastian Burckhardt and Rajeev Alur and Milo M. K.
		  Martin},
  booktitle	= {CONF_CAV 2006},
  year		= {2006},
  editor	= {Thomas Ball and Robert B. Jones},
  series	= {LNCS},
  volume	= {4144},
  pages		= {489--502},
  publisher	= {Springer},
  doi		= {10.1007/11817963_45}
}

@InProceedings{	  burckhardt2007:checkfence,
  title		= {{CheckFence}: {Checking} consistency of concurrent data
		  types on relaxed memory models},
  author	= {Sebastian Burckhardt and Rajeev Alur and Milo M. K.
		  Martin},
  booktitle	= {CONF_PLDI 2007},
  year		= {2007},
  pages		= {12-21},
  editor	= {Jeanne Ferrante and Kathryn S. McKinley},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  doi		= {10.1145/1250734.1250737},
  ids		= {checkfence}
}

@InProceedings{	  burckhardt2008:sober,
  title		= {Effective Program Verification for Relaxed Memory Models},
  author	= {Sebastian Burckhardt and Madanlal Musuvathi},
  booktitle	= {CONF_CAV 2008},
  year		= {2008},
  editor	= {Aarti Gupta and Sharad Malik},
  series	= {LNCS},
  volume	= {5123},
  pages		= {107--120},
  publisher	= {Springer},
  doi		= {10.1007/978-3-540-70545-1_12}
}

@InProceedings{	  burckhardt2008:verification-relaxed,
  title		= {Effective program verification for relaxed memory models},
  author	= {Sebastian Burckhardt and Madanlal Musuvathi},
  booktitle	= {CONF_CAV 2008},
  year		= {2008},
  pages		= {107-120},
  editor	= {Aarti Gupta and Sharad Malik},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {5123}
}

@InProceedings{	  burckhardt2010:lineup,
  title		= {{Line-Up}: {A} complete and automatic linearizability
		  checker},
  author	= {Sebastian Burckhardt and Chris Dern and Madanlal Musuvathi
		  and Roy Tan},
  booktitle	= {CONF_PLDI 2010},
  year		= {2010},
  editor	= {Benjamin G. Zorn and Alexander Aiken},
  pages		= {330-340},
  publisher	= {ACM},
  doi		= {10.1145/1806596.1806634}
}

@InProceedings{	  burckhardt2010:pct,
  title		= {A randomized scheduler with probabilistic guarantees of
		  finding bugs},
  author	= {Burckhardt, Sebastian and Kothari, Pravesh and Musuvathi,
		  Madanlal and Nagarakatte, Santosh},
  booktitle	= {CONF_ASPLOS 2010},
  year		= {2010},
  isbn		= {9781605588391},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/1736020.1736040},
  doi		= {10.1145/1736020.1736040},
  pages		= {167–178},
  numpages	= {12},
  keywords	= {concurrency, race conditions, randomized algorithms,
		  testing},
  location	= {Pittsburgh, Pennsylvania, USA},
  series	= {ASPLOS XV},
  ids		= {pct}
}

@InProceedings{	  burckhardt2010:transformations,
  title		= {Verifying local transformations on relaxed memory models},
  author	= {Burckhardt, Sebastian and Musuvathi, Madanlal and Singh,
		  Vasu},
  booktitle	= {CONF_CC 2010},
  year		= {2010},
  location	= {Paphos, Cyprus},
  pages		= {104--123},
  numpages	= {20},
  url		= {http://dx.doi.org/10.1007/978-3-642-11970-5_7},
  doi		= {10.1007/978-3-642-11970-5_7},
  acmid		= {2175472},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg}
}

@InProceedings{	  burckhardt2012:tso,
  title		= {Concurrent Library Correctness on the {TSO} Memory Model},
  author	= {Sebastian Burckhardt and Alexey Gotsman and Madanlal
		  Musuvathi and Hongseok Yang},
  booktitle	= {CONF_ESOP 2012},
  year		= {2012},
  editor	= {Helmut Seidl},
  series	= {LNCS},
  volume	= {7211},
  pages		= {87--107},
  publisher	= {Springer},
  doi		= {10.1007/978-3-642-28869-2_5}
}

@InProceedings{	  cadar2008:klee,
  title		= {{KLEE}: {Unassisted} and Automatic Generation of
		  High-Coverage Tests for Complex Systems Programs},
  author	= {Cadar, Cristian and Dunbar, Daniel and Engler, Dawson},
  booktitle	= {CONF_OSDI 2008},
  year		= {2008},
  publisher	= {{USENIX} Association},
  address	= {USA},
  pages		= {209–224},
  numpages	= {16},
  location	= {San Diego, California},
  ids		= {klee}
}

@InProceedings{	  calcagno2009:invariant-synthesis,
  title		= {Bi-abductive Resource Invariant Synthesis},
  author	= {Cristiano Calcagno and Dino Distefano and Viktor
		  Vafeiadis},
  booktitle	= {CONF_APLAS 2009},
  year		= {2009},
  pages		= {259--274},
  editor	= {Zhenjiang Hu},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {5904}
}

@Article{	  calcagno2011:shape-analysis,
  title		= {Compositional Shape Analysis by Means of Bi-Abduction},
  author	= {Cristiano Calcagno and Dino Distefano and Peter W. O'Hearn
		  and Hongseok Yang},
  journal	= {JNL_JACM},
  year		= {2011},
  volume	= {58},
  number	= {6},
  pages		= {26}
}

@InProceedings{	  cerone2015:visibility,
  title		= {{A Framework for Transactional Consistency Models with
		  Atomic Visibility}},
  author	= {Andrea Cerone and Giovanni Bernardi and Alexey Gotsman},
  booktitle	= {CONF_CONCUR 2015},
  year		= {2015},
  pages		= {58--71},
  series	= {LIPIcs},
  volume	= {42},
  editor	= {Luca Aceto and David de Frutos Escrig},
  publisher	= {DAGSTUHL},
  doi		= {10.4230/LIPIcs.CONCUR.2015.58}
}

@Article{	  chakraborty2019:weakestmo,
  title		= {Grounding thin-air reads with event structures},
  author	= {Chakraborty, Soham and Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2019},
  month		= jan,
  issue_date	= {January 2019},
  volume	= {3},
  number	= {POPL},
  pages		= {70:1--70:28},
  articleno	= {70},
  numpages	= {28},
  doi		= {10.1145/3290383},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {weakestmo}
}

@Article{	  chalupa2017:datacentric,
  title		= {Data-centric dynamic partial order reduction},
  author	= {Chalupa, Marek and Chatterjee, Krishnendu and
		  Pavlogiannis, Andreas and Sinha, Nishant and Vaidya,
		  Kapil},
  journal	= {JNL_PACMPL},
  year		= {2017},
  month		= dec,
  issue_date	= {January 2018},
  volume	= {2},
  number	= {POPL},
  pages		= {31:1--31:30},
  articleno	= {31},
  numpages	= {30},
  url		= {http://doi.acm.org/10.1145/3158119},
  doi		= {10.1145/3158119},
  acmid		= {3158119},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {Concurrency, Partial-order Reduction, Stateless
		  model-checking},
  ids		= {dcdpor,dc-dpor,datacentric,datacentric-dpor}
}

@InProceedings{	  chase2005:deque,
  title		= {Dynamic circular work-stealing deque},
  author	= {David Chase and Yossi Lev},
  booktitle	= {CONF_SPAA 2005},
  year		= {2005},
  pages		= {21--28},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/1073970.1073974},
  doi		= {10.1145/1073970.1073974}
}

@Article{	  chatterjee2019:vcdpor,
  title		= {Value-Centric Dynamic Partial Order Reduction},
  author	= {Chatterjee, Krishnendu and Pavlogiannis, Andreas and
		  Toman, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2019},
  month		= oct,
  issue_date	= {October 2019},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {3},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3360550},
  doi		= {10.1145/3360550},
  articleno	= {124},
  numpages	= {29},
  ids		= {vcdpor,vc-dpor,valuecentric,valuecentric-dpor}
}

@InProceedings{	  chen2015:crash-hoare-logic,
  title		= {Using Crash Hoare logic for certifying the {FSCQ} file
		  system},
  author	= {Chen, Haogang and Ziegler, Daniel and Chajed, Tej and
		  Chlipala, Adam and Kaashoek, M. Frans and Zeldovich,
		  Nickolai},
  booktitle	= {CONF_SOSP 2015},
  location	= {Monterey, California},
  url		= {http://dl.acm.org/citation.cfm?doid=2815400.2815402},
  doi		= {10.1145/2815400.2815402},
  eventtitle	= {the 25th Symposium},
  pages		= {18--37},
  publisher	= {ACM},
  urldate	= {2020-06-17},
  date		= {2015},
  langid	= {english}
}

@Article{	  chen2016:path-resolution,
  title		= {A Formal Proof of a Unix Path Resolution Algorithm},
  author	= {Chen, Ran and Clochard, Martin and Marché, Claude},
  journal	= {JNL_HAL},
  year		= {2016},
  optpages	= {31},
  volume	= {hal-01406848},
  url		= {https://hal.inria.fr/hal-01406848/document},
  urldate	= {2020-11-16},
  langid	= {english}
}

@Article{	  chidambaram2012:consistency,
  title		= {Consistency Without Ordering},
  author	= {Chidambaram, Vijay and Sharma, Tushar and Arpaci-Dusseau,
		  Andrea C and Arpaci-Dusseau, Remzi H},
  year		= {2012},
  booktitile	= {CONF_FAST 2012},
  pages		= {16},
  langid	= {english},
  url		= {https://www.usenix.org/conference/fast12/consistency-without-ordering}
}

@InProceedings{	  clarke1983:model-checking,
  title		= {Automatic verification of finite-state concurrent systems
		  Using temporal logics specification: {A} practical
		  approach},
  author	= {Edmund M. Clarke and E. Allen Emerson and A. Prasad
		  Sistla},
  booktitle	= {CONF_POPL 1983},
  year		= {1983},
  pages		= {117--126},
  publisher	= {ACM},
  doi		= {10.1145/567067.567080},
  ids		= {model-checking}
}

@Article{	  clarke1996:symmetry,
  title		= {Exploiting symmetry in temporal logic model checking},
  author	= {Edmund M. Clarke and Somesh Jha and Reinhard Enders and
		  Thomas Filkorn},
  journal	= {JNL_FMSD},
  year		= {1996},
  volume	= {9},
  number	= {1/2},
  pages		= {77--104},
  doi		= {10.1007/BF00625969}
}

@InProceedings{	  clarke1998:alias-protection,
  title		= {Ownership types for flexible alias protection},
  author	= {David G. Clarke and John M. Potter and James Noble},
  booktitle	= {CONF_OOPSLA 1998},
  year		= {1998}
}

@InProceedings{	  clarke2000:cegar,
  title		= {Counterexample-Guided Abstraction Refinement },
  author	= {Clarke, Edmund and Grumberg, Orna and Jha, Somesh and Lu,
		  Yuan and Veith, Helmut},
  booktitle	= {CONF_CAV 2000},
  year		= {2000},
  editor	= {Emerson, E. Allen and Sistla, Aravinda Prasad},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {154--169},
  doi		= {10.1007/10722167_15},
  ids		= {cegar}
}

@InProceedings{	  clarke2004:cbmc,
  title		= {A tool for checking {ANSI-C} programs},
  author	= {Edmund M. Clarke and Daniel Kroening and Flavio Lerda},
  booktitle	= {CONF_TACAS 2004},
  year		= {2004},
  editor	= {Kurt Jensen and Andreas Podelski},
  pages		= {168--176},
  volume	= {2988},
  series	= {LNCS},
  doi		= {10.1007/978-3-540-24730-2_15},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  ids		= {cbmc, CBMC}
}

@Book{		  clrs2009:algorithms,
  title		= {Introduction to Algorithms, 3rd Edition},
  author	= {Thomas H. Cormen and Charles E. Leiserson and Ronald L.
		  Rivest and Clifford Stein},
  year		= {2009},
  publisher	= {{MIT} Press},
  url		= {http://mitpress.mit.edu/books/introduction-algorithms},
  ids		= {clrs}
}

@InProceedings{	  cohen2010:tso-sc,
  title		= {From total store order to sequential consistency: {A}
		  practical reduction theorem},
  author	= {Ernie Cohen and Bert Schirmer},
  booktitle	= {CONF_ITP 2010},
  year		= {2010},
  pages		= {403-418}
}

@Article{	  cohen2014:causal-memory,
  title		= {Coherent causal memory},
  author	= {Ernie Cohen},
  journal	= {JNL_CORR},
  year		= {2014},
  volume	= {abs/1404.2187}
}

@Article{	  colvin2005:simulation,
  title		= {Verifying concurrent data structures by simulation},
  author	= {Robert Colvin and Simon Doherty and Lindsay Groves},
  journal	= {JNL_ENTCS},
  year		= {2005},
  volume	= {137},
  number	= {2},
  pages		= {93-110}
}

@Article{	  conway1963:coroutine,
  title		= {Design of a separable transition-diagram compiler},
  author	= {Melvin E. Conway},
  journal	= {JNL_CACM},
  year		= {1963},
  volume	= {6},
  number	= {7},
  pages		= {396--408},
  doi		= {10.1145/366663.366704}
}

@InProceedings{	  coons2013:bounded-por,
  title		= {Bounded Partial-Order Reduction},
  author	= {Coons, Katherine E. and Musuvathi, Madan and McKinley,
		  Kathryn S.},
  booktitle	= {CONF_OOPSLA 2013},
  year		= {2013},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/2509136.2509556},
  doi		= {10.1145/2509136.2509556},
  pages		= {833–848},
  numpages	= {16},
  location	= {Indianapolis, Indiana, USA},
  ids		= {bdpor,bounded-dpor,bpor,bounded-por}
}

@Misc{		  corbet2008:ticket-locks,
  title		= {Ticket spinlocks},
  author	= {Jonathan Corbet},
  year		= 2008,
  url		= {http://lwn.net/Articles/267968/}
}

@Misc{		  corbet2013:lockrefs,
  title		= {Introducing lockrefs},
  author	= {Jonathan Corbet},
  year		= 2013,
  ids		= {lockrefs},
  url		= {http://lwn.net/Articles/565734/}
}

@Misc{		  corbet2014:mcs-qspinlocks,
  title		= {MCS locks and qspinlocks},
  author	= {Jonathan Corbet},
  year		= 2014,
  ids		= {mcs-qspinlocks},
  url		= {http://lwn.net/Articles/590243/}
}

@InProceedings{	  cui2013:rules-symbolic,
  title		= {Verifying systems rules using rule-directed symbolic
		  execution},
  author	= {Cui, Heming and Hu, Gang and Wu, Jingyue and Yang,
		  Junfeng},
  booktitle	= {CONF_ASPLOS 2013},
  year		= {2013},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  doi		= {10.1145/2451116.2451152},
  pages		= {329–342},
  numpages	= {14},
  location	= {Houston, Texas, USA}
}

@InProceedings{	  dalvandi2020:owicki-rar,
  title		= {{Owicki-Gries Reasoning for C11 RAR}},
  author	= {Sadegh Dalvandi and Simon Doherty and Brijesh Dongol and
		  Heike Wehrheim},
  booktitle	= {CONF_ECOOP 2020},
  year		= 2020,
  editor	= {Robert Hirschfeld and Tobias Pape},
  volume	= 166,
  series	= {LIPIcs},
  pages		= {11:1--11:26},
  address	= {Dagstuhl, Germany},
  publisher	= {DAGSTUHL},
  doi		= {10.4230/LIPIcs.ECOOP.2020.11}
}

@InCollection{	  dan2015:abstractions,
  title		= {Effective abstractions for verification under relaxed
		  memory models},
  author	= {Dan, Andrei and Meshman, Yuri and Vechev, Martin and
		  Yahav, Eran},
  booktitle	= {CONF_VMCAI 2015},
  year		= {2015},
  volume	= {8931},
  series	= {LNCS},
  editor	= {D’Souza, Deepak and Lal, Akash and Larsen,
		  KimGuldstrand},
  doi		= {10.1007/978-3-662-46081-8_25},
  publisher	= {Springer},
  pages		= {449-466},
  language	= {English}
}

@InProceedings{	  deligiannis2015:state-machines,
  title		= {Asynchronous programming, analysis and testing with state
		  machines},
  author	= {Pantazis Deligiannis and Alastair F. Donaldson and Jeroen
		  Ketema and Akash Lal and Paul Thomson},
  booktitle	= {CONF_PLDI 2015},
  year		= {2015},
  opteditor	= {David Grove and Stephen M. Blackburn},
  pages		= {154--164},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2737924.2737996},
  doi		= {10.1145/2737924.2737996}
}

@InProceedings{	  deligiannis2021:coyote,
  title		= {Building Reliable Cloud Services Using Coyote Actors},
  author	= {Pantazis Deligiannis and Narayanan Ganapathy and Akash Lal
		  and Shaz Qadeer},
  booktitle	= {CONF_SOCC 2021},
  year		= {2021},
  opteditor	= {Carlo Curino and Georgia Koutrika and Ravi Netravali},
  pages		= {108--121},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/3472883.3486983},
  doi		= {10.1145/3472883.3486983}
}

@InProceedings{	  deligiannis2023:ct,
  title		= {Industrial-Strength Controlled Concurrency Testing for sc
		  {C} tt {\#} Programs with sc Coyote},
  author	= {Pantazis Deligiannis and Aditya Senthilnathan and Fahad
		  Nayyar and Chris Lovett and Akash Lal},
  booktitle	= {CONF_TACAS 2023},
  year		= {2023},
  opteditor	= {Sriram Sankaranarayanan and Natasha Sharygina},
  series	= {LNCS},
  volume	= {13994},
  pages		= {433--452},
  publisher	= {Springer},
  url		= {https://doi.org/10.1007/978-3-031-30820-8\_26},
  doi		= {10.1007/978-3-031-30820-8\_26}
}

@InProceedings{	  demange2013:java-bmm,
  title		= {Plan {B}: {A} buffered memory model for {Java}},
  author	= {Demange, Delphine and Laporte, Vincent and Zhao, Lei and
		  Jagannathan, Suresh and Pichardie, David and Vitek, Jan},
  booktitle	= {CONF_POPL 2013},
  year		= {2013},
  pages		= {329--342},
  doi		= {10.1145/2429069.2429110},
  acmid		= {2429110},
  publisher	= {ACM}
}

@InProceedings{	  demsky2015:satcheck,
  title		= {{SATCheck}: {SAT}-directed stateless model checking for
		  {SC} and {TSO}},
  author	= {Demsky, Brian and Lam, Patrick},
  booktitle	= {CONF_OOPSLA 2015},
  year		= {2015},
  location	= {Pittsburgh, PA, USA},
  pages		= {20--36},
  url		= {http://doi.acm.org/10.1145/2814270.2814297},
  doi		= {10.1145/2814270.2814297},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {Relaxed memory model, model checking},
  ids		= {satcheck}
}

@InProceedings{	  derrick2011:linearisability,
  title		= {Verifying linearisability with potential linearisation
		  points},
  author	= {Derrick, John and Schellhorn, Gerhard and Wehrheim,
		  Heike},
  booktitle	= {CONF_FM 2011},
  year		= {2011},
  pages		= {323-337},
  editor	= {Michael Butler and Wolfram Schulte},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {6664}
}

@InProceedings{	  desai2013:p,
  title		= {{P:} safe asynchronous event-driven programming},
  author	= {Ankush Desai and Vivek Gupta and Ethan K. Jackson and Shaz
		  Qadeer and Sriram K. Rajamani and Damien Zufferey},
  booktitle	= {CONF_PLDI 2013},
  year		= {2013},
  opteditor	= {Hans{-}Juergen Boehm and Cormac Flanagan},
  pages		= {321--332},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2491956.2462184},
  doi		= {10.1145/2491956.2462184}
}

@Article{	  desnoyers2012:urcu,
  title		= {User-Level Implementations of Read-Copy Update},
  author	= {Mathieu Desnoyers and Paul E. McKenney and Alan S. Stern
		  and Michel R. Dagenais and Jonathan Walpole},
  journal	= {JNL_TPDS},
  year		= {2012},
  volume	= {23},
  number	= {2},
  pages		= {375--382},
  doi		= {10.1109/TPDS.2011.159},
  ids		= {urcu}
}

@InProceedings{	  dice2019:twalock,
  title		= {TWA – Ticket Locks Augmented with a Waiting Array},
  author	= {Dice, Dave and Kogan, Alex},
  booktitle	= {CONF_EUROPAR 2019},
  year		= {2019},
  publisher	= {Springer-Verlag},
  address	= {Berlin, Heidelberg},
  url		= {https://doi.org/10.1007/978-3-030-29400-7_24},
  doi		= {10.1007/978-3-030-29400-7_24},
  pages		= {334–345},
  numpages	= {12},
  ids		= {twalock}
}

@Article{	  digiusto2023:message-passing,
  title		= {A Partial Order View of Message-Passing Communication
		  Models},
  author	= {Di Giusto, Cinzia and Ferré, Davide and Laversa, Laetitia
		  and Lozes, Etienne},
  journal	= {JNL_PACMPL},
  year		= {2023},
  month		= {jan},
  issue_date	= {January 2023},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {7},
  number	= {POPL},
  url		= {https://doi.org/10.1145/3571248},
  doi		= {10.1145/3571248},
  articleno	= {55},
  numpages	= {27}
}

@TechReport{	  dijkstra1965:cooperating,
  title		= {{EWD123: {Cooperating} sequential processes}},
  author	= {Dijkstra, Edsger W.},
  year		= {1965},
  url		= {http://www.cs.utexas.edu/~EWD/transcriptions/EWD01xx/EWD123.html}
}

@InProceedings{	  dinsdale-young2010:cap,
  title		= {Concurrent abstract predicates},
  author	= {Dinsdale-Young, T. and Dodds, M. and Gardner, P. and
		  Parkinson, M. and Vafeiadis, V.},
  booktitle	= {CONF_ECOOP 2010},
  year		= {2010},
  series	= {LNCS},
  volume	= {6183},
  pages		= {504--528},
  publisher	= {Springer}
}

@InProceedings{	  dinsdale-young2013:views,
  title		= {Views: {Compositional} reasoning for concurrent programs},
  author	= {Thomas Dinsdale-Young and Lars Birkedal and Philippa
		  Gardner and Matthew J. Parkinson and Hongseok Yang},
  booktitle	= {CONF_POPL 2013},
  year		= {2013}
}

@InProceedings{	  distefano2006:shape-analysis,
  title		= {A Local Shape Analysis Based on Separation Logic},
  author	= {Dino Distefano and Peter W. {O'Hearn} and Hongseok Yang},
  booktitle	= {CONF_TACAS 2006},
  year		= {2006},
  pages		= {287--302},
  editor	= {Holger Hermanns and Jens Palsberg},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {3920}
}

@InProceedings{	  distefano2008:jstar,
  title		= {{jStar}: {Towards} practical verification for {J}ava},
  author	= {Dino Distefano and Matthew J. Parkinson},
  booktitle	= {CONF_OOPSLA 2008},
  year		= {2008},
  pages		= {213-226},
  editor	= {Gail E. Harris},
  publisher	= {ACM},
  ids		= {jstar}
}

@Article{	  dodds2013:causal-cycles,
  title		= {{C/C++} Causal Cycles Confound Compositionality},
  author	= {Mike Dodds and Mark Batty and Alexey Gotsman},
  journal	= {JNL_TINYTOCS},
  year		= {2013},
  volume	= {2}
}

@InProceedings{	  dodds2015:timestamped-stack,
  title		= {A Scalable, Correct Time-Stamped Stack},
  author	= {Dodds, Mike and Haas, Andreas and Kirsch, Christoph M.},
  booktitle	= {CONF_POPL 2015},
  year		= {2015},
  isbn		= {9781450333009},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/2676726.2676963},
  doi		= {10.1145/2676726.2676963},
  pages		= {233–246},
  numpages	= {14},
  keywords	= {concurrent stack, linearizability, timestamps,
		  verification},
  location	= {Mumbai, India},
  optseries	= {POPL '15}
}

@InProceedings{	  doherty2004:queue,
  title		= {Formal Verification of a Practical Lock-Free Queue
		  Algorithm},
  author	= {Simon Doherty and Lindsay Groves and Victor Luchangco and
		  Mark Moir},
  booktitle	= {CONF_FORTE 2004},
  year		= {2004},
  editor	= {David de Frutos{-}Escrig and Manuel Núñez},
  series	= {LNCS},
  volume	= {3235},
  pages		= {97--114},
  publisher	= {Springer},
  doi		= {10.1007/978-3-540-30232-2_7},
  ids		= {dglm,dglm-queue}
}

@InProceedings{	  doherty2009:nonblocking,
  title		= {Nonblocking Algorithms and Backward Simulation},
  author	= {Simon Doherty and Mark Moir},
  booktitle	= {CONF_DISC 2009},
  year		= {2009},
  pages		= {274-288},
  editor	= {Idit Keidar},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {5805}
}

@InProceedings{	  doko2016:fsl,
  title		= {A Program Logic for {C11} Memory Fences},
  author	= {Marko Doko and Viktor Vafeiadis},
  booktitle	= {CONF_VMCAI 2016},
  year		= {2016},
  editor	= {Barbara Jobstmann and K. Rustan M. Leino},
  series	= {LNCS},
  volume	= {9583},
  pages		= {413--430},
  publisher	= {Springer},
  url		= {http://dx.doi.org/10.1007/978-3-662-49122-5_20},
  doi		= {10.1007/978-3-662-49122-5_20}
}

@InProceedings{	  doko2017:fsl++,
  title		= {Tackling Real-Life Relaxed Concurrency with {FSL++}},
  author	= {Marko Doko and Viktor Vafeiadis},
  booktitle	= {CONF_ESOP 2017},
  year		= {2017},
  editor	= {Hongseok Yang},
  series	= {LNCS},
  volume	= {10201},
  pages		= {448--475},
  publisher	= {Springer},
  doi		= {10.1007/978-3-662-54434-1_17}
}

@InProceedings{	  dolan2018:ocaml,
  title		= {Bounding Data Races in Space and Time},
  author	= {Dolan, Stephen and Sivaramakrishnan, KC and Madhavapeddy,
		  Anil},
  booktitle	= {CONF_PLDI 2018},
  year		= {2018},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/3192366.3192421},
  doi		= {10.1145/3192366.3192421},
  pages		= {242–255},
  numpages	= {14},
  keywords	= {operational semantics, weak memory models},
  location	= {Philadelphia, PA, USA},
  ids		= {ocaml-mm}
}

@InProceedings{	  dongol2018:lin-for-partial-execs,
  title		= {Making Linearizability Compositional for Partially
		  Ordered Executions},
  author	= {Doherty, Simon and Dongol, Brijesh and Wehrheim, Heike and
		  Derrick, John},
  booktitle	= {CONF_IFM},
  year		= 2018,
  editor	= {Furia, Carlo A. and Winter, Kirsten},
  pages		= {110--129},
  address	= {Cham},
  publisher	= {Springer}
}

@InProceedings{	  dongol2018:lin-for-wmm,
  title		= {On abstraction and compositionality for weak-memory
		  linearisability},
  author	= {Brijesh Dongol and Radha Jagadeesan and James Riely and
		  Alasdair Armstrong},
  booktitle	= {CONF_VMCAI 2018},
  year		= 2018,
  editor	= {Isil Dillig and Jens Palsberg},
  doi		= {10.1007/978-3-319-73721-8_9},
  volume	= 10747,
  series	= {LNCS},
  pages		= {183--204},
  publisher	= {Springer}
}

@InProceedings{	  dragoi2013:cooperating,
  title		= {Automatic Linearizability Proofs of Concurrent Objects
		  with Cooperating Updates},
  author	= {Cezara Dragoi and Ashutosh Gupta and Thomas A. Henzinger},
  booktitle	= {CONF_CAV 2013},
  year		= {2013},
  pages		= {174-190},
  editor	= {Natasha Sharygina and Helmut Veith},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {8044}
}

@InProceedings{	  dudka2011:predator,
  title		= {Predator: {A} Practical Tool for Checking Manipulation of
		  Dynamic Data Structures Using Separation Logic},
  author	= {Kamil Dudka and Petr Peringer and Tomás Vojnar},
  booktitle	= {CONF_CAV 2011},
  year		= {2011},
  pages		= {372-378},
  editor	= {Ganesh Gopalakrishnan and Shaz Qadeer},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {6806},
  ids		= {predator}
}

@Article{	  duggan2002:linking,
  title		= {Type-Safe Linking with Recursive {DLL's} and Shared
		  Libraries},
  author	= {Dominic Duggan},
  journal	= {JNL_TOPLAS},
  year		= 2002,
  volume	= {24},
  number	= {6},
  pages		= {711--804}
}

@InProceedings{	  elmas2009:qed,
  title		= {A calculus of atomic actions},
  author	= {Tayfun Elmas and Shaz Qadeer and Serdar Tasiran},
  booktitle	= {CONF_POPL 2009},
  year		= {2009},
  editor	= {Zhong Shao and Benjamin C. Pierce},
  pages		= {2--15},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/1480881.1480885},
  doi		= {10.1145/1480881.1480885}
}

@Article{	  elrad1982:communication-closure,
  title		= {Decomposition of distributed programs into
		  communication-closed layers},
  author	= {Tzilla Elrad and Nissim Francez},
  journal	= {JNL_SCP},
  year		= {1982},
  volume	= {2},
  number	= {3},
  pages		= {155-173},
  doi		= {10.1016/0167-6423(83)90013-8},
  url		= {https://doi.org/10.1016/0167-6423(83)90013-8}
}

@InProceedings{	  emerson2005:symmetry,
  title		= {Dynamic Symmetry Reduction},
  author	= {E. Allen Emerson and Thomas Wahl},
  booktitle	= {CONF_TACAS 2005},
  year		= {2005},
  editor	= {Nicolas Halbwachs and Lenore D. Zuck},
  series	= {LNCS},
  volume	= {3440},
  pages		= {382--396},
  publisher	= {Springer},
  url		= {https://doi.org/10.1007/978-3-540-31980-1_25},
  doi		= {10.1007/978-3-540-31980-1_25}
}

@InProceedings{	  emmi2011:delay-bounded,
  title		= {Delay-Bounded Scheduling},
  author	= {Emmi, Michael and Qadeer, Shaz and Rakamarić, Zvonimir},
  booktitle	= {CONF_POPL 2011},
  year		= {2011},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  doi		= {10.1145/1926385.1926432},
  pages		= {411–422},
  numpages	= {12},
  location	= {Austin, Texas, USA}
}

@InProceedings{	  emmi2019:violat,
  title		= {Violat: Generating Tests of Observational Refinement for
		  Concurrent Objects},
  author	= {Michael Emmi and Constantin Enea},
  booktitle	= {CONF_CAV 2019},
  year		= {2019},
  editor	= {Isil Dillig and Serdar Tasiran},
  series	= {LNCS},
  volume	= {11562},
  pages		= {534--546},
  publisher	= {Springer},
  doi		= {10.1007/978-3-030-25543-5_30}
}

@Article{	  emmi2019:visibility-relaxation,
  title		= {Weak-consistency specification via visibility relaxation},
  author	= {Michael Emmi and Constantin Enea},
  journal	= {JNL_PACMPL},
  year		= {2019},
  volume	= {3},
  number	= {POPL},
  pages		= {60:1--60:28},
  doi		= {10.1145/3290373}
}

@Article{	  enea2024:must,
  title		= {Model Checking Distributed Protocols in Must},
  author	= {Enea, Constantin and Giannakopoulou, Dimitra and
		  Kokologiannakis, Michalis and Majumdar, Rupak},
  journal	= {JNL_PACMPL},
  year		= {2024},
  month		= oct,
  issue_date	= {October 2024},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {8},
  number	= {OOPSLA2},
  url		= {https://doi.org/10.1145/3689778},
  doi		= {10.1145/3689778},
  articleno	= {338},
  numpages	= {28},
  keywords	= {Distributed Systems, Model Checking},
  ids		= {must}
}

@Article{	  esik1995:ka-converse,
  title		= {Equational Properties of Kleene Algebras of Relations with
		  Conversion},
  author	= {Zoltán Ésik and L. Bernátsky},
  journal	= {JNL_TCS},
  year		= {1995},
  volume	= {137},
  number	= {2},
  pages		= {237--251},
  url		= {https://doi.org/10.1016/0304-3975(94)00041-G},
  doi		= {10.1016/0304-3975(94)00041-G}
}

@Article{	  fairbanks2012:ext4-analysis,
  title		= {An analysis of Ext4 for digital forensics},
  author	= {Fairbanks, Kevin D.},
  booktitle	= {CONF_DFRWS 2012},
  volume	= {9},
  url		= {http://www.sciencedirect.com/science/article/pii/S1742287612000357},
  doi		= {10.1016/j.diin.2012.05.010},
  pages		= {S118--S130},
  urldate	= {2020-05-13},
  date		= {2012-08-01},
  langid	= {english},
  keywords	= {Digital forensics, Ext4, Extents, File system forensics,
		  Flex block groups}
}

@Article{	  feldman2020:traversals,
  title		= {Proving highly-concurrent traversals correct},
  author	= {Yotam M. Y. Feldman and Artem Khyzha and Constantin Enea
		  and Adam Morrison and Aleksandar Nanevski and Noam Rinetzky
		  and Sharon Shoham},
  journal	= {JNL_PACMPL},
  year		= {2020},
  volume	= {4},
  number	= {OOPSLA},
  pages		= {128:1--128:29},
  doi		= {10.1145/3428196}
}

@InProceedings{	  feng2009:local-rely-guarantee,
  title		= {Local rely-guarantee reasoning},
  author	= {Xinyu Feng},
  booktitle	= {CONF_POPL 2009},
  year		= {2009},
  pages		= {315-327},
  editor	= {Zhong Shao and Benjamin C. Pierce},
  publisher	= {ACM}
}

@InProceedings{	  ferreira2010:concurrent-sep,
  title		= {Parameterized memory models and concurrent separation
		  logic},
  author	= {Ferreira, Rodrigo and Feng, Xinyu and Shao, Zhong},
  booktitle	= {CONF_ESOP 2010},
  year		= {2010},
  series	= {LNCS},
  volume	= {6012},
  pages		= {267--286},
  editor	= {Andrew D. Gordon},
  publisher	= {Springer}
}

@Article{	  filipovic2009:lin-as-obsv-refinement,
  title		= {Abstraction for concurrent objects},
  author	= {Ivana Filipović and Peter O’Hearn and Noam Rinetzky and
		  Hongseok Yang},
  journal	= {JNL_TCS},
  year		= {2010},
  volume	= {411},
  number	= {51},
  pages		= {4379-4398},
  doi		= {10.1016/j.tcs.2010.09.021}
}

@InProceedings{	  flanagan1993:continuations,
  title		= {The essence of compiling with continuations},
  author	= {Flanagan, Cormac and Sabry, Amr and Duba, Bruce F. and
		  Felleisen, Matthias},
  booktitle	= {CONF_PLDI 1993},
  year		= {1993},
  pages		= {237--247},
  publisher	= {ACM}
}

@InProceedings{	  flanagan2005:dpor,
  title		= {Dynamic partial-order reduction for model checking
		  software},
  author	= {Cormac Flanagan and Patrice Godefroid},
  booktitle	= {CONF_POPL 2005},
  year		= {2005},
  pages		= {110--121},
  publisher	= {{ACM}},
  address	= {New York, NY, USA},
  url		= {http://doi.acm.org/10.1145/1040305.1040315},
  doi		= {10.1145/1040305.1040315},
  ids		= {dpor}
}

@Article{	  flanagan2005:purity,
  title		= {Exploiting Purity for Atomicity},
  author	= {Cormac Flanagan and Stephen N. Freund and Shaz Qadeer},
  journal	= {JNL_TSE},
  year		= {2005},
  volume	= {31},
  number	= {4},
  pages		= {275--291},
  url		= {https://doi.org/10.1109/TSE.2005.47},
  doi		= {10.1109/TSE.2005.47}
}

@InProceedings{	  flatt1998:units,
  title		= {Units: {Cool} modules for {HOT} languages},
  author	= {Matthew Flatt and Matthias Felleisen},
  booktitle	= {CONF_PLDI 1998},
  year		= 1998
}

@InProceedings{	  flur2016:arm8,
  title		= {Modelling the {ARMv8} architecture, operationally:
		  Concurrency and {ISA}},
  author	= {Flur, Shaked and Gray, Kathryn E. and Pulte, Christopher
		  and Sarkar, Susmit and Sezgin, Ali and Maranget, Luc and
		  Deacon, Will and Sewell, Peter},
  booktitle	= {CONF_POPL 2016},
  year		= {2016},
  location	= {St. Petersburg, FL, USA},
  pages		= {608--621},
  numpages	= {14},
  url		= {http://doi.acm.org/10.1145/2837614.2837615},
  doi		= {10.1145/2837614.2837615},
  acmid		= {2837615},
  publisher	= {ACM},
  optaddress	= {New York, NY, USA},
  keywords	= {ISA, Relaxed Memory Models, semantics},
  ids		= {arm8, arm8-model}
}

@InProceedings{	  flur2017:mixed-size,
  title		= {Mixed-size concurrency: {ARM}, {POWER}, {C/C++11}, and
		  {SC}},
  author	= {Flur, Shaked and Sarkar, Susmit and Pulte, Christopher and
		  Nienhuis, Kyndylan and Maranget, Luc and Gray, Kathryn E.
		  and Sezgin, Ali and Batty, Mark and Sewell, Peter},
  booktitle	= {CONF_POPL 2017},
  year		= {2017},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/3009837.3009839},
  doi		= {10.1145/3009837.3009839},
  pages		= {429–442},
  numpages	= {14},
  keywords	= {semantics, Relaxed Memory Models, mixed-size, ISA},
  location	= {Paris, France}
}

@TechReport{	  fober2001:optimized-lfqueue,
  title		= {{Optimised Lock-Free FIFO Queue}},
  author	= {Fober, Dominique and Orlarey, Yann and Letz, Stéphane},
  year		= {2001},
  url		= {https://hal.archives-ouvertes.fr/hal-02158792},
  type		= {Technical Report},
  institution	= {{GRAME}},
  keywords	= {concurrency ; fifo ; lifo ; lock-free},
  pdf		= {https://hal.archives-ouvertes.fr/hal-02158792/file/LockFree.pdf},
  hal_id	= {hal-02158792},
  hal_version	= {v1},
  ids		= {optimized-lfqueue,optimized-lf-queue}
}

@InProceedings{	  friedman2021:mirror,
  title		= {Mirror: Making Lock-Free Data Structures Persistent},
  author	= {Friedman, Michal and Petrank, Erez and Ramalhete, Pedro},
  booktitle	= {CONF_PLDI 2021},
  year		= {2021},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/3453483.3454105},
  pages		= {1218–1232},
  numpages	= {15},
  ids		= {mirror}
}

@Book{		  galloway2009:model-checking-vfs,
  title		= {Model-checking the Linux virtual file system},
  author	= {Galloway, Andy and Lüttgen, Gerald and Mühlberg, Jan
		  Tobias and Siminiceanu, Radu I.},
  booktitle	= {CONF_VMCAI 2009},
  year		= {2009},
  pages		= {74--88}
}

@InProceedings{	  gavrilenko2019:dartagnan,
  title		= {{BMC} for weak memory models: {Relation} analysis for
		  compact {SMT} encodings},
  author	= {Gavrilenko, Natalia and Ponce-de-León, Hernán and
		  Furbach, Florian and Heljanko, Keijo and Meyer, Roland},
  booktitle	= {CONF_CAV 2019},
  year		= {2019},
  editor	= {Dillig, Isil and Tasiran, Serdar},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {355--365},
  doi		= {10.1007/978-3-030-25540-4_19},
  ids		= {dartagnan}
}

@Article{	  gibbons1997:testing,
  title		= {Testing shared memories},
  author	= {Gibbons, Phillip B. and Korach, Ephraim},
  journal	= {JNL_JCOMP},
  year		= {1997},
  month		= aug,
  issue_date	= {Aug. 1997},
  volume	= {26},
  number	= {4},
  pages		= {1208--1244},
  numpages	= {37},
  doi		= {10.1137/S0097539794279614},
  acmid		= {262561},
  publisher	= {Society for Industrial and Applied Mathematics},
  address	= {Philadelphia, PA, USA},
  ids		= {gibbons-korach}
}

@PhDThesis{	  girard:thesis,
  title		= {Interprétation fonctionelle et élimination des coupures
		  de l'arithmétique d'ordre supérieur},
  author	= {Jean-Yves Girard},
  year		= 1972,
  school	= {Université Paris VII}
}

@InProceedings{	  godefroid1997:verisoft,
  title		= {Model checking for programming languages using
		  {VeriSoft}},
  author	= {Patrice Godefroid},
  booktitle	= {CONF_POPL 1997},
  year		= {1997},
  pages		= {174--186},
  location	= {Paris, France},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {http://doi.acm.org/10.1145/263699.263717},
  doi		= {10.1145/263699.263717},
  ids		= {verisoft}
}

@InProceedings{	  godefroid1998:heartbeat,
  title		= {Model Checking Without a Model: {An} Analysis of the
		  Heart-beat Monitor of a Telephone Switch Using {VeriSoft}},
  author	= {Godefroid, Patrice and Hanmer, Robert S. and Jagadeesan,
		  Lalita Jategaonkar},
  booktitle	= {CONF_ISSTA 1998},
  year		= {1998},
  location	= {Clearwater Beach, Florida, USA},
  pages		= {124--133},
  numpages	= {10},
  url		= {http://doi.acm.org/10.1145/271771.271800},
  doi		= {10.1145/271771.271800},
  acmid		= {271800},
  publisher	= {ACM},
  address	= {New York, NY, USA}
}

@Article{	  godefroid2005:verisoft,
  title		= {Software Model Checking: {The} {VeriSoft} Approach},
  author	= {Patrice Godefroid},
  journal	= {JNL_FMSD},
  year		= {2005},
  month		= mar,
  volume	= {26},
  number	= {2},
  pages		= {77--101},
  url		= {http://dx.doi.org/10.1007/s10703-005-1489-x},
  doi		= {10.1007/s10703-005-1489-x},
  ids		= {verisoft-journal}
}

@Misc{		  golovin2024:relinche-artifact,
  title		= {{RELINCHE}: Automatically Checking Linearizability under
		  Relaxed Memory Consistency (Replication Package)},
  author	= {Golovin, Pavel and Kokologiannakis, Michalis and
		  Vafeiadis, Viktor},
  year		= {2024},
  month		= oct,
  url		= {https://zenodo.org/doi/10.5281/zenodo.13992580},
  doi		= {10.5281/zenodo.13992580},
  ids		= {relinche-artifact}
}

@Article{	  goloving2025:relinche,
  title		= {{RELINCHE}: {Automatically} Checking Linearizability under
		  Relaxed Memory Consistency},
  author	= {Golovin, Pavel and Kokologiannakis, Michalis and
		  Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2025},
  month		= jan,
  issue_date	= {January 2025},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {9},
  number	= {POPL},
  url		= {https://doi.org/10.1145/3704906},
  doi		= {10.1145/3704906},
  articleno	= {70},
  numpages	= {28},
  ids		= {relinche}
}

@Article{	  gonzalez2009:error-prop-fs,
  title		= {Error propagation analysis for file systems},
  author	= {Rubio-González, Cindy and Gunawi, Haryadi S. and Liblit,
		  Ben and Arpaci-Dusseau, Remzi H. and Arpaci-Dusseau, Andrea
		  C.},
  volume	= {44},
  doi		= {10.1145/1543135.1542506},
  pages		= {270--280},
  number	= {6},
  journaltitle	= {JNL_SN},
  shortjournal	= {JNL_SN},
  urldate	= {2020-06-17},
  date		= {2009-06-15},
  keywords	= {binary decision diagrams, copy constant propagation,
		  interprocedural dataflow analysis, static program analysis,
		  weighted pushdown systems}
}

@InProceedings{	  gotovos2011:concuerror,
  title		= {Test-driven development of concurrent programs using
		  concuerror},
  author	= {Alkis Gotovos and Maria Christakis and Konstantinos
		  Sagonas},
  booktitle	= {CONF_ERLANG 2011},
  year		= {2011},
  editor	= {Kenji Rikitake and Erik Stenman},
  pages		= {51--61},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2034654.2034664},
  doi		= {10.1145/2034654.2034664},
  ids		= {concuerror}
}

@InProceedings{	  gotsman2007:local-reasoning,
  title		= {Local Reasoning for Storable Locks and Threads},
  author	= {Alexey Gotsman and Josh Berdine and Byron Cook and Noam
		  Rinetzky and Mooly Sagiv},
  booktitle	= {CONF_APLAS 2007},
  year		= {2007},
  pages		= {19-37},
  editor	= {Zhong Shao},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {4807}
}

@InProceedings{	  gotsman2007:storable,
  title		= {Local Reasoning for Storable Locks and Threads},
  author	= {Alexey Gotsman and Josh Berdine and Byron Cook and Noam
		  Rinetzky and Mooly Sagiv},
  booktitle	= {CONF_APLAS 2007},
  year		= {2007},
  pages		= {19-37}
}

@InProceedings{	  gotsman2007:thread-modular,
  title		= {Thread-modular shape analysis},
  author	= {Alexey Gotsman and Josh Berdine and Byron Cook and Mooly
		  Sagiv},
  booktitle	= {CONF_PLDI 2007},
  year		= {2007},
  pages		= {266-277},
  editor	= {Jeanne Ferrante and Kathryn S. McKinley},
  publisher	= {ACM}
}

@InProceedings{	  gotsman2009:nonblocking,
  title		= {Proving that non-blocking algorithms don't block},
  author	= {Alexey Gotsman and Byron Cook and Matthew J. Parkinson and
		  Viktor Vafeiadis},
  booktitle	= {CONF_POPL 2009},
  year		= {2009},
  pages		= {16-28},
  editor	= {Zhong Shao and Benjamin C. Pierce},
  publisher	= {ACM}
}

@InProceedings{	  grathwohl2014:kat-b,
  title		= {{KAT} + B!},
  author	= {Niels Bjørn Bugge Grathwohl and Dexter Kozen and
		  Konstantinos Mamouras},
  booktitle	= {CONF_LICS 2014},
  year		= {2014},
  editor	= {Thomas A. Henzinger and Dale Miller},
  pages		= {44:1--44:10},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2603088.2603095},
  doi		= {10.1145/2603088.2603095}
}

@InProceedings{	  grebenshchikov2012:verifiers,
  title		= {Synthesizing software verifiers from proof rules},
  author	= {Sergey Grebenshchikov and Nuno P. Lopes and Corneliu
		  Popeea and Andrey Rybalchenko},
  booktitle	= {CONF_PLDI 2012},
  year		= {2012},
  pages		= {405--416},
  publisher	= {ACM}
}

@Article{	  haas2022:caat,
  title		= {CAAT: Consistency as a Theory},
  author	= {Haas, Thomas and Meyer, Roland and Ponce de León,
		  Hernán},
  journal	= {JNL_PACMPL},
  year		= {2022},
  month		= {oct},
  issue_date	= {October 2022},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {6},
  number	= {OOPSLA2},
  url		= {https://doi.org/10.1145/3563292},
  doi		= {10.1145/3563292},
  articleno	= {129},
  numpages	= {31},
  ids		= {caat}
}

@InProceedings{	  harper1995:intensional,
  title		= {Compiling Polymorphism Using Intensional Type Analysis},
  author	= {Robert Harper and Greg Morrisett},
  booktitle	= {CONF_POPL 1995},
  year		= 1995
}

@InProceedings{	  harris2001:nonblocking-list,
  title		= {A Pragmatic Implementation of Non-blocking Linked-Lists},
  author	= {Harris, Timothy L.},
  booktitle	= {CONF_DISC 2001},
  year		= {2001},
  isbn		= {3540426051},
  doi		= {10.5555/645958.676105},
  publisher	= {Springer-Verlag},
  address	= {Berlin, Heidelberg},
  pages		= {300–314},
  numpages	= {15},
  optseries	= {DISC '01}
}

@InProceedings{	  harris2002:rdcss,
  title		= {A Practical Multi-word Compare-and-Swap Operation},
  author	= {Timothy L. Harris and Keir Fraser and Ian A. Pratt},
  booktitle	= {CONF_DISC 2002},
  year		= {2002},
  editor	= {Dahlia Malkhi},
  series	= {LNCS},
  volume	= {2508},
  pages		= {265--279},
  publisher	= {Springer},
  doi		= {10.1007/3-540-36108-1_18},
  ids		= {rdcss}
}

@Article{	  havelund2000:jpf,
  title		= {Model Checking {JAVA} Programs using {JAVA} PathFinder},
  author	= {Klaus Havelund and Thomas Pressburger},
  journal	= {JNL_STTT},
  year		= {2000},
  volume	= {2},
  number	= {4},
  pages		= {366--381},
  url		= {https://doi.org/10.1007/s100090050043},
  doi		= {10.1007/S100090050043},
  ids		= {jpf,pathfinder}
}

@InProceedings{	  he2021:satisfiability,
  title		= {Satisfiability modulo Ordering Consistency Theory for
		  Multi-Threaded Program Verification},
  author	= {He, Fei and Sun, Zhihang and Fan, Hongyu},
  booktitle	= {CONF_PLDI 2021},
  year		= {2021},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/3453483.3454108},
  doi		= {10.1145/3453483.3454108},
  pages		= {1264–1279},
  numpages	= {16},
  keywords	= {concurrency, Program verification, memory consistency
		  model, satisfiability modulo theory},
  location	= {Virtual, Canada},
  ids		= {zord}
}

@InProceedings{	  heller2005:lazy-list,
  title		= {A Lazy Concurrent List-Based Set Algorithm},
  author	= {Heller, Steve and Herlihy, Maurice and Luchangco, Victor
		  and Moir, Mark and Scherer, William N. and Shavit, Nir},
  booktitle	= {CONF_OPODIS 2005},
  year		= {2005},
  publisher	= {Springer-Verlag},
  address	= {Berlin, Heidelberg},
  url		= {https://doi.org/10.1007/11795490_3},
  doi		= {10.1007/11795490_3},
  pages		= {3–16},
  numpages	= {14},
  location	= {Pisa, Italy}
}

@InProceedings{	  hendler2004:elimination,
  title		= {A Scalable Lock-Free Stack Algorithm},
  author	= {Hendler, Danny and Shavit, Nir and Yerushalmi, Lena},
  booktitle	= {CONF_SPAA 2004},
  year		= {2004},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/1007912.1007944},
  doi		= {10.1145/1007912.1007944},
  pages		= {206–215},
  numpages	= {10},
  location	= {Barcelona, Spain}
}

@InProceedings{	  hendler2010:flat-combining,
  title		= {Flat combining and the synchronization-parallelism
		  tradeoff},
  author	= {Hendler, Danny and Incze, Itai and Shavit, Nir and
		  Tzafrir, Moran},
  booktitle	= {CONF_SPAA 2010},
  year		= {2010},
  pages		= {355-364},
  publisher	= {ACM}
}

@InProceedings{	  henzinger2002:blast,
  title		= {Lazy Abstraction},
  author	= {Henzinger, Thomas A. and Jhala, Ranjit and Majumdar, Rupak
		  and Sutre, Grégoire},
  booktitle	= {CONF_POPL 2002},
  year		= {2002},
  publisher	= {Association for Computing Machinery},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/503272.503279},
  doi		= {10.1145/503272.503279},
  pages		= {58–70},
  numpages	= {13},
  location	= {Portland, Oregon},
  ids		= {blast}
}

@InProceedings{	  henzinger2013:aspect-linear,
  title		= {Aspect-Oriented Linearizability Proofs},
  author	= {Thomas A. Henzinger and Ali Sezgin and Viktor Vafeiadis},
  booktitle	= {CONF_CONCUR 2013},
  year		= {2013},
  pages		= {242-256},
  editor	= {Pedro R. D'Argenio and Hernán C. Melgratti},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {8052}
}

@Article{	  herlify1991:wait-free,
  title		= {Wait-Free Synchronization},
  author	= {Maurice Herlihy},
  journal	= {JNL_TOPLAS},
  year		= {1991},
  volume	= {13},
  number	= {1},
  pages		= {124-149}
}

@Article{	  herlihy1990:linearizability,
  title		= {Linearizability: {A} Correctness Condition for Concurrent
		  Objects},
  author	= {Maurice Herlihy and Jeannette M. Wing},
  journal	= {JNL_TOPLAS},
  year		= {1990},
  volume	= {12},
  number	= {3},
  pages		= {463-492},
  doi		= {10.1145/78969.78972},
  ids		= {linearizability, herlihy-wing}
}

@InProceedings{	  herlihy2003:obstruction-free,
  title		= {Obstruction-Free Synchronization: {Double}-Ended Queues as
		  an Example},
  author	= {Maurice Herlihy and Victor Luchangco and Mark Moir},
  booktitle	= {CONF_ICDCS 2003},
  year		= {2003},
  pages		= {522-529},
  publisher	= {IEEE Computer Society},
  ids		= {obstruction-free}
}

@InProceedings{	  hoare2003:grand-challenge,
  title		= {The Verifying Compiler: A Grand Challenge for Computing
		  Research},
  author	= {Hoare, Tony},
  booktitle	= {CONF_JMLC 2003},
  year		= {2003},
  editor	= {Böszörményi, László and Schojer, Peter},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {25--35}
}

@InProceedings{	  hoare2008:grand-challenge,
  title		= {Verified Software: Theories, Tools, Experiments Vision of
		  a Grand Challenge Project},
  author	= {Hoare, Tony and Misra, Jay},
  booktitle	= {CONF_VSTTE 2005},
  year		= {2008},
  editor	= {Meyer, Bertrand and Woodcock, Jim},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {1--18},
  doi		= {10.1007/978-3-540-69149-5_1},
  url		= {https://doi.org/10.1007/978-3-540-69149-5_1}
}

@InProceedings{	  hoare2009:cka,
  title		= {Concurrent Kleene Algebra},
  author	= {C. A. R. Hoare and Bernhard Möller and Georg Struth and
		  Ian Wehrman},
  booktitle	= {CONF_CONCUR 2009},
  year		= {2009},
  editor	= {Mario Bravetti and Gianluigi Zavattaro},
  series	= {LNCS},
  volume	= {5710},
  pages		= {399--414},
  publisher	= {Springer},
  doi		= {10.1007/978-3-642-04081-8_27}
}

@InProceedings{	  hobor2010:indirection,
  title		= {A theory of indirection via approximation},
  author	= {Hobor, Aquinas and Dockins, Robert and Appel, Andrew W.},
  booktitle	= {CONF_POPL 2010},
  year		= {2010},
  pages		= {171--184},
  publisher	= {ACM},
  address	= {New York, NY, USA}
}

@InProceedings{	  hoffman2007:baskets-queue,
  title		= {The Baskets Queue},
  author	= {Hoffman, Moshe and Shalev, Ori and Shavit, Nir},
  booktitle	= {CONF_OPODIS 2007},
  year		= {2007},
  doi		= {10.1007/978-3-540-77096-1_29},
  opteditor	= {Tovar, Eduardo and Tsigas, Philippas and Fouchal,
		  Hacène},
  publisher	= {Springer Berlin Heidelberg},
  address	= {Berlin, Heidelberg},
  pages		= {401--414}
}

@InProceedings{	  hoffman2007:baskets-queue,
  title		= {The baskets queue},
  author	= {Hoffman, Moshe and Shalev, Ori and Shavit, Nir},
  booktitle	= {CONF_OPODIS 2007},
  year		= {2007},
  pages		= {401-414},
  editor	= {Eduardo Tovar and Philippas Tsigas and Hacène Fouchal},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {4878}
}

@Article{	  holzmann1997:spin,
  title		= {The model checker SPIN},
  author	= {Holzmann, G.J.},
  journal	= {JNL_TSE},
  year		= {1997},
  volume	= {23},
  number	= {5},
  pages		= {279-295},
  doi		= {10.1109/32.588521},
  ids		= {spin}
}

@InProceedings{	  huang2015:mcr,
  title		= {Stateless model checking concurrent programs with maximal
		  causality reduction},
  author	= {Jeff Huang},
  booktitle	= {CONF_PLDI 2015},
  year		= {2015},
  pages		= {165--174},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {http://doi.acm.org/10.1145/2737924.2737975},
  doi		= {10.1145/2737924.2737975},
  ids		= {MCR, mcr}
}

@InProceedings{	  huang2016:mcr-tso-pso,
  title		= {Maximal Causality Reduction for {TSO} and {PSO}},
  author	= {Huang, Shiyou and Huang, Jeff},
  booktitle	= {CONF_OOPSLA 2016},
  year		= {2016},
  pages		= {447--461},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {http://doi.acm.org/10.1145/2983990.2984025},
  doi		= {10.1145/2983990.2984025},
  ids		= {mcr-tso-pso, MCR-TSO-PSO}
}

@InProceedings{	  hur2011:gcsl,
  title		= {Separation logic in the presence of garbage collection},
  author	= {Chung-Kil Hur and Derek Dreyer and Viktor Vafeiadis},
  booktitle	= {CONF_LICS 2011},
  year		= {2011},
  publisher	= {IEEE},
  ids		= {gcsl}
}

@InProceedings{	  hur2012:rts,
  title		= {The marriage of bisimulations and {K}ripke logical
		  relations},
  author	= {Chung-Kil Hur and Derek Dreyer and Georg Neis and Viktor
		  Vafeiadis},
  booktitle	= {CONF_POPL 2012},
  year		= {2012},
  publisher	= {ACM}
}

@InProceedings{	  hur2013:paco,
  title		= {The power of parameterization in coinductive proof},
  author	= {Chung-Kil Hur and Georg Neis and Derek Dreyer and Viktor
		  Vafeiadis},
  booktitle	= {CONF_POPL 2013},
  year		= {2013},
  publisher	= {ACM},
  ids		= {paco}
}

@InCollection{	  hutchison2014:verified-flash-fs,
  title		= {Development of a Verified Flash File System},
  author	= {Schellhorn, Gerhard and Ernst, Gidon and Pfähler, Jörg
		  and Haneberg, Dominik and Reif, Wolfgang},
  booktitle	= {CONF_ABZ 2014},
  location	= {Berlin, Heidelberg},
  volume	= {8477},
  url		= {http://link.springer.com/10.1007/978-3-662-43652-3_2},
  pages		= {9--24},
  urldate	= {2020-06-17},
  date		= {2014},
  langid	= {english},
  doi		= {10.1007/978-3-662-43652-3_2}
}

@Article{	  jagadeesan2020:pomsets,
  title		= {Pomsets with preconditions: A simple model of relaxed
		  memory},
  author	= {Jagadeesan, Radha and Jeffrey, Alan and Riely, James},
  journal	= {JNL_PACMPL},
  year		= {2020},
  month		= nov,
  issue_date	= {November 2020},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {4},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3428262},
  doi		= {10.1145/3428262},
  articleno	= {194},
  numpages	= {30},
  keywords	= {Thin-Air Reads, Compiler Optimizations, Concurrency,
		  Multi-Copy Atomicity, Preconditions, Pomsets, ARMv8,
		  Relaxed Memory Models, Temporal Safety Properties}
}

@Article{	  jagannathan2014:atomicity,
  title		= {Atomicity refinement for verified compilation},
  author	= {Suresh Jagannathan and Vincent Laporte and Gustavo Petri
		  and David Pichardie and Jan Vitek},
  journal	= {JNL_TOPLAS},
  year		= {2014},
  volume	= {36},
  number	= {2},
  pages		= {6:1--6:30},
  doi		= {10.1145/2601339}
}

@InProceedings{	  jeffrey2016:out-of-thin-air,
  title		= {On thin air reads: {Towards} an event structures model of
		  relaxed memory},
  author	= {Alan Jeffrey and James Riely},
  booktitle	= {CONF_LICS 2016},
  year		= 2016,
  url		= {https://doi.org/10.1145/2933575.2934536},
  doi		= {10.1145/2933575.2934536},
  pages		= {759--767},
  publisher	= {ACM},
  location	= {New York, NY, USA}
}

@Article{	  jeffrey2022:leaky-semicolon,
  title		= {The leaky semicolon: {Compositional} semantic dependencies
		  for relaxed-memory concurrency},
  author	= {Alan Jeffrey and James Riely and Mark Batty and Simon
		  Cooksey and Ilya Kaysin and Anton Podkopaev},
  journal	= {JNL_PACMPL},
  year		= {2022},
  volume	= {6},
  number	= {{POPL}},
  pages		= {1--30},
  publisher	= {ACM},
  location	= {New York, NY, USA},
  doi		= {10.1145/3498716}
}

@InProceedings{	  jensen2012:fsl,
  title		= {Fictional separation logic},
  author	= {Jonas Jensen and Lars Birkedal},
  booktitle	= {CONF_ESOP 2012},
  year		= 2012
}

@Article{	  jhala2009:model-checking,
  title		= {Software model checking},
  author	= {Ranjit Jhala and Rupak Majumdar},
  journal	= {JNL_ACMCS},
  year		= {2009},
  volume	= {41},
  number	= {4}
}

@InProceedings{	  jipsen2014:ckat,
  title		= {Concurrent Kleene Algebra with Tests},
  author	= {Peter Jipsen},
  booktitle	= {CONF_RAMICS 2014},
  year		= {2014},
  editor	= {Peter Höfner and Peter Jipsen and Wolfram Kahl and Martin
		  Eric Müller},
  series	= {LNCS},
  volume	= {8428},
  pages		= {37--48},
  publisher	= {Springer},
  doi		= {10.1007/978-3-319-06251-8_3}
}

@InProceedings{	  joerg1995:mc-hardware,
  title		= {Model Checking in Industrial Hardware Design},
  author	= {Bormann, Jörg and Lohse, Jörg and Payer, Michael and
		  Venzl, Gerd},
  booktitle	= {CONF_DAC 1995},
  year		= {1995},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/217474.217545},
  doi		= {10.1145/217474.217545},
  pages		= {298–303},
  numpages	= {6},
  location	= {San Francisco, California, USA}
}

@InProceedings{	  jones1932:specification,
  title		= {Specification and Design of (Parallel) Programs},
  author	= {Cliff B. Jones},
  booktitle	= {CONF_IFIP 1983},
  year		= {1983},
  pages		= {321-332}
}

@InProceedings{	  jonsson2022:godot,
  title		= {Awaiting for {Godot}: {Stateless} Model Checking that
		  Avoids Executions where Nothing Happens},
  author	= {Jonsson, Bengt and Lång, Magnus and Sagonas,
		  Konstantinos},
  booktitle	= {CONF_FMCAD 2022},
  year		= {2022},
  pages		= {163--172},
  publisher	= {{TU Wien Academic Press}},
  doi		= {10.34727/2022/isbn.978-3-85448-053-2_35},
  ids		= {godot}
}

@Article{	  joshi2007:verifiable-fs,
  title		= {A Mini Challenge: Build a Verifiable Filesystem},
  author	= {Joshi, Rajeev and Holzmann, Gerard},
  volume	= {19},
  doi		= {10.1007/s00165-006-0022-3},
  shorttitle	= {A Mini Challenge},
  pages		= {269--272},
  journaltitle	= {JNL_FAC},
  shortjournal	= {JNL_FAC},
  date		= {2007-06-11}
}

@InProceedings{	  junfeng2006:gen-disks-symbolic,
  title		= {Automatically generating malicious disks using symbolic
		  execution},
  author	= {{Junfeng Yang} and {Can Sar} and Twohey, P. and Cadar, C.
		  and Engler, D.},
  booktitle	= {CONF_SP 2006},
  location	= {Berkeley/Oakland, {CA}},
  url		= {http://ieeexplore.ieee.org/document/1624015/},
  doi		= {10.1109/SP.2006.7},
  pages		= {15 pp.--257},
  publisher	= {{IEEE}},
  urldate	= {2020-06-17},
  date		= {2006},
  langid	= {english}
}

@Article{	  jung2018:iris,
  title		= {Iris from the ground up: A modular foundation for
		  higher-order concurrent separation logic},
  author	= {Jung, Ralf and Krebbers, Robbert and Jourdan,
		  Jacques-Henri and Bizjak, Aleš and Birkedal, Lars and
		  Dreyer, Derek},
  journal	= {JNL_JFP},
  year		= {2018},
  volume	= {28},
  doi		= {10.1017/S0956796818000151},
  publisher	= {Cambridge University Press},
  pages		= {e20},
  ids		= {iris-jfp,iris-journal}
}

@Article{	  kahkonen2015:unfolding,
  title		= {Unfolding Based Automated Testing of Multithreaded
		  Programs},
  author	= {Kari Kähkönen and Olli Saarikivi and Keijo Heljanko},
  journal	= {JNL_ASE},
  year		= {2015},
  month		= dec,
  volume	= {22},
  number	= {4},
  pages		= {475--515},
  publisher	= {Springer},
  url		= {http://dx.doi.org/10.1007/s10515-014-0150-6},
  doi		= {10.1007/s10515-014-0150-6}
}

@InProceedings{	  kaiser2017:iris-weak-memory,
  title		= {Strong Logic for Weak Memory: {Reasoning} About
		  Release-Acquire Consistency in {Iris}},
  author	= {Jan-Oliver Kaiser and Hoang-Hai Dang and Derek Dreyer and
		  Ori Lahav and Viktor Vafeiadis},
  booktitle	= {CONF_ECOOP 2017},
  year		= {2017},
  pages		= {17:1--17:29},
  series	= {LIPIcs},
  volume	= {74},
  editor	= {Peter Müller},
  publisher	= {DAGSTUHL},
  address	= {Dagstuhl, Germany},
  doi		= {10.4230/LIPIcs.ECOOP.2017.17},
  ids		= {igps}
}

@Article{	  kameda1970:nfa,
  title		= {On the State Minimization of Nondeterministic Finite
		  Automata},
  author	= {Tsunehiko Kameda and Peter Weiner},
  journal	= {JNL_TCOMPUT},
  year		= {1970},
  volume	= {19},
  number	= {7},
  pages		= {617--627},
  url		= {https://doi.org/10.1109/T-C.1970.222994},
  doi		= {10.1109/T-C.1970.222994},
  ids		= {kameda-weiner}
}

@Article{	  kaminski1994:automata,
  title		= {Finite-Memory Automata},
  author	= {Michael Kaminski and Nissim Francez},
  journal	= {JNL_TCS},
  year		= {1994},
  volume	= {134},
  number	= {2},
  pages		= {329--363},
  url		= {https://doi.org/10.1016/0304-3975(94)90242-9},
  doi		= {10.1016/0304-3975(94)90242-9}
}

@InProceedings{	  kang2017:promising,
  title		= {A promising semantics for relaxed-memory concurrency},
  author	= {Kang, Jeehoon and Hur, Chung-Kil and Lahav, Ori and
		  Vafeiadis, Viktor and Dreyer, Derek},
  booktitle	= {CONF_POPL 2017},
  year		= {2017},
  location	= {Paris, France},
  pages		= {175--189},
  numpages	= {15},
  doi		= {10.1145/3009837.3009850},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {promising}
}

@InProceedings{	  kant2015:ltsmin,
  title		= {LTSmin: High-Performance Language-Independent Model
		  Checking},
  author	= {Kant, Gijs and Laarman, Alfons and Meijer, Jeroen and van
		  de Pol, Jaco and Blom, Stefan and van Dijk, Tom},
  booktitle	= {CONF_TACAS 2015},
  year		= {2015},
  editor	= {Baier, Christel and Tinelli, Cesare},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {692--707},
  doi		= {10.1007/978-3-662-46681-0_61},
  ids		= {ltsmin}
}

@InProceedings{	  kappe2019:kao,
  title		= {Kleene Algebra with Observations},
  author	= {Tobias Kappé and Paul Brunet and Jurriaan Rot and
		  Alexandra Silva and Jana Wagemaker and Fabio Zanasi},
  booktitle	= {CONF_CONCUR 2019},
  year		= {2019},
  editor	= {Wan J. Fokkink and Rob van Glabbeek},
  series	= {LIPIcs},
  volume	= {140},
  pages		= {41:1--41:16},
  publisher	= {DAGSTUHL},
  url		= {https://doi.org/10.4230/LIPIcs.CONCUR.2019.41},
  doi		= {10.4230/LIPICS.CONCUR.2019.41}
}

@Article{	  kappe2020:ckao,
  title		= {Concurrent Kleene Algebra with Observations: from
		  Hypotheses to Completeness},
  author	= {Tobias Kappé and Paul Brunet and Alexandra Silva and Jana
		  Wagemaker and Fabio Zanasi},
  journal	= {JNL_CORR},
  year		= {2020},
  volume	= {abs/2002.09682},
  url		= {https://arxiv.org/abs/2002.09682},
  eprinttype	= {arXiv},
  eprint	= {2002.09682}
}

@InProceedings{	  keller2013:fs-verification,
  title		= {File systems deserve verification too!},
  author	= {Keller, Gabriele and Murray, Toby and Amani, Sidney and
		  O'Connor, Liam and Chen, Zilin and Ryzhyk, Leonid and
		  Klein, Gerwin and Heiser, Gernot},
  booktitle	= {CONF_PLOS 2013},
  year		= 2013,
  location	= {Farmington, Pennsylvania},
  doi		= {10.1145/2525528.2525530},
  pages		= {1--7},
  publisher	= {ACM}
}

@InProceedings{	  khyzha2016:logic-for-linearizability,
  title		= {A Generic Logic for Proving Linearizability},
  author	= {Artem Khyzha and Alexey Gotsman and Matthew J. Parkinson},
  booktitle	= {CONF_FM 2016},
  year		= {2016},
  editor	= {John S. Fitzgerald and Constance L. Heitmeyer and Stefania
		  Gnesi and Anna Philippou},
  series	= {LNCS},
  volume	= {9995},
  pages		= {426--443},
  publisher	= {Springer},
  doi		= {10.1007/978-3-319-48989-6_26}
}

@InProceedings{	  killian2007:liveness,
  title		= {Life, death, and the critical transition: finding liveness
		  bugs in systems code},
  author	= {Killian, Charles and Anderson, James W. and Jhala, Ranjit
		  and Vahdat, Amin},
  booktitle	= {CONF_NSDI 2007},
  year		= {2007},
  publisher	= {USENIX Association},
  address	= {USA},
  pages		= {18},
  numpages	= {1},
  location	= {Cambridge, MA},
  optseries	= {NSDI'07},
  doi		= {10.5555/1973430.1973448}
}

@InProceedings{	  killian2007:mace,
  title		= {Mace: Language Support for Building Distributed Systems},
  author	= {Killian, Charles Edwin and Anderson, James W. and Braud,
		  Ryan and Jhala, Ranjit and Vahdat, Amin M.},
  booktitle	= {CONF_PLDI 2007},
  year		= {2007},
  isbn		= {9781595936332},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/1250734.1250755},
  doi		= {10.1145/1250734.1250755},
  pages		= {179–188},
  numpages	= {10},
  location	= {San Diego, California, USA}
}

@Article{	  king1976:symbolic,
  title		= {Symbolic execution and program testing},
  author	= {King, James C.},
  journal	= {JNL_CACM},
  year		= {1976},
  month		= jul,
  issue_date	= {July 1976},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {19},
  number	= {7},
  issn		= {0001-0782},
  url		= {https://doi.org/10.1145/360248.360252},
  doi		= {10.1145/360248.360252},
  pages		= {385–394},
  numpages	= {10}
}

@InProceedings{	  kini2017:race-prediction,
  title		= {Dynamic race prediction in linear time},
  author	= {Kini, Dileep and Mathur, Umang and Viswanathan, Mahesh},
  booktitle	= {CONF_PLDI 2017},
  year		= {2017},
  location	= {Barcelona, Spain},
  pages		= {157--170},
  numpages	= {14},
  url		= {http://doi.acm.org/10.1145/3062341.3062374},
  doi		= {10.1145/3062341.3062374},
  acmid		= {3062374},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {Concurrency, Online Algorithm, Race Prediction}
}

@Article{	  knuth1975:estimation,
  title		= {Estimating the Efficiency of Backtrack Programs},
  author	= {Donald E. Knuth},
  journal	= {JNL_MOS},
  year		= {1975},
  number	= {129},
  pages		= {121--136},
  publisher	= {AMS},
  urldate	= {2023-10-16},
  volume	= {29},
  doi		= {10.2307/2005469}
}

@Article{	  kokologiannakis2017:rcmc,
  title		= {Effective stateless model checking for {C/C++}
		  concurrency},
  author	= {Kokologiannakis, Michalis and Lahav, Ori and Sagonas,
		  Konstantinos and Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2017},
  month		= dec,
  issue_date	= {January 2018},
  volume	= {2},
  number	= {POPL},
  pages		= {17:1--17:32},
  articleno	= {17},
  numpages	= {32},
  doi		= {10.1145/3158105},
  acmid		= {3158105},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {rcmc}
}

@InProceedings{	  kokologiannakis2017:rcu,
  title		= {Stateless model checking of the Linux kernel's
		  hierarchical read-copy-update ({Tree} {RCU})},
  author	= {Kokologiannakis, Michalis and Sagonas, Konstantinos},
  booktitle	= {CONF_SPIN 2017},
  year		= {2017},
  location	= {Santa Barbara, CA, USA},
  pages		= {172--181},
  numpages	= {10},
  url		= {http://doi.acm.org/10.1145/3092282.3092287},
  doi		= {10.1145/3092282.3092287},
  acmid		= {3092287},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {Linux kernel, Nidhugg, Read-Copy-Update, Software model
		  checking}
}

@InProceedings{	  kokologiannakis2019:genmc,
  title		= {Model checking for weakly consistent libraries},
  author	= {Kokologiannakis, Michalis and Raad, Azalea and Vafeiadis,
		  Viktor},
  booktitle	= {CONF_PLDI 2019},
  year		= {2019},
  numpages	= {15},
  doi		= {10.1145/3314221.3314609},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {genmc}
}

@Article{	  kokologiannakis2019:lapor,
  title		= {Effective lock handling in stateless model checking},
  author	= {Kokologiannakis, Michalis and Raad, Azalea and Vafeiadis,
		  Viktor},
  journal	= {JNL_PACMPL},
  year		= {2019},
  month		= oct,
  issue_date	= {October 2019},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {3},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3360599},
  doi		= {10.1145/3360599},
  articleno	= {173},
  numpages	= {26},
  keywords	= {Model checking, mutual exclusion locks, weak memory
		  models},
  ids		= {lapor}
}

@Article{	  kokologiannakis2019:rcu,
  title		= {Stateless model checking of the Linux kernel's read--copy
		  update ({RCU})},
  author	= {Kokologiannakis, Michalis and Sagonas, Konstantinos},
  journal	= {JNL_STTT},
  year		= {2019},
  month		= mar,
  day		= {11},
  doi		= {10.1007/s10009-019-00514-6}
}

@InProceedings{	  kokologiannakis2020:hmc,
  title		= {{HMC}: Model checking for hardware memory models},
  author	= {Kokologiannakis, Michalis and Vafeiadis, Viktor},
  booktitle	= {CONF_ASPLOS 2020},
  year		= {2020},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/3373376.3378480},
  doi		= {10.1145/3373376.3378480},
  pages		= {1157–1171},
  numpages	= {15},
  keywords	= {weak memory models, model checking},
  location	= {Lausanne, Switzerland},
  series	= {ASPLOS ’20},
  ids		= {hmc}
}

@InProceedings{	  kokologiannakis2021:bam,
  title		= {{BAM}: {Efficient} Model Checking for Barriers},
  author	= {Kokologiannakis, Michalis and Vafeiadis, Viktor},
  booktitle	= {CONF_NETYS 2021},
  year		= {2021},
  url		= {https://plv.mpi-sws.org/genmc},
  doi		= {10.1007/978-3-030-91014-3_16},
  series	= {LNCS},
  publisher	= {Springer},
  ids		= {bam}
}

@InProceedings{	  kokologiannakis2021:genmc-tool,
  title		= {{GenMC}: {A} model checker for weak memory models},
  author	= {Michalis Kokologiannakis and Viktor Vafeiadis},
  booktitle	= {CONF_CAV 2021},
  year		= {2021},
  editor	= {Alexandra Silva and K. Rustan M. Leino},
  series	= {LNCS},
  volume	= {12759},
  pages		= {427--440},
  publisher	= {Springer},
  doi		= {10.1007/978-3-030-81685-8_20},
  ids		= {genmc-tool}
}

@Article{	  kokologiannakis2021:persevere,
  title		= {{PerSeVerE}: {Persistency} semantics for verification
		  under ext4},
  author	= {Kokologiannakis, Michalis and Kaysin, Ilya and Raad,
		  Azalea and Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2021},
  month		= jan,
  issue_date	= {January 2021},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {5},
  number	= {POPL},
  url		= {https://doi.org/10.1145/3434324},
  doi		= {10.1145/3434324},
  articleno	= {43},
  numpages	= {26},
  keywords	= {File Systems; Persistency; Weak Consistency; Model
		  Checking},
  ids		= {persevere}
}

@Article{	  kokologiannakis2021:persevere-supp-material,
  title		= {{PerSeVerE}: {Persistency} semantics for verification
		  under ext4 (supplementary material)},
  author	= {Kokologiannakis, Michalis and Kaysin, Ilya and Raad,
		  Azalea and Vafeiadis, Viktor},
  year		= {2021},
  month		= jan,
  url		= {https://plv.mpi-sws.org/persevere},
  ids		= {persevere-supp-material}
}

@InProceedings{	  kokologiannakis2021:saver,
  title		= {Dynamic Partial Order Reductions for Spinloops},
  author	= {Michalis Kokologiannakis and Xiaowei Ren and Viktor
		  Vafeiadis},
  booktitle	= {CONF_FMCAD 2021},
  year		= {2021},
  pages		= {163--172},
  publisher	= {{IEEE}},
  doi		= {10.34727/2021/isbn.978-3-85448-046-4_25},
  ids		= {saver}
}

@Article{	  kokologiannakis2022:trust,
  title		= {Truly stateless, optimal dynamic partial order reduction},
  author	= {Kokologiannakis, Michalis and Marmanis, Iason and
		  Gladstein, Vladimir and Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2022},
  month		= jan,
  issue_date	= {January 2022},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {6},
  number	= {POPL},
  url		= {https://doi.org/10.1145/3498711},
  doi		= {10.1145/3498711},
  articleno	= {49},
  numpages	= {26},
  keywords	= {Model Checking; Dynamic Partial Order Reduction; Weak
		  Memory Models},
  ids		= {trust}
}

@Article{	  kokologiannakis2022:trust-supp-material,
  title		= {Truly Stateless, Optimal Dynamic Partial Order Reduction
		  (supplementary material)},
  author	= {Kokologiannakis, Michalis and Marmanis, Iason and
		  Gladstein, Vladimir and Vafeiadis, Viktor},
  year		= {2022},
  month		= jan,
  url		= {https://plv.mpi-sws.org/genmc},
  ids		= {trust-supp-material}
}

@InProceedings{	  kokologiannakis2023:awamoche,
  title		= {Unblocking Dynamic Partial Order Reduction},
  author	= {Kokologiannakis, Michalis and Marmanis, Iason and
		  Vafeiadis, Viktor},
  booktitle	= {CONF_CAV 2023},
  year		= {2023},
  series	= {LNCS},
  editor	= {Constantin Enea and Akash Lal},
  volume	= {13964},
  pages		= {230--250},
  publisher	= {Springer},
  doi		= {10.1007/978-3-031-37706-8_12},
  ids		= {awamoche}
}

@Misc{		  kokologiannakis2023:gater-artifact,
  title		= {Enhancing GenMC's Usability and Performance (Replication
		  Package)},
  author	= {Kokologiannakis, Michalis and Majumdar, Rupak and
		  Vafeiadis, Viktor},
  year		= {2024},
  month		= apr,
  url		= {https://zenodo.org/doi/10.5281/zenodo.10018135},
  doi		= {10.5281/zenodo.10018135},
  ids		= {gater-artifact}
}

@Article{	  kokologiannakis2023:kater,
  title		= {{Kater}: {Automating} Weak Memory Model Metatheory and
		  Consistency Checking},
  author	= {Kokologiannakis, Michalis and Lahav, Ori and Vafeiadis,
		  Viktor},
  journal	= {JNL_PACMPL},
  year		= {2023},
  month		= {jan},
  issue_date	= {January 2023},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {7},
  number	= {POPL},
  url		= {https://doi.org/10.1145/3571212},
  doi		= {10.1145/3571212},
  articleno	= {19},
  numpages	= {29},
  ids		= {kater}
}

@Article{	  kokologiannakis2023:thesis-artifact,
  title		= {Automated Reasoning under Weak Memory Consistency
		  (replication package)},
  author	= {Kokologiannakis, Michalis},
  year		= {2023},
  month		= jun,
  ids		= {thesis-artifact},
  doi		= {10.5281/zenodo.10575926}
}

@Article{	  kokologiannakis2024:spore,
  title		= {{SPORE}: {Combining} Symmetry and Partial Order
		  Reduction},
  author	= {Michalis Kokologiannakis and Iason Marmanis and Viktor
		  Vafeiadis},
  journal	= {JNL_PACMPL},
  year		= {2024},
  month		= jun,
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {8},
  number	= {PLDI},
  doi		= {10.1145/3656449},
  ids		= {spore}
}

@Misc{		  kokologiannakis2024:spore-artifact,
  title		= {{SPORE}: {Combining} Symmetry and Partial Order Reduction
		  (Replication Package)},
  author	= {Kokologiannakis, Michalis and Marmanis, Iason and
		  Vafeiadis, Viktor},
  year		= {2024},
  month		= jun,
  url		= {https://zenodo.org/doi/10.5281/zenodo.10798179},
  doi		= {10.5281/zenodo.10798179},
  ids		= {spore-artifact}
}

@Article{	  kokologiannakis2024:spore-supp-material,
  title		= {Spore: Combining Symmetry and Partial Order Reduction
		  (supplementary material)},
  author	= {Kokologiannakis, Michalis and Marmanis, Iason and
		  Vafeiadis, Viktor},
  year		= {2024},
  month		= jun,
  url		= {https://plv.mpi-sws.org/genmc},
  ids		= {spore-supp-material}
}

@Article{	  koutsouridis2024:kati-supp-material,
  title		= {Automating Memory Model Metatheory with Intersections
		  (supplementary material)},
  author	= {Koutsouridis, Aristotelis and Kokologiannakis, Michalis
		  and Vafeiadis, Viktor},
  year		= {2024},
  month		= apr,
  opturl	= {https://plv.mpi-sws.org/kater},
  ids		= {kati-supp-material}
}

@InProceedings{	  koval2023:lincheck,
  title		= {Lincheck: A Practical Framework for Testing Concurrent
		  Data Structures on {JVM}},
  author	= {Koval, Nikita and Fedorov, Alexander and Sokolova, Maria
		  and Tsitelov, Dmitry and Alistarh, Dan},
  booktitle	= {CONF_CAV 2023},
  year		= 2023,
  editor	= {Enea, Constantin and Lal, Akash},
  pages		= {156--169},
  address	= {Cham},
  doi		= {10.1007/978-3-031-37706-8_8},
  publisher	= {Springer},
  ids		= {lincheck}
}

@InProceedings{	  kozen1996:kat-completeness,
  title		= {Kleene Algebra with Tests: Completeness and Decidability},
  author	= {Dexter Kozen and Frederick Smith},
  booktitle	= {CONF_CSL 1996},
  year		= {1996},
  editor	= {Dirk van Dalen and Marc Bezem},
  series	= {LNCS},
  volume	= {1258},
  pages		= {244--259},
  publisher	= {Springer},
  doi		= {10.1007/3-540-63172-0_43},
  ids		= {kozen-kat-completeness}
}

@Article{	  kozen1997:kat,
  title		= {Kleene Algebra with Tests},
  author	= {Dexter Kozen},
  journal	= {JNL_TOPLAS},
  year		= {1997},
  volume	= {19},
  number	= {3},
  optpages	= {427--443},
  url		= {https://doi.org/10.1145/256167.256195},
  doi		= {10.1145/256167.256195},
  opttimestamp	= {Thu, 14 Oct 2021 09:12:21 +0200},
  optbiburl	= {https://dblp.org/rec/journals/toplas/Kozen97.bib},
  optbibsource	= {dblp computer science bibliography, https://dblp.org},
  ids		= {kat}
}

@Article{	  kulik2022:survey-fm-security,
  title		= {A Survey of Practical Formal Methods for Security},
  author	= {Kulik, Tomas and Dongol, Brijesh and Larsen, Peter Gorm
		  and Macedo, Hugo Daniel and Schneider, Steve and
		  Tran-Jørgensen, Peter W. V. and Woodcock, James},
  journal	= {JNL_FAC},
  year		= {2022},
  month		= {jul},
  issue_date	= {March 2022},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {34},
  number	= {1},
  doi		= {10.1145/3522582},
  articleno	= {5},
  numpages	= {39}
}

@InProceedings{	  kuperstein2011:abstractions,
  title		= {Partial-coherence abstractions for relaxed memory models},
  author	= {Kuperstein, Michael and Vechev, Martin and Yahav, Eran},
  booktitle	= {CONF_PLDI 2011},
  year		= {2011},
  pages		= {187--198},
  doi		= {10.1145/1993498.1993521},
  publisher	= {ACM}
}

@Article{	  kuperstein2012:fences,
  title		= {Automatic inference of memory fences},
  author	= {Michael Kuperstein and Martin T. Vechev and Eran Yahav},
  journal	= {JNL_SIGACTNEWS},
  year		= {2012},
  volume	= {43},
  number	= {2},
  pages		= {108-123}
}

@InProceedings{	  laadan2011:races-systems,
  title		= {Pervasive detection of process races in deployed systems},
  author	= {Laadan, Oren and Viennot, Nicolas and Tsai, Chia-Che and
		  Blinn, Chris and Yang, Junfeng and Nieh, Jason},
  booktitle	= {CONF_SOSP 2011},
  location	= {Cascais, Portugal},
  url		= {https://doi.org/10.1145/2043556.2043589},
  doi		= {10.1145/2043556.2043589},
  series	= {CONF_SOSP 2011},
  pages		= {353--367},
  publisher	= {ACM},
  urldate	= {2020-06-17},
  date		= {2011-10-23},
  keywords	= {debugging, model checking, race detection, record-replay}
}

@InProceedings{	  ladan-mozes2004:optimistic,
  title		= {An Optimistic Approach to Lock-Free {FIFO} Queues},
  author	= {Edya Ladan-Mozes and Nir Shavit},
  booktitle	= {CONF_DISC 2004},
  year		= {2004},
  pages		= {117-131},
  editor	= {Rachid Guerraoui},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {3274},
  ids		= {ms-queue-optimistic, msqueue-optimistic,
		  optimistic-msqueue}
}

@Article{	  ladan-mozes2008:optimistic-msqueue,
  title		= {An optimistic approach to lock-free FIFO queues},
  author	= {Ladan-Mozes, Edya and Shavit, Nir},
  journal	= {JNL_DC},
  year		= {2008},
  month		= {02},
  pages		= {323-341},
  volume	= {20},
  doi		= {10.1007/s00446-007-0050-0},
  ids		= {ms-queue-optimistic-journal,optimistic-msqueue-journal}
}

@InProceedings{	  lahav2015:owicki-gries,
  title		= {{O}wicki-{G}ries Reasoning for Weak Memory Models},
  author	= {Lahav, Ori and Vafeiadis, Viktor},
  booktitle	= {CONF_ICALP 2015},
  year		= {2015},
  volume	= {9135},
  series	= {LNCS},
  editor	= {Halldórsson, Magnús M. and Iwama, Kazuo and Kobayashi,
		  Naoki and Speckmann, Bettina},
  publisher	= {Springer},
  pages		= {311--323},
  doi		= {10.1007/978-3-662-47666-6_25},
  ids		= {ogra}
}

@InProceedings{	  lahav2016:taming,
  title		= {Taming Release-acquire Consistency},
  author	= {Lahav, Ori and Giannarakis, Nick and Vafeiadis, Viktor},
  booktitle	= {CONF_POPL 2016},
  year		= {2016},
  location	= {St. Petersburg, FL, USA},
  pages		= {649--662},
  numpages	= {14},
  url		= {http://doi.acm.org/10.1145/2837614.2837643},
  doi		= {10.1145/2837614.2837643},
  acmid		= {2837643},
  publisher	= {ACM},
  optaddress	= {New York, NY, USA},
  keywords	= {C11, Weak memory model, operational semantics,
		  release-acquire},
  ids		= {sra, strong-release-acquire, strong-ra}
}

@InProceedings{	  lahav2016:transformations,
  title		= {Explaining Relaxed Memory Models with Program
		  Transformations},
  author	= {Ori Lahav and Viktor Vafeiadis},
  booktitle	= {CONF_FM 2016},
  year		= {2016},
  pages		= {479--495},
  url		= {http://dx.doi.org/10.1007/978-3-319-48989-6_29},
  doi		= {10.1007/978-3-319-48989-6_29},
  publisher	= {Springer}
}

@InProceedings{	  lahav2017:repairing,
  title		= {Repairing sequential consistency in {C/C++11}},
  author	= {Lahav, Ori and Vafeiadis, Viktor and Kang, Jeehoon and
		  Hur, Chung-Kil and Dreyer, Derek},
  booktitle	= {CONF_PLDI 2017},
  year		= {2017},
  location	= {Barcelona, Spain},
  pages		= {618--632},
  numpages	= {15},
  url		= {http://doi.acm.org/10.1145/3062341.3062352},
  doi		= {10.1145/3062341.3062352},
  acmid		= {3062352},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {C++11, Weak memory models, declarative semantics,
		  sequential consistency},
  ids		= {rc11, RC11}
}

@Article{	  lahav2021:fairness,
  title		= {Making Weak Memory Models Fair},
  author	= {Lahav, Ori and Namakonov, Egor and Oberhauser, Jonas and
		  Podkopaev, Anton and Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2021},
  month		= {oct},
  issue_date	= {October 2021},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {5},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3485475},
  doi		= {10.1145/3485475},
  articleno	= {98},
  numpages	= {27},
  keywords	= {Formal semantics, weak memory models, verification,
		  concurrency}
}

@Article{	  lahav2023:lib,
  title		= {An Operational Approach to Library Abstraction under
		  Relaxed Memory Concurrency},
  author	= {Singh, Abhishek Kr and Lahav, Ori},
  journal	= {Proc. ACM Program. Lang.},
  year		= 2023,
  month		= {jan},
  volume	= 7,
  number	= {POPL},
  issue_date	= {January 2023},
  publisher	= {Association for Computing Machinery},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/3571246},
  doi		= {10.1145/3571246},
  articleno	= 53,
  numpages	= 31,
  keywords	= {Concurrent objects, Library abstraction, Linearizability,
		  Relaxed memory consistency}
}

@InProceedings{	  lal2008:context-bound-reduction,
  title		= {Reducing Concurrent Analysis Under a Context Bound to
		  Sequential Analysis },
  author	= {Lal, Akash and Reps, Thomas},
  booktitle	= {CONF_CAV 2008},
  year		= {2008},
  editor	= {Gupta, Aarti and Malik, Sharad},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {37--51}
}

@Article{	  lamport1979:sc,
  title		= {How to Make a Multiprocessor Computer that Correctly
		  Executes Multiprocess Programs},
  author	= {Leslie Lamport},
  journal	= {JNL_TCOMPUT},
  year		= {1979},
  month		= sep,
  volume	= {28},
  number	= {9},
  pages		= {690--691},
  publisher	= {IEEE Computer Society},
  address	= {Washington, DC, USA},
  url		= {http://dx.doi.org/10.1109/TC.1979.1675439},
  doi		= {10.1109/TC.1979.1675439},
  ids		= {lamport-sc, sc, SC}
}

@Article{	  lamport1987:fastmutex,
  title		= {A Fast Mutual Exclusion Algorithm},
  author	= {Lamport, Leslie},
  journal	= {JNL_ACMTCS},
  year		= {1987},
  month		= jan,
  issue_date	= {Feb. 1987},
  volume	= {5},
  number	= {1},
  pages		= {1--11},
  numpages	= {11},
  doi		= {10.1145/7351.7352},
  acmid		= {7352},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {fastmutex}
}

@InProceedings{	  lang2020:parallel-smc,
  title		= {Parallel Graph-Based Stateless Model Checking},
  author	= {Lång, Magnus and Sagonas, Konstantinos},
  booktitle	= {CONF_ATVA 2020},
  year		= {2020},
  editor	= {Hung, Dang Van and Sokolsky, Oleg},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {377--393},
  doi		= {10.1007/978-3-030-59152-6_21},
  ids		= {nidhugg-parallel,parallel-nidhugg}
}

@InProceedings{	  lattner2004:llvm,
  title		= {LLVM: A Compilation Framework for Lifelong Program
		  Analysis \& Transformation},
  author	= {Lattner, Chris and Adve, Vikram},
  booktitle	= {CONF_CGO 2004},
  year		= {2004},
  publisher	= {IEEE Computer Society},
  address	= {USA},
  pages		= {75},
  location	= {Palo Alto, California},
  doi		= {10.5555/977395.977673},
  ids		= {llvm}
}

@InProceedings{	  lauterburg2009:java-actor,
  title		= {A Framework for State-Space Exploration of Java-Based
		  Actor Programs},
  author	= {Lauterburg, Steven and Dotta, Mirco and Marinov, Darko and
		  Agha, Gul},
  booktitle	= {CONF_ASE 2009},
  year		= {2009},
  pages		= {468-479},
  doi		= {10.1109/ASE.2009.88}
}

@InProceedings{	  laviron2010:shape-graphs,
  title		= {Separating Shape Graphs},
  author	= {Vincent Laviron and Bor-Yuh Evan Chang and Xavier Rival},
  booktitle	= {CONF_ESOP 2010},
  year		= {2010},
  pages		= {387-406},
  editor	= {Andrew D. Gordon},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {6012}
}

@InProceedings{	  le2013:work-stealing,
  title		= {Correct and Efficient Work-stealing for Weak Memory
		  Models},
  author	= {Lê, Nhat Minh and Pop, Antoniu and Cohen, Albert and
		  Zappa Nardelli, Francesco},
  booktitle	= {CONF_PPOPP 2013},
  year		= {2013},
  location	= {Shenzhen, China},
  pages		= {69--80},
  numpages	= {12},
  doi		= {10.1145/2442516.2442524},
  acmid		= {2442524},
  publisher	= {ACM}
}

@Article{	  lea2005:util-concurrent,
  title		= {The {java.util.concurrent} synchronizer framework},
  author	= {Doug Lea},
  journal	= {JNL_SCP},
  year		= {2005},
  volume	= {58},
  number	= {3},
  pages		= {293-309},
  note		= {Special Issue on Concurrency and synchonization in Java
		  programs},
  doi		= {10.1016/j.scico.2005.03.007},
  ids		= {java-util-concurrent}
}

@InProceedings{	  lee2007:ml-metatheory,
  title		= {Towards a mechanized metatheory of {Standard ML}},
  author	= {Daniel K. Lee and Karl Crary and Robert Harper},
  booktitle	= {CONF_POPL 2007},
  year		= {2007}
}

@InProceedings{	  lee2020:promising2,
  title		= {Promising 2.0: Global optimizations in relaxed memory
		  concurrency},
  author	= {Sung-Hwan Lee and Minki Cho and Anton Podkopaev and Soham
		  Chakraborty and Chung-Kil Hur and Ori Lahav and Viktor
		  Vafeiadis},
  booktitle	= {CONF_PLDI 2020},
  year		= {2020},
  editor	= {Alastair F. Donaldson and Emina Torlak},
  pages		= {362--376},
  publisher	= {{ACM}},
  doi		= {10.1145/3385412.3386010}
}

@Article{	  lengauer1979:dominators,
  title		= {A Fast Algorithm for Finding Dominators in a Flowgraph},
  author	= {Lengauer, Thomas and Tarjan, Robert Endre},
  journal	= {JNL_TOPLAS},
  year		= {1979},
  month		= jan,
  issue_date	= {July 1979},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {1},
  number	= {1},
  doi		= {10.1145/357062.357071},
  pages		= {121–141},
  numpages	= {21}
}

@Article{	  leroy2009:compcert,
  title		= {Formal verification of a realistic compiler},
  author	= {Xavier Leroy},
  journal	= {JNL_CACM},
  year		= {2009},
  volume	= {52},
  number	= {7},
  pages		= {107-115},
  ids		= {compcert}
}

@InProceedings{	  ley-wild2013:auxiliary,
  title		= {Subjective Auxiliary State for Coarse-Grained
		  Concurrency},
  author	= {Ley-Wild, Ruy and Nanevski, Aleksandar},
  booktitle	= {CONF_POPL 2013},
  year		= {2013}
}

@InProceedings{	  liang2012:rg-simulation,
  title		= {A rely-guarantee-based simulation for verifying concurrent
		  program transformations},
  author	= {Hongjin Liang and Xinyu Feng and Ming Fu},
  booktitle	= {CONF_POPL 2012},
  year		= {2012},
  pages		= {455-468},
  editor	= {John Field and Michael Hicks},
  doi		= {10.1145/2103656.2103711},
  publisher	= {ACM}
}

@InProceedings{	  liang2013:modular-linearizability,
  title		= {Modular verification of linearizability with non-fixed
		  linearization points},
  author	= {Hongjin Liang and Xinyu Feng},
  booktitle	= {CONF_PLDI 2013},
  year		= {2013},
  pages		= {459-470},
  editor	= {Hans-Juergen Boehm and Cormac Flanagan},
  doi		= {10.1145/2491956.2462189},
  publisher	= {ACM}
}

@InProceedings{	  liang2018:rcu,
  title		= {Verification of tree-based hierarchical read-copy update
		  in the Linux kernel},
  author	= {L. Liang and P. E. McKenney and D. Kroening and T.
		  Melham},
  booktitle	= {CONF_DATE 2018},
  year		= {2018},
  month		= {March},
  volume	= {},
  number	= {},
  pages		= {61-66},
  doi		= {10.23919/DATE.2018.8341980}
}

@TechReport{	  lipton1988:pram,
  title		= {{PRAM}: {A} scalable shared memory},
  author	= {Richard J Lipton and J S Sandberg},
  year		= {1988},
  institution	= {Technical Report CS-TR-180-88, Princeton University}
}

@InProceedings{	  liu2009:refinement,
  title		= {Model Checking Linearizability via Refinement},
  author	= {Liu, Yang and Chen, Wei and Liu, Yanhong A. and Sun, Jun},
  booktitle	= {CONF_FM 2009},
  year		= {2009},
  pages		= {321-337},
  editor	= {Ana Cavalcanti and Dennis Dams},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {5850}
}

@InProceedings{	  liu2019:pmtest,
  title		= {{PMTest}: {A} Fast and Flexible Testing Framework for
		  Persistent Memory Programs},
  author	= {Sihang Liu and Yizhou Wei and Jishen Zhao and Aasheesh
		  Kolli and Samira Manabi Khan},
  booktitle	= {CONF_ASPLOS 2019},
  year		= {2019},
  editor	= {Iris Bahar and Maurice Herlihy and Emmett Witchel and
		  Alvin R. Lebeck},
  pages		= {411--425},
  publisher	= {{ACM}},
  doi		= {10.1145/3297858.3304015}
}

@Article{	  lowe2017:lin-testing,
  title		= {Testing for linearizability},
  author	= {Lowe, Gavin},
  journal	= {JNL_CCPE},
  year		= 2017,
  doi		= {10.1002/CPE.3928},
  volume	= 29,
  number	= 4
}

@Misc{		  lynch2017:losing-460m,
  title		= {The Worst Computer Bugs in History: Losing \$460m in 45
		  minutes},
  author	= {Lynch, Jamie},
  year		= {2017},
  month		= sep,
  url		= {https://www.bugsnag.com/blog/bug-day-460m-loss},
  day		= {14},
  urldate	= {2020-11-26}
}

@InProceedings{	  mador-haim2010:litmus,
  title		= {Generating Litmus Tests for Contrasting Memory Consistency
		  Models},
  author	= {Sela Mador{-}Haim and Rajeev Alur and Milo M. K. Martin},
  booktitle	= {CONF_CAV 2010},
  year		= {2010},
  editor	= {Tayssir Touili and Byron Cook and Paul B. Jackson},
  series	= {LNCS},
  volume	= {6174},
  pages		= {273--287},
  publisher	= {Springer},
  doi		= {10.1007/978-3-642-14295-6_26},
  opttimestamp	= {Tue, 14 May 2019 10:00:43 +0200},
  optbiburl	= {https://dblp.org/rec/conf/cav/Mador-HaimAM10.bib},
  optbibsource	= {dblp computer science bibliography, https://dblp.org}
}

@InProceedings{	  mador-haim2011:litmus,
  title		= {Litmus tests for comparing memory consistency models: how
		  long do they need to be?},
  author	= {Sela Mador{-}Haim and Rajeev Alur and Milo M. K. Martin},
  booktitle	= {CONF_DAC 2011},
  year		= {2011},
  editor	= {Leon Stok and Nikil D. Dutt and Soha Hassoun},
  optpages	= {504--509},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2024724.2024842},
  doi		= {10.1145/2024724.2024842},
  opttimestamp	= {Tue, 06 Nov 2018 16:58:18 +0100},
  optbiburl	= {https://dblp.org/rec/conf/dac/Mador-HaimAM11.bib},
  optbibsource	= {dblp computer science bibliography, https://dblp.org}
}

@InProceedings{	  maiya2016:por-event-driven,
  title		= {Partial Order Reduction for Event-Driven Multi-threaded
		  Programs},
  author	= {Maiya, Pallavi and Gupta, Rahul and Kanade, Aditya and
		  Majumdar, Rupak},
  booktitle	= {CONF_TACAS 2016},
  year		= {2016},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {680--697},
  doi		= {10.1007/978-3-662-49674-9_44}
}

@Article{	  manerkar2016:counterexamples,
  title		= {Counterexamples and Proof Loophole for the {C/C++} to
		  {POWER} and {ARMv7} Trailing-Sync Compiler Mappings},
  author	= {Manerkar, Yatin A and Trippel, Caroline and Lustig, Daniel
		  and Pellauer, Michael and Martonosi, Margaret},
  journal	= {JNL_CORR},
  year		= {2016}
}

@InProceedings{	  mansky2014:optimizations,
  title		= {Verifying Optimizations for Concurrent Programs},
  author	= {William Mansky and Elsa L. Gunter},
  booktitle	= {CONF_WPTE 2014},
  year		= {2014},
  editor	= {Manfred Schmidt-Schauß and Masahiko Sakai and David Sabel
		  and Yuki Chiba},
  series	= {{OASICS}},
  volume	= {40},
  pages		= {15--26},
  publisher	= {DAGSTUHL},
  doi		= {10.4230/OASIcs.WPTE.2014.15}
}

@InProceedings{	  manson2005:jmm,
  title		= {The {Java} memory model},
  author	= {Jeremy Manson and William Pugh and Sarita V. Adve},
  booktitle	= {CONF_POPL 2005},
  year		= {2005},
  pages		= {378-391},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/1040305.1040336},
  doi		= {10.1145/1040305.1040336}
}

@Book{		  manual:cpp20standard,
  key		= {programming language {C++}},
  title		= {Programming Language {C++}},
  author	= {{ISO/IEC 14882:2020}},
  year		= {2020}
}

@Book{		  manual:cppstandard,
  key		= {programming language {C++}},
  title		= {Programming Language {C++}},
  author	= {{ISO/IEC 14882:2011}},
  year		= {2011}
}

@Book{		  manual:cstandard,
  key		= {programming language {C}},
  title		= {Programming Language {C}},
  author	= {{ISO/IEC 9899:2011}},
  year		= {2011}
}

@Book{		  manual:risc-v,
  title		= {The RISC-V Instruction Set Manual Volume I: {User}-Level
		  {ISA}},
  author	= {Waterman, Andrew and Asanović, Krste},
  year		= {2017},
  url		= {https://content.riscv.org/wp-content/uploads/2017/05/riscv-spec-v2.2.pdf},
  ids		= {risc-v}
}

@Book{		  manual:risc-v-2019,
  title		= {The RISC-V Instruction Set Manual Volume I: {User}-level
		  {ISA}},
  author	= {Waterman, Andrew and Asanović, Krste},
  year		= {2019},
  url		= {https://content.riscv.org/wp-content/uploads/2019/06/riscv-spec.pdf},
  ids		= {risc-v-2019}
}

@Book{		  manual:sparc-tso,
  title		= {The {SPARC} architecture manual (version 9)},
  author	= {{SPARC International Inc.}},
  year		= {1994},
  publisher	= {Prentice-Hall},
  ids		= {sparc-tso}
}

@Book{		  manual:sparc-v8,
  title		= {The {SPARC} Architecture Manual: {Version} 8},
  author	= {{SPARC International Inc.}},
  year		= {1992},
  publisher	= {Prentice-Hall, Inc.}
}

@Misc{		  maranget2012:arm-tutorial,
  title		= {A Tutorial Introduction to the {ARM} and {POWER} Relaxed
		  Memory Models},
  author	= {Maranget, Luc and Sarkar, Susmit and Sewell,Peter},
  year		= 2012,
  url		= {http://www.cl.cam.ac.uk/~pes20/ppc-supplemental/test7.pdf}
}

@InProceedings{	  marino2011:sc-mm,
  title		= {A case for an SC-preserving compiler},
  author	= {Daniel Marino and Abhayendra Singh and Todd D. Millstein
		  and Madanlal Musuvathi and Satish Narayanasamy},
  booktitle	= {CONF_PLDI 2011},
  year		= {2011},
  editor	= {Mary W. Hall and David A. Padua},
  pages		= {199--210},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/1993498.1993522},
  doi		= {10.1145/1993498.1993522}
}

@InProceedings{	  marmanis2023:buster,
  title		= {Reconciling Preemption Bounding with DPOR},
  author	= {Marmanis, Iason and Kokologiannakis, Michalis and
		  Vafeiadis, Viktor},
  booktitle	= {CONF_TACAS 2023},
  year		= {2023},
  editor	= {Sankaranarayanan, Sriram and Sharygina, Natasha},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {85--104},
  ids		= {genmc-bounding}
}

@Article{	  marmanis2023:buster-artifact,
  title		= {Reconciling Preemption Bounding with DPOR (artifact)},
  author	= {Marmanis, Iason and Kokologiannakis, Michalis and
		  Vafeiadis, Viktor},
  year		= {2023},
  month		= apr,
  ids		= {buster-artifact},
  doi		= {10.5281/zenodo.7505917}
}

@Article{	  marmanis2023:buster-supp-material,
  title		= {Reconciling Preemption Bounding with DPOR (supplementary
		  material)},
  author	= {Marmanis, Iason and Kokologiannakis, Michalis and
		  Vafeiadis, Viktor},
  year		= {2023},
  month		= apr,
  url		= {https://plv.mpi-sws.org/genmc},
  ids		= {buster-supp-material}
}

@InProceedings{	  marmanis2023:robin-bound,
  title		= {Optimal Bounded Partial Order Reduction},
  author	= {Iason Marmanis and Viktor Vafeiadis},
  booktitle	= {CONF_FMCAD 2023},
  year		= {2023},
  editor	= {Alexander Nadel and Kristin Yvonne Rozier},
  pages		= {86--91},
  publisher	= {{IEEE}},
  doi		= {10.34727/2023/ISBN.978-3-85448-060-0_16}
}

@Article{	  marmanis2025:mixer,
  title		= {Model Checking C/C++ with Mixed-Size Accesses},
  author	= {Marmanis, Iason and Kokologiannakis, Michalis and
		  Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2025},
  month		= jan,
  issue_date	= {January 2025},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {9},
  number	= {POPL},
  url		= {https://doi.org/10.1145/3704911},
  doi		= {10.1145/3704911},
  articleno	= {75},
  numpages	= {21},
  ids		= {mixer}
}

@TechReport{	  marrero1997:mc-sp,
  title		= {Model Checking for Security Protocols},
  author	= {Will Marrero and Edmund M. Clarke and Somesh Jha},
  year		= {1997},
  url		= {http://www.cs.cmu.edu/~emc/papers/Conference%20Papers/Model%20Checking%20for%20Security%20Protocols.pdf}
}

@Article{	  massalin1992:lock-free,
  title		= {A Lock-Free Multiprocessor OS Kernel (Abstract)},
  author	= {Henry Massalin and Calton Pu},
  journal	= {JNL_SOSR},
  year		= {1992},
  volume	= {26},
  number	= {2},
  pages		= {8}
}

@Misc{		  mathur2007:ext4,
  title		= {The new ext4 filesystem: current status and future plans},
  author	= {Mathur, Avantika and Cao, Mingming and Bhattacharya,
		  Suparna and Dilger, Andreas and Vivier, Laurent},
  booktitle	= {CONF_OLS 2007},
  url		= {https://www.kernel.org/doc/ols/2007/ols2007v2-pages-21-34.pdf},
  shorttitle	= {The new ext 4 filesystem},
  pages		= {21--34},
  urldate	= {2020-06-17},
  date		= {2007},
  langid	= {english}
}

@InProceedings{	  mazurkiewicz1987:traces,
  title		= {Trace Theory},
  author	= {Antoni Mazurkiewicz},
  booktitle	= {CONF_PNAROMC 1987},
  year		= {1987},
  series	= {LNCS},
  volume	= {255},
  pages		= {279--324},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  url		= {http://dx.doi.org/10.1007/3-540-17906-2_30},
  doi		= {10.1007/3-540-17906-2_30},
  ids		= {mazurkiewicz}
}

@Misc{		  mckenney2011:rationale,
  title		= {{N1525}: {Memory}-Order Rationale},
  author	= {Paul E. McKenney and Blaine Garst},
  year		= {2011},
  url		= {http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1525.htm}
}

@Article{	  mcmillan1995:unfolding,
  title		= {A Technique of a State Space Search Based on Unfolding},
  author	= {Kenneth L. {McMillan}},
  journal	= {JNL_FMSD},
  year		= {1995},
  month		= jan,
  volume	= {6},
  number	= {1},
  pages		= {45--65},
  publisher	= {Kluwer Academic Publishers},
  url		= {http://dx.doi.org/10.1007/BF01384314},
  doi		= {10.1007/BF01384314},
  ids		= {unfoldings}
}

@InProceedings{	  mcmillan2003:interpolation,
  title		= {Interpolation and SAT-Based Model Checking},
  author	= {McMillan, K. L.},
  booktitle	= {CONF_CAV 2003},
  year		= {2003},
  editor	= {Hunt, Warren A. and Somenzi, Fabio},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {1--13},
  doi		= {10.1007/978-3-540-45069-6_1},
  url		= {https://doi.org/10.1007/978-3-540-45069-6_1}
}

@InProceedings{	  medor-haim2012:power,
  title		= {An Axiomatic Memory Model for {POWER} Multiprocessors},
  author	= {Sela Mador-Haim and Luc Maranget and Susmit Sarkar and
		  Kayvan Memarian and Jade Alglave and Scott Owens and Rajeev
		  Alur and Milo M. K. Martin and Peter Sewell and Derek
		  Williams},
  booktitle	= {CONF_CAV 2012},
  year		= {2012},
  pages		= {495--512},
  editor	= {P. Madhusudan and Sanjit A. Seshia},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {7358}
}

@Article{	  mellor-crummey1991:mcs-lock,
  title		= {Algorithms for Scalable Synchronization on Shared-memory
		  Multiprocessors},
  author	= {Mellor-Crummey, John M. and Scott, Michael L.},
  journal	= {JNL_ACMTCS},
  year		= {1991},
  month		= feb,
  issue_date	= {Feb. 1991},
  volume	= {9},
  number	= {1},
  pages		= {21--65},
  numpages	= {45},
  url		= {http://doi.acm.org/10.1145/103727.103729},
  doi		= {10.1145/103727.103729},
  acmid		= {103729},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {mcs-lock}
}

@Article{	  michael1998:nonblocking,
  title		= {Nonblocking algorithms and preemption-safe locking on
		  multiprogrammed shared memory multiprocessors},
  author	= {Michael, Maged M. and Scott, Michael L.},
  journal	= {JNL_JPDC},
  year		= {1998},
  volume	= {51},
  number	= {1},
  pages		= {1--26},
  ids		= {ms-queue, msqueue}
}

@InProceedings{	  michael2002:lockfree-list,
  title		= {High performance dynamic lock-free hash tables and
		  list-based sets},
  author	= {Michael, Maged M.},
  booktitle	= {CONF_SPAA 2002},
  year		= {2002},
  isbn		= {1581135297},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/564870.564881},
  doi		= {10.1145/564870.564881},
  pages		= {73–82},
  numpages	= {10},
  location	= {Winnipeg, Manitoba, Canada},
  optseries	= {SPAA '02}
}

@Article{	  michael2004:hazard-pointers,
  title		= {Hazard Pointers: Safe Memory Reclamation for Lock-Free
		  Objects},
  author	= {Michael, Maged M.},
  journal	= {JNL_TPDS},
  year		= {2004},
  month		= {06},
  issue_date	= {June 2004},
  publisher	= {IEEE Press},
  volume	= {15},
  number	= {6},
  url		= {https://doi.org/10.1109/TPDS.2004.8},
  doi		= {10.1109/TPDS.2004.8},
  pages		= {491–504},
  numpages	= {14},
  ids		= {hazard-pointers, hps}
}

@InProceedings{	  mitchell1986:data-abstraction,
  title		= {Representation independence and data abstraction},
  author	= {John C. Mitchell},
  booktitle	= {CONF_POPL 1986},
  year		= 1986
}

@InProceedings{	  mitchell1997:mc-protocols,
  title		= {Automated analysis of cryptographic protocols using
		  {Mur}/spl phi/},
  author	= {Mitchell, J.C. and Mitchell, M. and Stern, U.},
  booktitle	= {CONF_SP 1997},
  year		= {1997},
  volume	= {},
  number	= {},
  pages		= {141-151},
  doi		= {10.1109/SECPRI.1997.601329},
  url		= {https://doi.org/10.1109/SECPRI.1997.601329}
}

@InProceedings{	  mohan2018:crashmonkey,
  title		= {Finding Crash-Consistency Bugs with Bounded Black-Box
		  Crash Testing},
  author	= {Mohan, Jayashree and Martinez, Ashlie and Ponnapalli,
		  Soujanya and Raju, Pandian and Chidambaram, Vijay},
  booktitle	= {CONF_OSDI 2018},
  year		= {2018},
  publisher	= {USENIX Association},
  address	= {USA},
  pages		= {33–50},
  numpages	= {18},
  location	= {Carlsbad, CA, USA},
  url		= {https://www.usenix.org/system/files/osdi18-mohan.pdf},
  urldate	= {2020-11-16}
}

@InProceedings{	  moir2005:elimination,
  title		= {Using elimination to implement scalable and lock-free
		  {FIFO} queues},
  author	= {Moir, Mark and Nussbaum, Daniel and Shalev, Ori and
		  Shavit, Nir},
  booktitle	= {CONF_SPAA 2005},
  year		= {2005},
  pages		= {253-262},
  numpages	= {10},
  publisher	= {ACM}
}

@InProceedings{	  moiseenko2020:reconciling-es,
  title		= {Reconciling Event Structures with Modern Multiprocessors},
  author	= {Evgenii Moiseenko and Anton Podkopaev and Ori Lahav and
		  Orestis Melkonian and Viktor Vafeiadis},
  booktitle	= {CONF_ECOOP 2020},
  year		= {2020},
  pages		= {5:1--5:26},
  series	= {LIPIcs},
  volume	= {166},
  editor	= {Robert Hirschfeld and Tobias Pape},
  publisher	= {DAGSTUHL},
  address	= {Dagstuhl, Germany},
  url		= {https://drops.dagstuhl.de/opus/volltexte/2020/13162},
  urn		= {urn:nbn:de:0030-drops-131622},
  doi		= {10.4230/LIPIcs.ECOOP.2020.5},
  annote	= {Keywords: Weak Memory Consistency, Event Structures, IMM,
		  Weakestmo}
}

@Article{	  moiseenko2022:wmc,
  title		= {Model Checking on a Multi-execution Memory Model},
  author	= {Moiseenko, Evgenii and Kokologiannakis, Michalis and
		  Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2022},
  month		= oct,
  issue_date	= {October 2022},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {6},
  number	= {OOPSLA2},
  url		= {https://doi.org/10.1145/3563315},
  doi		= {10.1145/3563315},
  articleno	= {152},
  numpages	= {28},
  keywords	= {Weak Memory Models; Model Checking},
  ids		= {wmc}
}

@InProceedings{	  morisset2013:sound-optimizations,
  title		= {Compiler Testing via a Theory of Sound Optimisations in
		  the {C11/C++11} Memory Model},
  author	= {Morisset, Robin and Pawan, Pankaj and Zappa Nardelli,
		  Francesco},
  booktitle	= {CONF_PLDI 2013},
  year		= {2013},
  location	= {Seattle, Washington, USA},
  pages		= {187--196},
  numpages	= {10},
  url		= {http://doi.acm.org/10.1145/2491956.2491967},
  doi		= {10.1145/2491956.2491967},
  acmid		= {2491967},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {c11/c++11 memory model, compiler testing}
}

@Article{	  mukherjee2020:cct,
  title		= {Learning-based controlled concurrency testing},
  author	= {Mukherjee, Suvam and Deligiannis, Pantazis and Biswas,
		  Arpita and Lal, Akash},
  journal	= {JNL_PACMPL},
  year		= {2020},
  month		= nov,
  issue_date	= {November 2020},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {4},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3428298},
  doi		= {10.1145/3428298},
  articleno	= {230},
  numpages	= {31},
  keywords	= {Concurrency, Reinforcement Learning, Systematic Testing}
}

@InProceedings{	  musuvathi2007:iterative-bounding,
  title		= {Iterative Context Bounding for Systematic Testing of
		  Multithreaded Programs},
  author	= {Musuvathi, Madanlal and Qadeer, Shaz},
  booktitle	= {CONF_PLDI 2007},
  year		= {2007},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/1250734.1250785},
  doi		= {10.1145/1250734.1250785},
  pages		= {446–455},
  numpages	= {10},
  keywords	= {concurrency, model checking, partial-order reduction,
		  software testing, multithreading, context-bounding,
		  shared-memory programs},
  location	= {San Diego, California, USA}
}

@TechReport{	  musuvathi2007:por-cb,
  title		= {Partial-Order Reduction for Context-Bounded State
		  Exploration},
  author	= {Madalan Musuvathi and Shaz Qadeer},
  year		= {2007},
  institution	= {Microsoft Research},
  number	= {MSR-TR-2007-12},
  url		= {https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/tr-2007-12.pdf}
}

@InProceedings{	  musuvathi2008:fair-smc,
  title		= {Fair Stateless Model Checking},
  author	= {Musuvathi, Madanlal and Qadeer, Shaz},
  booktitle	= {CONF_PLDI 2008},
  year		= {2008},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/1375581.1375625},
  doi		= {10.1145/1375581.1375625},
  pages		= {362–371},
  numpages	= {10},
  keywords	= {concurrency, liveness, shared-memory programs, software
		  testing, multi-threading, fairness, model checking},
  location	= {Tucson, AZ, USA}
}

@InProceedings{	  musuvathi2008:heisenbugs,
  title		= {Finding and reproducing Heisenbugs in concurrent
		  programs},
  author	= {Madanlal Musuvathi and Shaz Qadeer and Thomas Ball and
		  Gérard Basler and Piramanayagam Arumuga Nainar and Iulian
		  Neamtiu},
  booktitle	= {CONF_OSDI 2008},
  year		= {2008},
  pages		= {267--280},
  publisher	= {{USENIX} Association},
  url		= {https://www.usenix.org/legacy/events/osdi08/tech/full_papers/musuvathi/musuvathi.pdf},
  urldate	= {2020-11-16},
  ids		= {heisenbugs,chess}
}

@InProceedings{	  nanevski2010:heap-programs,
  title		= {Structuring the verification of heap-manipulating
		  programs},
  author	= {Aleksandar Nanevski and Viktor Vafeiadis and Josh
		  Berdine},
  booktitle	= {CONF_POPL 2010},
  year		= {2010},
  pages		= {261-274},
  editor	= {Manuel V. Hermenegildo and Jens Palsberg}
}

@InProceedings{	  necula2000:translation,
  title		= {Translation validation for an optimizing compiler},
  author	= {George C. Necula},
  booktitle	= {CONF_PLDI 2000},
  year		= {2000},
  pages		= {83-94},
  editor	= {Monica S. Lam},
  publisher	= {ACM}
}

@InProceedings{	  nguyen2018:quasi,
  title		= {Quasi-optimal partial order reduction},
  author	= {Huyen T. T. Nguyen and César Rodríguez and Marcelo Sousa
		  and Camille Coti and Laure Petrucci},
  booktitle	= {CONF_CAV 2018},
  year		= {2018},
  editor	= {Hana Chockler and Georg Weissenbacher},
  series	= {LNCS},
  volume	= {10982},
  pages		= {354--371},
  publisher	= {Springer},
  doi		= {10.1007/978-3-319-96142-2_22},
  ids		= {quasi-optimal,quasi-dpor}
}

@Article{	  niej2024:ranking,
  title		= {Mostly Automated Verification of Liveness Properties for
		  Distributed Protocols with Ranking Functions},
  author	= {Yao, Jianan and Tao, Runzhou and Gu, Ronghui and Nieh,
		  Jason},
  journal	= {JNL_PACMPL},
  year		= {2024},
  month		= {jan},
  issue_date	= {January 2024},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {8},
  number	= {POPL},
  url		= {https://doi.org/10.1145/3632877},
  doi		= {10.1145/3632877},
  articleno	= {35},
  numpages	= {32},
  keywords	= {distributed protocols, liveness reasoning, ranking
		  function synthesis}
}

@InProceedings{	  norris2013:cdschecker,
  title		= {{CDSChecker}: {Checking} concurrent data structures
		  written with {C/C++} atomics},
  author	= {Brian Norris and Brian Demsky},
  booktitle	= {CONF_OOPSLA 2013},
  year		= {2013},
  pages		= {131--150},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2509136.2509514},
  doi		= {10.1145/2509136.2509514},
  ids		= {CDSChecker, CDSchecker, cdschecker}
}

@Article{	  norris2016:cdschecker-journal,
  title		= {A Practical Approach for Model Checking {C/C++11} Code},
  author	= {Norris, Brian and Demsky, Brian},
  journal	= {JNL_TOPLAS},
  year		= {2016},
  month		= may,
  issue_date	= {May 2016},
  volume	= {38},
  number	= {3},
  pages		= {10:1--10:51},
  articleno	= {10},
  numpages	= {51},
  url		= {http://doi.acm.org/10.1145/2806886},
  doi		= {10.1145/2806886},
  acmid		= {2806886},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {Relaxed memory model, model checking},
  ids		= {cdschecker-journal}
}

@InProceedings{	  ntzik2015:pathnames,
  title		= {Reasoning about the {POSIX} file system: local update and
		  global pathnames},
  author	= {Ntzik, Gian and Gardner, Philippa},
  booktitle	= {CONF_OOPSLA 2015},
  location	= {Pittsburgh, {PA}, {USA}},
  url		= {https://doi.org/10.1145/2814270.2814306},
  doi		= {10.1145/2814270.2814306},
  shorttitle	= {Reasoning about the {POSIX} file system},
  pages		= {201--220},
  publisher	= {ACM},
  urldate	= {2020-06-15},
  date		= {2015-10-23},
  keywords	= {file systems, global pathnames, local reasoning, {POSIX},
		  separation logic}
}

@InProceedings{	  oberhauser2021:vsync,
  title		= {{VSync}: {P}ush-Button Verification and Optimization for
		  Synchronization Primitives on Weak Memory Models},
  author	= {Oberhauser, Jonas and Chehab, Rafael Lourenco de Lima and
		  Behrens, Diogo and Fu, Ming and Paolillo, Antonio and
		  Oberhauser, Lilith and Bhat, Koustubha and Wen, Yuzhong and
		  Chen, Haibo and Kim, Jaeho and Vafeiadis, Viktor},
  booktitle	= {CONF_ASPLOS 2021},
  year		= {2021},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {https://doi.org/10.1145/3445814.3446748},
  doi		= {10.1145/3445814.3446748},
  pages		= {530–545},
  numpages	= {16},
  keywords	= {weak memory models, model checking},
  location	= {Virtual, USA},
  ids		= {vsync}
}

@InProceedings{	  oberhauser2021:vsync-hmcs,
  title		= {Verifying and Optimizing the HMCS Lock for Arm Servers},
  author	= {Oberhauser, Jonas and Oberhauser, Lilith and Paolillo,
		  Antonio and Behrens, Diogo and Fu, Ming and Vafeiadis,
		  Viktor},
  booktitle	= {CONF_NETYS 2021},
  year		= {2021},
  editor	= {Echihabi, Karima and Meyer, Roland},
  publisher	= {Springer},
  address	= {Cham},
  pages		= {240--260},
  url		= {https://doi.org/10.1007/978-3-030-91014-3_17},
  doi		= {10.1007/978-3-030-91014-3_17}
}

@InProceedings{	  odersky2005:scalable,
  title		= {Scalable Component Abstractions},
  author	= {Martin Odersky and Matthias Zenger},
  booktitle	= {CONF_OOPSLA 2005},
  year		= 2005
}

@Article{	  ohearn2007:csl,
  title		= {Resources, concurrency, and local reasoning},
  author	= {Peter W. O'Hearn},
  journal	= {JNL_TCS},
  year		= {2007},
  volume	= {375},
  number	= {1-3},
  pages		= {271-307},
  ids		= {csl}
}

@InProceedings{	  ohearn2010:hindsight,
  title		= {Verifying linearizability with hindsight},
  author	= {Peter W. O'Hearn and Noam Rinetzky and Martin T. Vechev
		  and Eran Yahav and Greta Yorsh},
  booktitle	= {CONF_PODC 2010},
  year		= {2010},
  pages		= {85-94},
  editor	= {Andréa W. Richa and Rachid Guerraoui},
  publisher	= {ACM}
}

@InProceedings{	  ou2017:cdsspec,
  title		= {Checking Concurrent Data Structures Under the {C/C++11}
		  Memory Model},
  author	= {Peizhao Ou and Brian Demsky},
  booktitle	= {CONF_PPOPP 2017},
  year		= {2017},
  opteditor	= {Vivek Sarkar and Lawrence Rauchwerger},
  pages		= {45-59},
  publisher	= {ACM},
  doi		= {10.1145/3018743.3018749}
}

@Article{	  ou2018:oota,
  title		= {Towards Understanding the Costs of Avoiding
		  Out-of-Thin-Air Results},
  author	= {Ou, Peizhao and Demsky, Brian},
  journal	= {JNL_PACMPL},
  year		= {2018},
  month		= oct,
  issue_date	= {November 2018},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {2},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3276506},
  doi		= {10.1145/3276506},
  articleno	= {136},
  numpages	= {29},
  keywords	= {compilers, memory models, concurrency}
}

@InProceedings{	  oukid2016:testing,
  title		= {On Testing Persistent-Memory-Based Software},
  author	= {Oukid, Ismail and Booss, Daniel and Lespinasse, Adrien and
		  Lehner, Wolfgang},
  year		= {2016},
  publisher	= {ACM},
  doi		= {10.1145/2933349.2933354},
  articleno	= {5},
  numpages	= {7},
  series	= {DaMoN '16}
}

@InProceedings{	  owens2006:units,
  title		= {From structures and functors to modules and units},
  author	= {Scott Owens and Matthew Flatt},
  booktitle	= {CONF_ICFP 2006},
  year		= 2006
}

@InProceedings{	  owens2009:x86-tso,
  title		= {A better {x86} memory model: {x86-TSO}},
  author	= {Owens, Scott and Sarkar, Susmit and Sewell, Peter},
  booktitle	= {CONF_TPHOLS 2009},
  year		= {2009},
  location	= {Munich, Germany},
  pages		= {391--407},
  numpages	= {17},
  url		= {http://dx.doi.org/10.1007/978-3-642-03359-9_27},
  doi		= {10.1007/978-3-642-03359-9_27},
  acmid		= {1616107},
  publisher	= {Springer},
  optaddress	= {Berlin, Heidelberg},
  ids		= {x86-tso-owens}
}

@InProceedings{	  owens2010:abstractions,
  title		= {Reasoning about the implementation of concurrency
		  abstractions on {x86-TSO}},
  author	= {Scott Owens},
  booktitle	= {CONF_ECOOP 2010},
  year		= {2010},
  editor	= {Theo D'Hondt},
  series	= {LNCS},
  volume	= {6183},
  pages		= {478--503},
  publisher	= {Springer},
  doi		= {10.1007/978-3-642-14107-2_23}
}

@Article{	  owicki1976:proof,
  title		= {An axiomatic proof technique for parallel programs {I}},
  author	= {Owicki, Susan and Gries, David},
  journal	= {JNL_AI},
  year		= {1976},
  volume	= {6},
  number	= {4},
  pages		= {319--340},
  doi		= {10.1007/BF00268134},
  publisher	= {Springer},
  language	= {English},
  ids		= {owicki-gries}
}

@Article{	  owicki1976:properties,
  title		= {Verifying properties of parallel programs: {An} axiomatic
		  approach},
  author	= {Owicki, Susan and Gries, David},
  journal	= {JNL_CACM},
  year		= {1976},
  month		= may,
  issue_date	= {May 1976},
  volume	= {19},
  number	= {5},
  pages		= {279--285},
  numpages	= {7},
  doi		= {10.1145/360051.360224},
  publisher	= {ACM}
}

@PhDThesis{	  owicki:thesis,
  title		= {Axiomatic proof techniques for parallel programs},
  author	= {Owicki, Susan Speer},
  year		= {1975},
  school	= {Cornell University}
}

@Article{	  ozkan2018:pct,
  title		= {Randomized testing of distributed systems with
		  probabilistic guarantees},
  author	= {Burcu Kulahcioglu Ozkan and Rupak Majumdar and Filip
		  Niksic and Mitra Tabaei Befrouei and Georg Weissenbacher},
  journal	= {JNL_PACMPL},
  year		= {2018},
  volume	= {2},
  number	= {{OOPSLA}},
  pages		= {160:1--160:28},
  url		= {https://doi.org/10.1145/3276530},
  doi		= {10.1145/3276530}
}

@Article{	  ozkan2019:tapct,
  title		= {Trace aware random testing for distributed systems},
  author	= {Ozkan, Burcu Kulahcioglu and Majumdar, Rupak and Oraee,
		  Simin},
  journal	= {JNL_PACMPL},
  year		= {2019},
  month		= oct,
  issue_date	= {October 2019},
  publisher	= {Association for Computing Machinery},
  address	= {New York, NY, USA},
  volume	= {3},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3360606},
  doi		= {10.1145/3360606},
  articleno	= {180},
  numpages	= {29},
  keywords	= {distributed systems, hitting families, partial order
		  reduction, random testing}
}

@InProceedings{	  padon2016:ivy,
  title		= {Ivy: safety verification by interactive generalization},
  author	= {Oded Padon and Kenneth L. McMillan and Aurojit Panda and
		  Mooly Sagiv and Sharon Shoham},
  booktitle	= {CONF_PLDI 2016},
  year		= {2016},
  opteditor	= {Chandra Krintz and Emery D. Berger},
  pages		= {614--630},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2908080.2908118},
  doi		= {10.1145/2908080.2908118}
}

@Article{	  papadimitriou1979:scdu,
  title		= {The serializability of concurrent database updates},
  author	= {Papadimitriou, Christos H.},
  journal	= {JNL_JACM},
  year		= {1979},
  month		= oct,
  issue_date	= {Oct. 1979},
  volume	= {26},
  number	= {4},
  pages		= {631--653},
  numpages	= {23},
  url		= {http://doi.acm.org/10.1145/322154.322158},
  doi		= {10.1145/322154.322158},
  acmid		= {322158},
  publisher	= {ACM},
  address	= {New York, NY, USA}
}

@Article{	  park2017:ijournaling,
  title		= {{iJournaling}: Fine-grained journaling for improving the
		  latency of fsync system call},
  author	= {Daejun Park and Dongkun Shin},
  booktitle	= {CONF_USENIX 2017},
  year		= {2017},
  pages		= {787--798},
  url		= {https://www.usenix.org/conference/atc17/technical-sessions/presentation/park}
}

@InProceedings{	  pavioti2020:mrd,
  title		= {Modular relaxed dependencies in weak memory concurrency},
  author	= {Marco Paviotti and Simon Cooksey and Anouk Paradis and
		  Daniel Wright and Scott Owens and Mark Batty},
  booktitle	= {CONF_ESOP 2020},
  year		= {2020},
  editor	= {Peter Müller},
  series	= {LNCS},
  volume	= {12075},
  pages		= {599--625},
  publisher	= {Springer},
  doi		= {10.1007/978-3-030-44914-8_22}
}

@InProceedings{	  pelley2014:persistency,
  title		= {Memory persistency},
  author	= {Pelley, Steven and Chen, Peter M. and Wenisch, Thomas F.},
  booktitle	= {CONF_ISCA 2014},
  year		= {2014},
  publisher	= {IEEE Press},
  pages		= {265–276},
  numpages	= {12},
  location	= {Minneapolis, Minnesota, USA},
  url		= {https://doi.org/10.1109/ISCA.2014.6853222},
  doi		= {10.1109/ISCA.2014.6853222},
  ids		= {persistency}
}

@InProceedings{	  pichon2016:semantics,
  title		= {A concurrency semantics for relaxed atomics that permits
		  optimisation and avoids thin-air executions},
  author	= {Pichon-Pharabod, Jean and Sewell, Peter},
  booktitle	= {CONF_POPL 2016},
  year		= {2016},
  location	= {St. Petersburg, FL, USA},
  pages		= {622--633},
  numpages	= {12},
  url		= {http://doi.acm.org/10.1145/2837614.2837616},
  doi		= {10.1145/2837614.2837616},
  acmid		= {2837616},
  publisher	= {ACM},
  keywords	= {C/C++, Concurrency, Relaxed memory models}
}

@Article{	  pillai2014:application-consistency,
  title		= {Towards efficient, portable application-level
		  consistency},
  author	= {Pillai, Thanumalayan Sankaranarayana and Chidambaram,
		  Vijay and Hwang, Joo-Young and Arpaci-Dusseau, Andrea C.
		  and Arpaci-Dusseau, Remzi H.},
  volume	= {48},
  doi		= {10.1145/2626401.2626407},
  pages		= {26--31},
  number	= {1},
  journaltitle	= {JNL_SOSR},
  shortjournal	= {JNL_SOSR},
  urldate	= {2020-06-17},
  date		= {2014-05-15},
  file		= {Full Text
		  PDF:/home/michalis/.zotero/zotero/0cxxanru.default/zotero/storage/NVEWPV7U/Pillai
		  et al. - 2014 - Towards efficient, portable
		  application-level cons.pdf:application/pdf}
}

@InProceedings{	  pillai2014:filesystems,
  title		= {All file systems are not created equal: on the complexity
		  of crafting crash-consistent applications},
  author	= {Thanumalayan Sankaranarayana Pillai and Vijay Chidambaram
		  and Ramnatthan Alagappan and Samer Al-Kiswany and Andrea C.
		  Arpaci-Dusseau and Remzi H. Arpaci-Dusseau},
  booktitle	= {CONF_OSDI 2014},
  year		= {2014},
  month		= oct,
  address	= {Broomfield, CO},
  pages		= {433--448},
  url		= {https://www.usenix.org/conference/osdi14/technical-sessions/presentation/pillai},
  publisher	= {{USENIX} Association}
}

@Article{	  pillai2017:crash-consistency,
  title		= {Application crash consistency and performance with
		  {CCFS}},
  author	= {Pillai, Thanumalayan Sankaranarayana and Alagappan,
		  Ramnatthan and Lu, Lanyue and Chidambaram, Vijay and
		  Arpaci-Dusseau, Andrea C. and Arpaci-Dusseau, Remzi H.},
  volume	= {13},
  doi		= {10.1145/3119897},
  pages		= {1--29},
  number	= {3},
  journaltitle	= {JNL_TS},
  urldate	= {2020-06-17},
  date		= {2017-10-27},
  langid	= {english}
}

@Book{		  pitts1998:local-states,
  title		= {Operational reasoning for functions with local state},
  author	= {Andrew Pitts and Ian Stark},
  year		= {1998},
  optpublisher	= {Cambridge University Press}
}

@InProceedings{	  plotkin1993:logic,
  title		= {A logic for parametric polymorphism},
  author	= {Gordon D. Plotkin and Martín Abadi},
  booktitle	= {CONF_TLCA 1993},
  year		= {1993}
}

@InProceedings{	  pnueli1993:translation,
  title		= {Translation validation},
  author	= {Amir Pnueli and Michael Siegel and Eli Singerman},
  booktitle	= {CONF_TACAS 1998},
  year		= {1998},
  pages		= {151-166},
  editor	= {Bernhard Steffen},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {1384}
}

@Article{	  podkopaev2019:imm,
  title		= {Bridging the gap between programming languages and
		  hardware weak memory models},
  author	= {Podkopaev, Anton and Lahav, Ori and Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2019},
  month		= jan,
  issue_date	= {January 2019},
  volume	= {3},
  number	= {POPL},
  pages		= {69:1--69:31},
  articleno	= {69},
  numpages	= {31},
  doi		= {10.1145/3290382},
  acmid		= {3290382},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {imm}
}

@InProceedings{	  pous2018:allegories,
  title		= {Allegories: decidability and graph homomorphisms},
  author	= {Damien Pous and Valeria Vignudelli},
  booktitle	= {CONF_LICS 2018},
  year		= {2018},
  editor	= {Anuj Dawar and Erich Grädel},
  pages		= {829--838},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/3209108.3209172},
  doi		= {10.1145/3209108.3209172}
}

@Article{	  pous2022:kah,
  title		= {On Tools for Completeness of Kleene Algebra with
		  Hypotheses},
  author	= {Damien Pous and Jurriaan Rot and Jana Wagemaker},
  journal	= {JNL_CORR},
  year		= {2022},
  volume	= {abs/2210.13020},
  url		= {https://doi.org/10.48550/arXiv.2210.13020},
  doi		= {10.48550/ARXIV.2210.13020},
  eprinttype	= {arXiv},
  eprint	= {2210.13020}
}

@Article{	  pous2023:kat-top,
  title		= {Completeness Theorems for Kleene algebra with tests and
		  top},
  author	= {Damien Pous and Jana Wagemaker},
  journal	= {JNL_CORR},
  year		= {2023},
  volume	= {abs/2304.07190},
  url		= {https://doi.org/10.48550/arXiv.2304.07190},
  doi		= {10.48550/ARXIV.2304.07190},
  eprinttype	= {arXiv},
  eprint	= {2304.07190}
}

@Article{	  prabhakaran2005:journaling-fs,
  title		= {Analysis and evolution of journaling file systems},
  author	= {Prabhakaran, Vijayan and Arpaci-Dusseau, Andrea C and
		  Arpaci-Dusseau, Remzi H},
  booktitle	= {CONF_USENIX 2005},
  pages		= {16},
  date		= {2005},
  langid	= {english},
  url		= {https://www.usenix.org/legacy/events/usenix05/tech/general/full_papers/prabhakaran/prabhakaran.pdf}
}

@Article{	  pulte2018:arm8-flat,
  title		= {Simplifying {ARM} concurrency: {Multicopy}-atomic
		  axiomatic and operational models for {ARMv8}},
  author	= {Christopher Pulte and Shaked Flur and Will Deacon and Jon
		  French and Susmit Sarkar and Peter Sewell},
  journal	= {JNL_PACMPL},
  year		= {2018},
  volume	= {2},
  number	= {{POPL}},
  pages		= {19:1--19:29},
  url		= {https://doi.org/10.1145/3158107},
  doi		= {10.1145/3158107},
  ids		= {arm8-flat, flat-arm8}
}

@InProceedings{	  pulte2019:promising-arm,
  title		= {Promising-{ARM/RISC-V}: {A} simpler and faster operational
		  concurrency model},
  author	= {Pulte, Christopher and Pichon-Pharabod, Jean and Kang,
		  Jeehoon and Lee, Sung-Hwan and Hur, Chung-Kil},
  booktitle	= {CONF_PLDI 2019},
  year		= {2019},
  location	= {Phoenix, AZ, USA},
  pages		= {1--15},
  numpages	= {15},
  url		= {http://doi.acm.org/10.1145/3314221.3314624},
  doi		= {10.1145/3314221.3314624},
  acmid		= {3314624},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {promising-arm, arm-promising}
}

@InProceedings{	  qadeer2005:context-bounded,
  title		= {Context-Bounded Model Checking of Concurrent Software},
  author	= {Shaz Qadeer and Jakob Rehof},
  booktitle	= {CONF_TACAS 2005},
  year		= {2005},
  editor	= {Nicolas Halbwachs and Lenore D. Zuck},
  series	= {LNCS},
  volume	= {3440},
  pages		= {93--107},
  publisher	= {Springer},
  doi		= {10.1007/978-3-540-31980-1_7}
}

@InProceedings{	  queille1982:cesar,
  title		= {Specification and verification of concurrent systems in
		  {CESAR}},
  author	= {Jean-Pierre Queille and Joseph Sifakis},
  booktitle	= {CONF_ISP 1982},
  year		= {1982},
  editor	= {Dezani-Ciancaglini, Mariangiola and Montanari, Ugo},
  series	= {LNCS},
  volume	= 137,
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {337--351},
  url		= {http://dx.doi.org/10.1007/3-540-11494-7_22},
  doi		= {10.1007/3-540-11494-7_22},
  ids		= {cesar}
}

@Article{	  raad2018:ptso,
  title		= {Persistence semantics for weak memory: Integrating epoch
		  persistency with the {TSO} memory model},
  author	= {Raad, Azalea and Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2018},
  month		= oct,
  issue_date	= {November 2018},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {2},
  number	= {OOPSLA},
  url		= {https://doi.org/10.1145/3276507},
  doi		= {10.1145/3276507},
  articleno	= {137},
  numpages	= {27}
}

@Article{	  raad2019:arm-pers,
  title		= {Weak persistency semantics from the ground up},
  author	= {Raad, Azalea and Wickerson, John and Vafeiadis, Viktor},
  volume	= {3},
  url		= {https://doi.org/10.1145/3360561},
  doi		= {10.1145/3360561},
  shorttitle	= {Weak persistency semantics from the ground up},
  pages		= {135:1--135:27},
  issue		= {{OOPSLA}},
  journaltitle	= {JNL_PACMPL},
  shortjournal	= {JNL_PACMPL},
  urldate	= {2020-02-07},
  date		= {2019-10-10},
  keywords	= {{ARMv}8, memory persistency, non-volatile memory, weak
		  memory}
}

@Article{	  raad2019:libraries,
  title		= {On library correctness under weak memory consistency:
		  Specifying and verifying concurrent libraries under
		  declarative consistency models},
  author	= {Raad, Azalea and Doko, Marko and Rožić, Lovro and Lahav,
		  Ori and Vafeiadis, Viktor},
  journal	= {JNL_PACMPL},
  year		= {2019},
  volume	= {3},
  number	= {{POPL}},
  pages		= {68:1--68:31},
  url		= {http://doi.acm.org/10.1145/3290381},
  doi		= {10.1145/3290381},
  ids		= {yacovet}
}

@Article{	  raad2020:x86-pers,
  title		= {Persistency semantics of the Intel-x86 architecture},
  author	= {Raad, Azalea and Wickerson, John and Neiger, Gil and
		  Vafeiadis, Viktor},
  volume	= {4},
  url		= {https://doi.org/10.1145/3371079},
  doi		= {10.1145/3371079},
  pages		= {11:1--11:31},
  issue		= {{POPL}},
  journaltitle	= {JNL_PACMPL},
  shortjournal	= {JNL_PACMPL},
  urldate	= {2020-06-17},
  date		= {2019-12-20},
  keywords	= {Intel-x86, memory persistency, non-volatile memory, weak
		  memory}
}

@InProceedings{	  rajaram2013:rmws-tso,
  title		= {Fast {RMWs} for {TSO}: {Semantics} and implementation},
  author	= {Rajaram, Bharghava and Nagarajan, Vijay and Sarkar, Susmit
		  and Elver, Marco},
  booktitle	= {CONF_PLDI 2013},
  year		= {2013},
  pages		= {61--72},
  doi		= {10.1145/2491956.2462196},
  publisher	= {ACM}
}

@InProceedings{	  regehr2011:bugs-compilers,
  title		= {Finding and understanding bugs in C compilers},
  author	= {Xuejun Yang and Yang Chen and Eric Eide and John Regehr},
  booktitle	= {CONF_PLDI 2011},
  year		= {2011},
  pages		= {283-294},
  editor	= {Mary W. Hall and David A. Padua},
  publisher	= {ACM}
}

@InProceedings{	  renesse2004:chain,
  title		= {Chain Replication for Supporting High Throughput and
		  Availability},
  author	= {Robbert van Renesse and Fred B. Schneider},
  booktitle	= {CONF_OSDI 2004},
  year		= {2004},
  editor	= {Eric A. Brewer and Peter Chen},
  pages		= {91--104},
  publisher	= {{USENIX} Association},
  url		= {http://www.usenix.org/events/osdi04/tech/renesse.html}
}

@InProceedings{	  renzelmann2012:drivers,
  title		= {SymDrive: Testing Drivers without Devices},
  author	= {Renzelmann, Matthew J. and Kadav, Asim and Swift, Michael
		  M.},
  booktitle	= {CONF_OSDI 2012},
  year		= {2012},
  publisher	= {{USENIX} Association},
  address	= {USA},
  pages		= {279–292},
  numpages	= {14},
  location	= {Hollywood, CA, USA},
  url		= {https://www.usenix.org/conference/osdi12/technical-sessions/presentation/renzelmann}
}

@InProceedings{	  reynolds1983:abstraction,
  title		= {Types, Abstraction, and Parametric Polymorphism},
  author	= {John C. Reynolds},
  booktitle	= {CONF_IFIP 1983},
  year		= {1983}
}

@InProceedings{	  ridge2010:rg-proof,
  title		= {A rely-guarantee proof system for {x86-TSO}},
  author	= {Ridge, Tom},
  booktitle	= {CONF_VSTTE 2010},
  year		= {2010},
  editor	= {Gary T. Leavens and Peter W. O'Hearn and Sriram K.
		  Rajamani},
  series	= {LNCS},
  volume	= {6217},
  pages		= {55--70},
  publisher	= {Springer}
}

@InProceedings{	  ridge2015:sibylfs,
  title		= {{SibylFS}: formal specification and oracle-based testing
		  for {POSIX} and real-world file systems},
  author	= {Ridge, Tom and Sheets, David and Tuerk, Thomas and
		  Giugliano, Andrea and Madhavapeddy, Anil and Sewell,
		  Peter},
  booktitle	= {CONF_SOSP 2015},
  location	= {Monterey, California},
  url		= {http://dl.acm.org/citation.cfm?doid=2815400.2815411},
  doi		= {10.1145/2815400.2815411},
  shorttitle	= {{SibylFS}},
  pages		= {38--53},
  publisher	= {ACM},
  urldate	= {2020-06-17},
  date		= {2015},
  langid	= {english}
}

@Article{	  rodeh2013:btrfs,
  title		= {{BTRFS}: The Linux B-Tree Filesystem},
  author	= {Rodeh, Ohad and Bacik, Josef and Mason, Chris},
  volume	= {9},
  doi		= {10.1145/2501620.2501623},
  shorttitle	= {{BTRFS}},
  pages		= {9:1--9:32},
  number	= {3},
  journaltitle	= {JNL_TS},
  shortjournal	= {JNL_TS},
  urldate	= {2020-06-17},
  date		= {2013-08-01},
  keywords	= {B-trees, concurrency, copy-on-write, filesystem, {RAID},
		  shadowing, snapshots},
  ids		= {btrfs}
}

@InProceedings{	  rodriguez2015:unfolding-dpor,
  title		= {Unfolding-based Partial Order Reduction},
  author	= {César Rodríguez and Marcelo Sousa and Subodh Sharma and
		  Daniel Kroening},
  booktitle	= {CONF_CONCUR 2015},
  year		= {2015},
  pages		= {456--469},
  series	= {LIPIcs},
  volume	= {42},
  publisher	= {DAGSTUHL},
  url		= {http://dx.doi.org/10.4230/LIPIcs.CONCUR.2015.456},
  doi		= {10.4230/LIPIcs.CONCUR.2015.456},
  ids		= {unfolding-dpor}
}

@InProceedings{	  roemer2018:race-prediction,
  title		= {High-coverage, Unbounded Sound Predictive Race Detection},
  author	= {Roemer, Jake and Genç, Kaan and Bond, Michael D.},
  booktitle	= {CONF_PLDI 2018},
  year		= {2018},
  location	= {Philadelphia, PA, USA},
  pages		= {374--389},
  numpages	= {16},
  url		= {http://doi.acm.org/10.1145/3192366.3192385},
  doi		= {10.1145/3192366.3192385},
  acmid		= {3192385},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {Data race detection, dynamic predictive analysis}
}

@InProceedings{	  rossberg2003:generativity,
  title		= {Generativity and Dynamic Opacity for Abstract Types},
  author	= {Andreas Rossberg},
  booktitle	= {CONF_PPDP 2003},
  year		= {2003}
}

@InProceedings{	  rossberg2004:alice-ml,
  title		= {Alice {ML} Through the Looking Glass},
  author	= {Andreas Rossberg and Didier {Le Botlan} and Guido Tack and
		  Thorsten Brunklaus and Gert Smolka},
  booktitle	= {CONF_TFP 2004},
  year		= {2004},
  volume	= 5
}

@Misc{		  rti2002:testing-impact,
  title		= {The Economic Impacts of Inadequate Infrastructure for
		  Software Testing},
  author	= {{RTI}},
  year		= {2002},
  month		= may,
  url		= {https://www.nist.gov/system/files/documents/director/planning/report02-3.pdf},
  urldate	= {2020-11-26}
}

@InProceedings{	  saraswat2007:memory-models,
  title		= {A theory of memory models},
  author	= {Saraswat, Vijay A. and Jagadeesan, Radha and Michael,
		  Maged and von Praun, Christoph},
  booktitle	= {CONF_PPOPP 2007},
  year		= {2007}
}

@InProceedings{	  sarkar2011:understanding-power,
  title		= {Understanding {POWER} multiprocessors},
  author	= {Susmit Sarkar and Peter Sewell and Jade Alglave and Luc
		  Maranget and Derek Williams},
  booktitle	= {CONF_PLDI 2011},
  year		= {2011},
  pages		= {175--186},
  publisher	= {{ACM}},
  doi		= {10.1145/1993498.1993520}
}

@InProceedings{	  sarkar2012:synchronising-power,
  title		= {Synchronising {C/C++} and {POWER}},
  author	= {Sarkar, Susmit and Memarian, Kayvan and Owens, Scott and
		  Batty, Mark and Sewell, Peter and Maranget, Luc and
		  Alglave, Jade and Williams, Derek},
  booktitle	= {CONF_PLDI 2012},
  year		= {2012},
  pages		= {311--322},
  doi		= {10.1145/2254064.2254102},
  publisher	= {ACM}
}

@InProceedings{	  schellhorn2012:linearisable,
  title		= {How to prove algorithms linearisable},
  author	= {Schellhorn, Gerhard and Wehrheim, Heike and Derrick,
		  John},
  booktitle	= {CONF_CAV 2012},
  year		= {2012},
  pages		= {243-259},
  editor	= {P. Madhusudan and Sanjit A. Seshia},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {7358}
}

@InProceedings{	  schmidt2012:tamarin,
  title		= {Automated Analysis of Diffie-Hellman Protocols and
		  Advanced Security Properties},
  author	= {Schmidt, Benedikt and Meier, Simon and Cremers, Cas and
		  Basin, David},
  booktitle	= {CONF_CSFS 2012},
  year		= {2012},
  pages		= {78-94},
  doi		= {10.1109/CSF.2012.25},
  url		= {https://doi.org/10.1109/CSF.2012.25}
}

@InProceedings{	  sen2006:cute,
  title		= {{CUTE} and {jCUTE}: concolic unit testing and explicit
		  path model-checking tools},
  author	= {Sen, Koushik and Agha, Gul},
  booktitle	= {CONF_CAV 2006},
  year		= {2006},
  isbn		= {354037406X},
  publisher	= {Springer-Verlag},
  address	= {Berlin, Heidelberg},
  url		= {https://doi.org/10.1007/11817963_38},
  doi		= {10.1007/11817963_38},
  pages		= {419–423},
  numpages	= {5},
  location	= {Seattle, WA},
  optseries	= {CAV'06}
}

@InProceedings{	  sen2006:sct,
  title		= {Automated Systematic Testing of Open Distributed
		  Programs},
  author	= {Sen, Koushik and Agha, Gul},
  booktitle	= {CONF_FASE 2006},
  year		= {2006},
  editor	= {Baresi, Luciano and Heckel, Reiko},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {339--356},
  doi		= {10.1007/11693017_25}
}

@InProceedings{	  serbanuta2013:mcm,
  title		= {Maximal Causal Models for Sequentially Consistent
		  Systems},
  author	= {Şerbănuţă, Traian Florin and Chen, Feng and Roşu,
		  Grigore},
  booktitle	= {CONF_RV 2012},
  year		= {2013},
  editor	= {Qadeer, Shaz and Tasiran, Serdar},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {136--150},
  doi		= {10.1007/978-3-642-35632-2_16}
}

@Article{	  sergey2018:diesel,
  title		= {Programming and proving with distributed protocols},
  author	= {Ilya Sergey and James R. Wilcox and Zachary Tatlock},
  journal	= {JNL_PACMPL},
  year		= {2018},
  volume	= {2},
  number	= {{POPL}},
  pages		= {28:1--28:30},
  url		= {https://doi.org/10.1145/3158116},
  doi		= {10.1145/3158116}
}

@InProceedings{	  sevcik2008:transformations-java,
  title		= {On validity of program transformations in the {Java}
		  memory model},
  author	= {Jaroslav Ševčík and David Aspinall},
  booktitle	= {CONF_ECOOP 2008},
  year		= {2008},
  pages		= {27-51},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {5142}
}

@InProceedings{	  sevcik2011:optimizations,
  title		= {Safe Optimisations for Shared-memory Concurrent Programs},
  author	= {Ševčík, Jaroslav},
  booktitle	= {CONF_PLDI 2011},
  year		= {2011},
  location	= {San Jose, California, USA},
  pages		= {306--316},
  numpages	= {11},
  url		= {http://doi.acm.org/10.1145/1993498.1993534},
  doi		= {10.1145/1993498.1993534},
  acmid		= {1993534},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {compiler optimizations, relaxed memory models, semantics}
}

@Article{	  sevcik2013:compcerttso,
  title		= {{CompCertTSO}: {A} verified compiler for relaxed-memory
		  concurrency},
  author	= {Jaroslav Ševčík and Viktor Vafeiadis and Francesco
		  {Zappa Nardelli} and Suresh Jagannathan and Peter Sewell},
  journal	= {JNL_JACM},
  year		= {2013},
  volume	= {60},
  number	= {3},
  pages		= {22}
}

@Article{	  sewell2007:acute,
  title		= {Acute: {High-level} programming language design for
		  distributed computation},
  author	= {Peter Sewell and James Leifer and Keith Wansbrough and
		  Francesco {Zappa Nardelli} and Mair Allen-Williams and
		  Pierre Habouzit and Viktor Vafeiadis},
  journal	= {JNL_JFP},
  year		= 2007,
  volume	= 17,
  number	= {4\&5},
  pages		= {547--612}
}

@Article{	  sewell2010:x86-tso,
  title		= {{X86-TSO}: {A} Rigorous and Usable Programmer's Model for
		  x86 Multiprocessors},
  author	= {Sewell, Peter and Sarkar, Susmit and Owens, Scott and
		  Nardelli, Francesco Zappa and Myreen, Magnus O.},
  journal	= {JNL_CACM},
  year		= {2010},
  month		= jul,
  issue_date	= {July 2010},
  volume	= {53},
  number	= {7},
  pages		= {89--97},
  doi		= {10.1145/1785414.1785443},
  publisher	= {ACM}
}

@PhDThesis{	  sezgin:thesis,
  title		= {Formalization and verification of shared memory},
  author	= {Sezgin, Ali},
  year		= {2004},
  school	= {University of Utah},
  optadvisor	= {Gopalakrishnan, Sanesh C.}
}

@Article{	  shasha1988:traces,
  title		= {Efficient and correct execution of parallel programs that
		  share memory},
  author	= {Shasha, Dennis and Snir, Marc},
  journal	= {JNL_TOPLAS},
  year		= {1988},
  month		= apr,
  issue_date	= {April 1988},
  volume	= {10},
  number	= {2},
  pages		= {282--312},
  numpages	= {31},
  url		= {http://doi.acm.org/10.1145/42190.42277},
  doi		= {10.1145/42190.42277},
  acmid		= {42277},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  ids		= {shasha-snir}
}

@InProceedings{	  sieczkowski2015:icap-tso,
  title		= {A separation logic for fictional sequential consistency},
  author	= {Filip Sieczkowski and Kasper Svendsen and Lars Birkedal
		  and Pichon-Pharabod, Jean},
  booktitle	= {CONF_ESOP 2015},
  year		= {2015},
  editor	= {Jan Vitek},
  series	= {LNCS},
  volume	= {9032},
  pages		= {736--761},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg}
}

@InProceedings{	  sigurbjarnarson2016:crash-refinement,
  title		= {Push-Button Verification of File Systems via Crash
		  Refinement},
  author	= {Sigurbjarnarson, Helgi and Bornholt, James and Torlak,
		  Emina and Wang, Xi},
  booktitle	= {CONF_OSDI 2016},
  year		= {2016},
  publisher	= {USENIX Association},
  address	= {USA},
  pages		= {1–16},
  numpages	= {16},
  location	= {Savannah, GA, USA},
  url		= {https://www.usenix.org/system/files/conference/osdi16/osdi16-sigurbjarnarson.pdf},
  ids		= {yggdrasil}
}

@InProceedings{	  simner2022:relaxed-virtual,
  title		= {Relaxed virtual memory in {Armv8-A}},
  author	= { Ben Simner and Alasdair Armstrong and Jean
		  Pichon-Pharabod and Christopher Pulte and Richard
		  Grisenthwaite and Peter Sewell },
  booktitle	= {CONF_ESOP 2022},
  year		= {2022},
  month		= apr,
  series	= {LNCS},
  volume	= {13240},
  pages		= {143--173},
  publisher	= {Springer},
  optyear	= {2022},
  url		= {https://doi.org/10.1007/978-3-030-99336-8_6},
  doi		= {10.1007/978-3-030-99336-8_6}
}

@InProceedings{	  smaragdakis2012:race-prediction,
  title		= {Sound Predictive Race Detection in Polynomial Time},
  author	= {Smaragdakis, Yannis and Evans, Jacob and Sadowski, Caitlin
		  and Yi, Jaeheon and Flanagan, Cormac},
  booktitle	= {CONF_POPL 2012},
  year		= {2012},
  location	= {Philadelphia, PA, USA},
  pages		= {387--400},
  numpages	= {14},
  url		= {http://doi.acm.org/10.1145/2103656.2103702},
  doi		= {10.1145/2103656.2103702},
  acmid		= {2103702},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {dynamic analysis, happens-before, race detection}
}

@Article{	  smith2017:lin-as-trace-refinement,
  title		= {Relating Trace Refinement and Linearizability},
  author	= {Smith, Graeme and Winter, Kirsten},
  journal	= {Form. Asp. Comput.},
  year		= 2017,
  month		= {nov},
  volume	= 29,
  number	= 6,
  pages		= {935–950},
  issue_date	= {Nov 2017},
  publisher	= {Springer-Verlag},
  address	= {Berlin, Heidelberg},
  doi		= {10.1007/s00165-017-0418-2},
  numpages	= 16,
  keywords	= {Trace refinement, Linearizability, Concurrency,
		  Correctness}
}

@InProceedings{	  son2017:ext4,
  title		= {Guaranteeing the Metadata Update Atomicity in {EXT}4 File
		  system},
  author	= {Son, Seongbae and Yoo, Jinsoo and Won, Youjip},
  booktitle	= {CONF_APSYS 2017},
  year		= {2017},
  url		= {https://doi.org/10.1145/3124680.3124722},
  doi		= {10.1145/3124680.3124722},
  pages		= {1--8},
  publisher	= {ACM},
  optlocation	= {Mumbai, India}
}

@Article{	  steinke2004:shared-memory,
  title		= {A unified theory of shared memory consistency},
  author	= {Steinke, Robert C. and Nutt, Gary J.},
  journal	= {JNL_JACM},
  year		= {2004},
  month		= sep,
  issue_date	= {September 2004},
  volume	= {51},
  number	= {5},
  pages		= {800--849},
  doi		= {10.1145/1017460.1017464},
  publisher	= {ACM}
}

@Article{	  stoica2003:chord,
  title		= {Chord: a scalable peer-to-peer lookup protocol for
		  internet applications},
  author	= {Ion Stoica and Robert Tappan Morris and David
		  Liben{-}Nowell and David R. Karger and M. Frans Kaashoek
		  and Frank Dabek and Hari Balakrishnan},
  journal	= {JNL_TN},
  year		= {2003},
  volume	= {11},
  number	= {1},
  pages		= {17--32},
  url		= {https://doi.org/10.1109/TNET.2002.808407},
  doi		= {10.1109/TNET.2002.808407}
}

@Article{	  sumii2007:bisimulation,
  title		= {A Bisimulation for Type Abstraction and Recursion},
  author	= {Eijiro Sumii and Benjamin Pierce},
  journal	= {JNL_JACM},
  year		= 2007,
  volume	= 54,
  number	= 5,
  pages		= {1--43}
}

@Article{	  sun2022:deagle,
  title		= {Consistency-Preserving Propagation for {SMT} Solving of
		  Concurrent Program Verification},
  author	= {Sun, Zhihang and Fan, Hongyu and He, Fei},
  journal	= {JNL_PACMPL},
  year		= {2022},
  month		= {oct},
  issue_date	= {October 2022},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  volume	= {6},
  number	= {OOPSLA2},
  url		= {https://doi.org/10.1145/3563321},
  doi		= {10.1145/3563321},
  articleno	= {158},
  numpages	= {28},
  keywords	= {satisfiability modulo theories, concurrent programs,
		  memory model, program verification},
  ids		= {deagle}
}

@InProceedings{	  svendsen2013:icap,
  title		= {Impredicative Concurrent Abstract Predicates},
  author	= {Svendsen, Kasper and Birkedal, Lars},
  booktitle	= {CONF_ESOP 2014},
  year		= {2014},
  editor	= {Shao, Zhong},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {149--168},
  ids		= {iCAP, icap}
}

@InProceedings{	  svendsen2018:slr,
  title		= {A separation logic for a promising semantics},
  author	= {Kasper Svendsen and Jean Pichon-Pharabod and Marko Doko
		  and Ori Lahav and Viktor Vafeiadis},
  booktitle	= {CONF_ESOP 2018},
  year		= {2018},
  editor	= {Amal Ahmed},
  series	= {LNCS},
  volume	= {10801},
  pages		= {357--384},
  publisher	= {Springer},
  doi		= {10.1007/978-3-319-89884-1_13}
}

@InProceedings{	  sweeney1996:xfs,
  title		= {Scalability in the {XFS} file system},
  author	= {Sweeney, Adam},
  booktitle	= {CONF_USENIX 1996},
  pages		= {1--14},
  date		= {1996},
  url		= {https://www.usenix.org/legacy/publications/library/proceedings/sd96/sweeney.html},
  ids		= {xfs}
}

@InProceedings{	  tasharofi2012:trans-dpor,
  title		= {TransDPOR: A Novel Dynamic Partial-Order Reduction
		  Technique for Testing Actor Programs},
  author	= {Tasharofi, Samira and Karmani, Rajesh K. and Lauterburg,
		  Steven and Legay, Axel and Marinov, Darko and Agha, Gul},
  booktitle	= {CONF_FORTE 2012},
  year		= {2012},
  editor	= {Giese, Holger and Rosu, Grigore},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {219--234},
  doi		= {10.1007/978-3-642-30793-5_14}
}

@InProceedings{	  tassarotti2015:rcu,
  title		= {Verifying read-copy-update in a logic for weak memory},
  author	= {Tassarotti, Joseph and Dreyer, Derek and Vafeiadis,
		  Viktor},
  booktitle	= {CONF_PLDI 2015},
  year		= {2015},
  pages		= {110--120},
  numpages	= {11},
  doi		= {10.1145/2737924.2737992},
  publisher	= {ACM}
}

@InProceedings{	  thomson2014:sct,
  title		= {Concurrency testing using schedule bounding: an empirical
		  study},
  author	= {Paul Thomson and Alastair F. Donaldson and Adam Betts},
  booktitle	= {CONF_PPOPP 2014},
  year		= {2014},
  pages		= {15--28},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/2555243.2555260},
  doi		= {10.1145/2555243.2555260}
}

@InProceedings{	  tillman2008:pex,
  title		= {{Pex}--{White} Box Test Generation for {.NET}},
  author	= {Tillmann, Nikolai and de Halleux, Jonathan},
  booktitle	= {CONF_TAP 2008},
  year		= {2008},
  publisher	= {Springer},
  address	= {Berlin, Heidelberg},
  pages		= {134--153},
  isbn		= {978-3-540-79124-9},
  doi		= {10.1007/978-3-540-79124-9_10}
}

@InProceedings{	  torlak2010:memsat,
  title		= {MemSAT: checking axiomatic specifications of memory
		  models},
  author	= {Emina Torlak and Mandana Vaziri and Julian Dolby},
  booktitle	= {CONF_PLDI 2010},
  year		= {2010},
  editor	= {Benjamin G. Zorn and Alexander Aiken},
  pages		= {341--350},
  publisher	= {{ACM}},
  url		= {https://doi.org/10.1145/1806596.1806635},
  doi		= {10.1145/1806596.1806635},
  opttimestamp	= {Tue, 22 Jun 2021 17:10:56 +0200},
  optbiburl	= {https://dblp.org/rec/conf/pldi/TorlakVD10.bib},
  optbibsource	= {dblp computer science bibliography, https://dblp.org}
}

@InProceedings{	  travkin2013:spin-as-lin-checker,
  title		= {SPIN as a Linearizability Checker under Weak Memory
		  Models},
  author	= {Travkin, Oleg and M{\"u}tze, Annika and Wehrheim, Heike},
  booktitle	= {CONF_HVC 2013},
  year		= 2013,
  editor	= {Bertacco, Valeria and Legay, Axel},
  pages		= {311--326},
  address	= {Cham},
  publisher	= {Springer}
}

@TechReport{	  treiber1986:stack,
  title		= {Systems Programming: Coping with Parallelism},
  author	= {R. Kent Treiber},
  year		= {1986},
  institution	= {Technical Report RJ5118, IBM},
  url		= {https://dominoweb.draco.res.ibm.com/58319a2ed2b1078985257003004617ef.html},
  ids		= {treiber-stack}
}

@InProceedings{	  trippel2017:tricheck,
  title		= {TriCheck: {Memory} model verification at the trisection of
		  software, hardware, and {ISA}},
  author	= {Trippel, Caroline and Manerkar, Yatin A. and Lustig,
		  Daniel and Pellauer, Michael and Martonosi, Margaret},
  booktitle	= {CONF_ASPLOS 2017},
  year		= {2017},
  location	= {Xi'an, China},
  pages		= {119--133},
  numpages	= {15},
  url		= {http://doi.acm.org/10.1145/3037697.3037719},
  doi		= {10.1145/3037697.3037719},
  acmid		= {3037719},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {here, keywords, separated by semi-colons},
  ids		= {tricheck}
}

@Article{	  tso2002:ext3-extensions,
  title		= {Planned Extensions to the Linux Ext2/Ext3 Filesystem},
  author	= {Theodore Y Ts'o and Stephen Tweedie},
  booktitle	= {CONF_FREENIX 2002},
  year		= {2002},
  pages		= {235--243},
  url		= {http://www.usenix.org/publications/library/proceedings/usenix02/tech/freenix/tso.html},
  publisher	= {{USENIX}},
  editor	= {Chris G. Demetriou},
  ids		= {ext4}
}

@InProceedings{	  turon2013:caresl,
  title		= {Unifying refinement and {H}oare-style reasoning in a logic
		  for higher-order concurrency},
  author	= {Aaron Turon and Derek Dreyer and Lars Birkedal},
  booktitle	= {CONF_ICFP 2013},
  year		= {2013},
  publisher	= {ACM},
  ids		= {caresl}
}

@InProceedings{	  turon2014:gps,
  title		= {{GPS}: {Navigating} weak memory with ghosts, protocols,
		  and separation},
  author	= {Turon, Aaron and Vafeiadis, Viktor and Dreyer, Derek},
  booktitle	= {CONF_OOPSLA 2014},
  year		= {2014},
  pages		= {691--707},
  numpages	= {17},
  doi		= {10.1145/2660193.2660243},
  publisher	= {ACM}
}

@InProceedings{	  tweedie1998:journaling,
  title		= {Journaling the Linux ext2fs filesystem},
  author	= {Tweedie, Stephen C},
  booktitle	= {CONF_LINUXEXPO 1998},
  year		= {1998},
  numpages	= {8},
  url		= {http://e2fsprogs.sourceforge.net/journal-design.pdf},
  urldate	= {2020-11-16},
  ids		= {ext3-journaling, ext4-journaling}
}

@InProceedings{	  vafeiadis2007:rgsep,
  title		= {A marriage of rely/guarantee and separation logic},
  author	= {Vafeiadis, Viktor and Parkinson, Matthew},
  booktitle	= {CONF_CONCUR 2007},
  year		= {2007},
  series	= {LNCS},
  volume	= {4703},
  pages		= {256--271},
  publisher	= {Springer},
  ids		= {rgsep}
}

@InProceedings{	  vafeiadis2009:shape-value,
  title		= {Shape-Value Abstraction for Verifying Linearizability},
  author	= {Viktor Vafeiadis},
  booktitle	= {CONF_VMCAI 2009},
  year		= {2009},
  pages		= {335-348},
  editor	= {Neil D. Jones and Markus Müller-Olm},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {5403},
  doi		= {10.1007/978-3-540-93900-9_27}
}

@InProceedings{	  vafeiadis2010:cave,
  title		= {Automatically Proving Linearizability},
  author	= {Viktor Vafeiadis},
  booktitle	= {CONF_CAV 2010},
  year		= {2010},
  pages		= {450-464},
  editor	= {Tayssir Touili and Byron Cook and Paul Jackson},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {6174},
  ids		= {cave}
}

@InProceedings{	  vafeiadis2010:inference,
  title		= {{RGSep} Action Inference},
  author	= {Viktor Vafeiadis},
  booktitle	= {CONF_VMCAI 2010},
  year		= {2010},
  pages		= {345-361},
  editor	= {Gilles Barthe and Manuel V. Hermenegildo},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {5944}
}

@Article{	  vafeiadis2011:cslsound,
  title		= {Concurrent separation logic and operational semantics},
  author	= {Viktor Vafeiadis},
  journal	= {JNL_ENTCS},
  year		= {2011},
  volume	= {276},
  pages		= {335-351}
}

@InProceedings{	  vafeiadis2011:fence-elim,
  title		= {Verifying Fence Elimination Optimisations},
  author	= {Vafeiadis, Viktor and Zappa Nardelli, Francesco},
  booktitle	= {CONF_SAS 2011},
  year		= {2011},
  pages		= {146--162},
  editor	= {Eran Yahav},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {6887}
}

@InProceedings{	  vafeiadis2013:adjustable-refs,
  title		= {Adjustable References},
  author	= {Viktor Vafeiadis},
  booktitle	= {CONF_ITP 2013},
  year		= {2013},
  pages		= {328-337},
  editor	= {Sandrine Blazy and Christine Paulin-Mohring and David
		  Pichardie},
  publisher	= {Springer},
  series	= {LNCS},
  volume	= {7998}
}

@InProceedings{	  vafeiadis2013:rsl,
  title		= {Relaxed Separation Logic: {A} program logic for {C11}
		  concurrency},
  author	= {Viktor Vafeiadis and Chinmay Narayan},
  booktitle	= {CONF_OOPSLA 2013},
  year		= {2013},
  pages		= {867--884},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  url		= {http://doi.acm.org/10.1145/2509136.2509532},
  doi		= {10.1145/2509136.2509532},
  ids		= {rsl}
}

@InProceedings{	  vafeiadis2015:optimisations,
  title		= {Common compiler optimisations are invalid in the {C11}
		  memory model and what we can do about it},
  author	= {Vafeiadis, Viktor and Balabonski, Thibaut and Chakraborty,
		  Soham and Morisset, Robin and Zappa Nardelli, Francesco},
  booktitle	= {CONF_POPL 2015},
  year		= {2015},
  location	= {Mumbai, India},
  pages		= {209--220},
  numpages	= {12},
  url		= {http://doi.acm.org/10.1145/2676726.2676995},
  doi		= {10.1145/2676726.2676995},
  acmid		= {2676995},
  publisher	= {ACM},
  address	= {New York, NY, USA},
  keywords	= {c/c++, compilers, concurrency, program transformations,
		  weak memory models}
}

@PhDThesis{	  vafeiadis:thesis,
  title		= {Modular fine-grained concurrency verification},
  author	= {Vafeiadis, Viktor},
  year		= {2007},
  school	= {University of Cambridge},
  note		= {Available as Tech.\ report UCAML-CL-TR-726}
}

@InCollection{	  van-de-pol:2010:symbolic-relaxed,
  title		= {An Automata-Based Symbolic Approach for Verifying Programs
		  on Relaxed Memory Models},
  author	= {Linden, Alexander and Wolper, Pierre},
  booktitle	= {CONF_SPIN 2010},
  year		= {2010},
  volume	= {6349},
  series	= {LNCS},
  editor	= {van de Pol, Jaco and Weber, Michael},
  doi		= {10.1007/978-3-642-16164-3_16},
  publisher	= {Springer},
  pages		= {212-226}
}

@InProceedings{	  vechev2009:linearizability-checking,
  title		= {Experience with Model Checking Linearizability},
  author	= {Vechev, Martin and Yahav, Eran and Yorsh, Greta},
  booktitle	= {CONF_SPIN 2009},
  year		= 2009,
  editor	= {P{\u{a}}s{\u{a}}reanu, Corina S.},
  pages		= {261--278},
  address	= {Berlin, Heidelberg},
  publisher	= {Springer}
}

@InProceedings{	  vytiniotis2005:typecase,
  title		= {An Open and Shut Typecase},
  author	= {Dimitrios Vytiniotis and Geoffrey Washburn and Stephanie
		  Weirich},
  booktitle	= {CONF_TLDI 2005},
  year		= 2005
}

@InProceedings{	  wagemaker2022:cnetkat,
  title		= {Concurrent NetKAT - Modeling and analyzing stateful,
		  concurrent networks},
  author	= {Jana Wagemaker and Nate Foster and Tobias Kappé and
		  Dexter Kozen and Jurriaan Rot and Alexandra Silva},
  booktitle	= {CONF_ESOP 2022},
  year		= {2022},
  editor	= {Ilya Sergey},
  series	= {LNCS},
  volume	= {13240},
  pages		= {575--602},
  publisher	= {Springer},
  doi		= {10.1007/978-3-030-99336-8_21}
}

@Article{	  wahl2010:symmetry,
  title		= {Replication and Abstraction: Symmetry in Automated Formal
		  Verification},
  author	= {Wahl, Thomas and Donaldson, Alastair},
  journal	= {JNL_SYMM},
  year		= {2010},
  volume	= {2},
  number	= {2},
  pages		= {799--847},
  url		= {https://www.mdpi.com/2073-8994/2/2/799},
  doi		= {10.3390/sym2020799}
}

@Article{	  wang2018:jpf,
  title		= {A Progress Bar for the {JPF} Search Using Program
		  Executions},
  author	= {Kaiyuan Wang and Hayes Converse and Milos Gligoric and
		  Sasa Misailovic and Sarfraz Khurshid},
  journal	= {JNL_SEN},
  year		= {2018},
  volume	= {43},
  number	= {4},
  pages		= {55},
  url		= {https://doi.org/10.1145/3282517.3282525},
  doi		= {10.1145/3282517.3282525}
}

@Article{	  wegman1991:constant-propagation,
  title		= {Constant propagation with conditional branches},
  author	= {Wegman, Mark N. and Zadeck, F. Kenneth},
  journal	= {JNL_TOPLAS},
  year		= {1991},
  month		= apr,
  issue_date	= {April 1991},
  volume	= {13},
  number	= {2},
  pages		= {181--210},
  numpages	= {30},
  acmid		= {103136},
  publisher	= {ACM}
}

@InProceedings{	  wehrman2011:wm-local-reasoning,
  title		= {A proposal for weak-memory local reasoning},
  author	= {Wehrman, Ian and Berdine, Josh},
  booktitle	= {CONF_LOLA 2011},
  year		= {2011}
}

@InProceedings{	  wickerson2017:comparing-models,
  title		= {Automatically Comparing Memory Consistency Models},
  author	= {Wickerson, John and Batty, Mark and Sorensen, Tyler and
		  Constantinides, George A.},
  booktitle	= {CONF_POPL 2017},
  year		= {2017},
  pages		= {190--204},
  publisher	= {ACM},
  url		= {https://doi.org/10.1145/3009837.3009838},
  doi		= {10.1145/3009837.3009838},
  ids		= {memalloy}
}

@Misc{		  www:advanced-format-wikipedia,
  key		= {advanced format},
  title		= {Advanced Format},
  year		= {2020},
  url		= {https://en.wikipedia.org/wiki/Advanced_Format},
  urldate	= {2020-05-20}
}

@Misc{		  www:aws-s3-objects,
  key		= {Amazon {S3} – 566 Billion Objects, 370,000
		  Requests/Second, and Hiring!},
  title		= {Amazon {S3} – 566 Billion Objects, 370,000
		  Requests/Second, and Hiring!},
  author	= {Jeff Barr},
  year		= {2011},
  url		= {https://aws.amazon.com/blogs/aws/amazon-s3-566-billion-objects-370000-requestssecond-and-hiring},
  urldate	= {2024-11-02}
}

@Misc{		  www:chromium-ext4-data-loss,
  key		= {ext4: Filesystem corruption on panic},
  title		= {ext4: Filesystem corruption on panic},
  author	= {{ext4 corruption}},
  year		= {2015},
  url		= {https://bugs.chromium.org/p/chromium/issues/detail?id=502898},
  urldate	= {2020-05-20}
}

@Misc{		  www:ckit,
  title		= {Concurrency Kit},
  author	= {Samy Al Bahra},
  url		= {https://github.com/concurrencykit/ck}
}

@Misc{		  www:copy-on-write-wikipedia,
  key		= {copy on write},
  title		= {Copy-on-write},
  year		= {2020},
  url		= {https://en.wikipedia.org/wiki/Copy-on-write},
  urldate	= {2020-05-20}
}

@Misc{		  www:coq,
  key		= {coq},
  title		= {The {Coq} Proof Assistant},
  author	= {{INRIA}},
  url		= {http://coq.inria.fr/},
  ids		= {coq}
}

@Misc{		  www:crossbeam,
  key		= {crossbeam},
  title		= {Crossbeam: {Support} for concurrent and parallel
		  programming},
  url		= {https://github.com/aturon/crossbeam},
  urldate	= {2016-10-24}
}

@Misc{		  www:crossbeam-fc-queue,
  key		= {crossbeam-flat-combining},
  title		= {Crossbeam: Flat combining \#63},
  author	= {Samuel Schetterer},
  year		= {2016},
  url		= {https://github.com/crossbeam-rs/crossbeam/issues/63},
  urldate	= {2021-01-29}
}

@Misc{		  www:dot-wikipedia,
  key		= {dot},
  title		= {{DOT} (graph description language)},
  year		= {2023},
  url		= {https://en.wikipedia.org/wiki/DOT_(graph_description_language)},
  urldate	= {2023-06-16}
}

@Misc{		  www:emacs,
  key		= {emacs},
  title		= {{GNU Emacs}: An extensible, customizable, free/libre text
		  editor — and more},
  author	= {{GNU Emacs}},
  year		= {2019},
  urldate	= {2020-06-15},
  url		= {https://www.gnu.org/software/emacs/},
  ids		= {emacs}
}

@Misc{		  www:ext4-data-loss,
  key		= {ext4 data loss},
  title		= {Ext4 data loss},
  year		= {2009},
  url		= {https://bugs.launchpad.net/ubuntu/+source/linux/+bug/317781},
  urldate	= {2020-05-20}
}

@Misc{		  www:ext4-kernel-doc,
  key		= {ext4 kernel doc},
  title		= {ext4 Data Structures and Algorithms},
  author	= {ext4 Linux kernel},
  year		= {2020},
  url		= {https://www.kernel.org/doc/html/latest/filesystems/ext4/index.html},
  urldate	= {2020-05-20}
}

@Misc{		  www:ext4-odirect,
  key		= {ext4 odirect},
  title		= {Clarifying direct IO's semantics},
  url		= {https://ext4.wiki.kernel.org/index.php/Clarifying_Direct_IO%27s_Semantics},
  urldate	= {2020-05-20}
}

@Misc{		  www:ext4-tuning-benchmarks,
  key		= {ext4 benchmarks},
  title		= {EXT4 File-System Tuning Benchmarks},
  author	= {{ext4 benchmarks}},
  year		= {2012},
  url		= {https://www.phoronix.com/scan.php?page=article&item=ext4_linux35_tuning&num=1},
  urldate	= {2020-05-20}
}

@Misc{		  www:folly,
  title		= {Folly: {Facebook} Open-source Library},
  author	= {Facebook},
  url		= {https://github.com/facebook/folly}
}

@Misc{		  www:genmc,
  key		= {genmc},
  title		= {{GenMC}: {Generic} model checking for C programs},
  author	= {Michalis Kokologiannakis},
  url		= {https://github.com/MPI-SWS/genmc},
  ids		= {genmc-github}
}

@Misc{		  www:genmc,
  key		= {Model checking for weakly consistent libraries (Project
		  page)},
  title		= {Model checking for weakly consistent libraries (Project
		  page)},
  author	= {Kokologiannakis, Michalis and Raad, Azalea and Vafeiadis,
		  Viktor},
  year		= {2019},
  url		= {https://plv.mpi-sws.org/genmc},
  urldate	= {2021-04-01},
  ids		= {genmc-page}
}

@Misc{		  www:genmc-mimalloc-issue,
  key		= {play-nice-with-tsan},
  title		= {Play nice with thread sanitizer \#130},
  author	= {mary3000},
  url		= {https://github.com/microsoft/mimalloc/issues/130#issuecomment-662666849},
  urldate	= {2022-10-16}
}

@Misc{		  www:genmc-mimalloc-patch,
  key		= {fix-memory-order-for-weak-CAS},
  title		= {fix memory order for weak CAS},
  author	= {daanx},
  url		= {https://github.com/microsoft/mimalloc/commit/444afa934ff8d53bf8c53602246bfc65828dc1d9},
  urldate	= {2022-10-16}
}

@Misc{		  www:joe,
  key		= {joe},
  title		= {{JOE} - Joe's Own Editor},
  author	= {{JOE}},
  year		= {2018},
  url		= {https://joe-editor.sourceforge.io},
  urldate	= {2020-06-15},
  ids		= {joe}
}

@Misc{		  www:kater,
  key		= {{Kater}: {Automating} Weak Memory Model Metatheory and
		  Consistency Checking (Project page)},
  title		= {{Kater}: {Automating} Weak Memory Model Metatheory and
		  Consistency Checking (Project page)},
  author	= {Kokologiannakis, Michalis and Lahav, Ori and Vafeiadis,
		  Viktor},
  year		= {2023},
  url		= {https://plv.mpi-sws.org/kater},
  urldate	= {2024-01-22},
  ids		= {kater-page}
}

@Misc{		  www:libcds,
  title		= {{CDS} {C++} library},
  author	= {Max Khizhinsky},
  url		= {https://github.com/khizmax/libcds}
}

@Misc{		  www:linux-elisa,
  key		= {linux elisa},
  title		= {The Linux Foundation Launches ELISA Project Enabling Linux
		  In Safety-Critical Systems},
  author	= {Emily Olin},
  year		= {2019},
  month		= feb,
  day		= {21},
  url		= {https://www.linuxfoundation.org/press-release/2019/02/the-linux-foundation-launches-elisa-project-enabling-linux-in-safety-critical-systems/},
  urldate	= {2019-08-16}
}

@Misc{		  www:linux-manpages,
  key		= {linux manpages},
  title		= {Linux man pages},
  year		= {2020},
  url		= {http://www.man7.org/linux/man-pages/index.html},
  urldate	= {2020-05-20}
}

@Misc{		  www:linux-medical-devices,
  key		= {choosing linux},
  title		= {Choosing Linux for Medical Devices},
  author	= {Ken Harold},
  year		= {2014},
  url		= {https://www.windriver.com/whitepapers/choosing-linux-for-medical-devices/White_Paper_Choosing_Linux_for_Medical_Devices.pdf},
  urldate	= {2019-08-16}
}

@Misc{		  www:linux-vfs-doc,
  key		= {linux vfs},
  title		= {Overview of the Linux Virtual File System},
  author	= {Gooch, Richard},
  year		= {1999},
  url		= {https://www.kernel.org/doc/html/latest/filesystems/vfs.html},
  urldate	= {2020-05-20}
}

@Misc{		  www:lkmm-tests,
  key		= {lkmm-tests},
  title		= {Automatically generated litmus tests for validation
		  {LISA}-language {Linux-kernel} memory models},
  author	= {Paul E. McKenney},
  year		= {2021},
  url		= {https://github.com/paulmckrcu/litmus},
  urldate	= {2021-05-28}
}

@Misc{		  www:lli,
  key		= {lli},
  title		= {{lli} - directly execute programs from {LLVM} bitcode},
  year		= {2003},
  url		= {https://llvm.org/docs/CommandGuide/lli.html},
  urldate	= {2021-01-29},
  ids		= {lli}
}

@Misc{		  www:llvm-pass,
  key		= {llvm-pass},
  title		= {Writing an LLVM Pass},
  year		= {2003},
  url		= {https://llvm.org/docs/WritingAnLLVMPass.html#introduction-what-is-a-pass},
  urldate	= {2023-06-16},
  ids		= {llvm-pass}
}

@Misc{		  www:loom-tool,
  key		= {loom},
  title		= {Loom},
  url		= {https://github.com/tokio-rs/loom},
  urldate	= {2023-07-06}
}

@Misc{		  www:lwn-ext4-data-loss,
  key		= {ext4 and data loss},
  title		= {ext4 and data loss},
  year		= {2009},
  url		= {https://lwn.net/Articles/322823/},
  urldate	= {2020-05-20}
}

@Misc{		  www:man-pthread,
  key		= {pthread.h man page},
  title		= {pthread.h man page},
  year		= {2017},
  url		= {https://man7.org/linux/man-pages/man0/pthread.h.0p.html},
  urldate	= {2021-03-19}
}

@Misc{		  www:mappings,
  key		= {c11-mappings},
  title		= {{C/C++11} mappings to processors},
  url		= {http://www.cl.cam.ac.uk/~pes20/cpp/cpp0xmappings.html},
  urldate	= {2016-09-27}
}

@Misc{		  www:nano,
  key		= {nano},
  title		= {The {GNU Nano} homepage},
  author	= {{GNU Nano}},
  year		= {2019},
  url		= {https://nano-editor.org},
  urldate	= {2020-06-15},
  ids		= {nano}
}

@Misc{		  www:persevere-nano-patch,
  key		= {files: improve the backup procedure to ensure no data is
		  lost},
  title		= {files: improve the backup procedure to ensure no data is
		  lost},
  author	= {Michalis Kokologiannakis},
  year		= {2020},
  month		= jul,
  day		= {09},
  url		= {https://git.savannah.gnu.org/cgit/nano.git/commit/?id=a84cdaaa50a804a8b872f6d468412dadf105b3c5},
  urldate	= {2020-07-09}
}

@Misc{		  www:posix-2017,
  key		= {POSIX},
  title		= {The Open Group Base Specifications Issue 7},
  author	= {POSIX},
  year		= {2018},
  url		= {https://pubs.opengroup.org/onlinepubs/9699919799/},
  urldate	= {2020-05-20}
}

@Misc{		  www:postgre-fsync-errors,
  key		= {postgre-fsync-errors},
  title		= {Fsync Errors},
  url		= {https://wiki.postgresql.org/wiki/Fsync_Errors},
  urldate	= {2020-05-20}
}

@Misc{		  www:power-regular-language,
  key		= {power-regular-language},
  title		= {Is the power of a regular language regular? {Is} the root
		  of a regular language regular?},
  author	= {Yuval Filmus},
  howpublished	= {Computer Science Stack Exchange},
  note		= {URL:https://cs.stackexchange.com/q/99371 (version:
		  2018-10-31)},
  eprint	= {https://cs.stackexchange.com/q/99371},
  url		= {https://cs.stackexchange.com/q/99371},
  urldate	= {2022-10-20}
}

@Misc{		  www:renameio,
  key		= {renameio},
  title		= {renameio},
  year		= {2020},
  url		= {https://github.com/google/renameio},
  urldate	= {2020-05-20}
}

@Misc{		  www:rmem,
  key		= {rmem},
  title		= {{rmem}: {Executable} concurrency models for {ARMv8},
		  {RISC-V}, {Power}, and {x86}},
  author	= {rmem},
  year		= {2009},
  url		= {https://github.com/rems-project/rmem},
  urldate	= {2019-08-24},
  ids		= {rmem}
}

@Misc{		  www:shuttle,
  key		= {shuttle},
  title		= {Shuttle},
  url		= {https://github.com/awslabs/shuttle},
  urldate	= {2023-07-06}
}

@Misc{		  www:sqlite,
  key		= {sqlite},
  title		= {SQLite},
  year		= {2020},
  url		= {https://sqlite.org/index.html},
  urldate	= {2020-05-20},
  ids		= {sqlite}
}

@Misc{		  www:sqlite-atomic-commit,
  key		= {sqlite-atomic-commit},
  title		= {Atomic Commit In SQLite},
  year		= {2020},
  url		= {https://sqlite.org/atomiccommit.html},
  urldate	= {2020-05-20}
}

@Misc{		  www:svcomp,
  key		= {svcomp},
  title		= {Competition on Software Verification ({SV-COMP})},
  author	= {SV-COMP},
  year		= {2019},
  url		= {https://sv-comp.sosy-lab.org/2019/},
  urldate	= {2019-03-27},
  ids		= {svcomp}
}

@Misc{		  www:vim,
  key		= {vim},
  title		= {{Vim} - the ubiquitous text editor},
  author	= {Vim},
  year		= {2019},
  url		= {https://vim.org},
  urldate	= {2020-06-15},
  ids		= {vim}
}

@InProceedings{	  yang2006:explode,
  title		= {{EXPLODE}: a lightweight, general system for finding
		  serious storage system errors},
  author	= {Yang, Junfeng and Sar, Can and Engler, Dawson},
  booktitle	= {CONF_OSDI 2006},
  location	= {Seattle, Washington},
  shorttitle	= {{EXPLODE}},
  pages		= {131--146},
  publisher	= {{USENIX} Association},
  url		= {https://www.usenix.org/legacy/event/osdi06/tech/full_papers/yang_junfeng/yang_junfeng.pdf},
  urldate	= {2020-06-17},
  date		= {2006-11-06}
}

@Article{	  yang2006:model-checking-fs,
  title		= {Using model checking to find serious file system errors},
  author	= {Yang, Junfeng and Twohey, Paul and Engler, Dawson and
		  Musuvathi, Madanlal},
  volume	= {24},
  url		= {https://doi.org/10.1145/1189256.1189259},
  doi		= {10.1145/1189256.1189259},
  pages		= {393--423},
  number	= {4},
  journaltitle	= {JNL_TCOMPUTS},
  shortjournal	= {JNL_TCOMPUTS},
  urldate	= {2020-06-17},
  date		= {2006-11-01},
  keywords	= {crash, file system, journaling, Model checking, recovery}
}

@InProceedings{	  yuan2018:rapos,
  title		= {Partial Order Aware Concurrency Sampling},
  author	= {Yuan, Xinhao and Yang, Junfeng and Gu, Ronghui},
  booktitle	= {CONF_CAV 2018},
  year		= {2018},
  editor	= {Chockler, Hana and Weissenbacher, Georg},
  publisher	= {Springer International Publishing},
  address	= {Cham},
  pages		= {317--335},
  doi		= {10.1007/978-3-319-96142-2_20},
  isbn		= {978-3-319-96142-2}
}

@InProceedings{	  zengin2013:ftpar,
  title		= {A programming language approach to fault tolerance for
		  fork-join parallelism},
  author	= {Mustafa Zengin and Viktor Vafeiadis},
  booktitle	= {CONF_TASE 2013},
  year		= {2013},
  publisher	= {IEEE}
}

@InProceedings{	  zhang2013:happens-before,
  title		= {An Operational Approach to Happens-Before Memory Model},
  author	= {Yang Zhang and Xinyu Feng},
  booktitle	= {CONF_TASE 2013},
  year		= {2013},
  pages		= {121--128},
  publisher	= {{IEEE} Computer Society},
  doi		= {10.1109/TASE.2013.24}
}

@InProceedings{	  zhang2015:dpor-relaxed,
  title		= {Dynamic partial order reduction for relaxed memory
		  models},
  author	= {Naling Zhang and Markus Kusano and Chao Wang},
  booktitle	= {CONF_PLDI 2015},
  year		= {2015},
  pages		= {250--259},
  publisher	= {{ACM}},
  address	= {New York, NY, USA},
  url		= {http://doi.acm.org/10.1145/2737924.2737956},
  doi		= {10.1145/2737924.2737956},
  ids		= {stateless-tso-zhang}
}

@InProceedings{	  zheng2014:torturing-databases,
  title		= {Torturing Databases for Fun and Profit},
  author	= {Zheng, Mai and Tucek, Joseph and Huang, Dachuan and Yang,
		  Elizabeth S and Zhao, Bill W and Qin, Feng and Lillibridge,
		  Mark and Singh, Shashank},
  booktitle	= {CONF_OSDI 2014},
  year		= {2014},
  publisher	= {{USENIX} Association},
  numpages	= {16},
  location	= {Broomfield, CO},
  pages		= {449--464},
  url		= {https://www.usenix.org/system/files/conference/osdi14/osdi14-paper-zheng_mai.pdf},
  urldate	= {2020-11-16},
  langid	= {english}
}

@InProceedings{	  ziliani2013:mtac,
  title		= {{Mtac}: {A} monad for typed tactic programming in {C}oq},
  author	= {Beta Ziliani and Derek Dreyer and Neelakantan R.
		  Krishnaswami and Aleksandar Nanevski and Viktor Vafeiadis},
  booktitle	= {CONF_ICFP 2013},
  year		= {2013},
  publisher	= {ACM},
  ids		= {mtac}
}
